package com.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.pojos.JobApplication;

public interface JobApplicationRepository extends JpaRepository<JobApplication,Long> {

}
