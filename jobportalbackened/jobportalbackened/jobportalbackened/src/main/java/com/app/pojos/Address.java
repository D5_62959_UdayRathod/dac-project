package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name="address")
public class Address extends BaseEntity {
	@Column(length = 50, name = "street_name")
	private String streetName;
	@Column(length = 50, name = "land_mark")
	private String landMark;
	@Column(length = 50, name = "city")
	private String cityName;
	@Column(length = 10, name = "pincode")
	private int pinCode;
	@Column(length = 30)
	private String state;	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="employer_id")
	private User employer;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="company_id")
	private Company company;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="jobpost_id")
	private JobPost jobPost;
	
}
