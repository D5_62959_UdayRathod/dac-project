package com.app.service;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dao.JobApplicationRepository;
import com.app.dao.JobPostRepository;
import com.app.dao.JobSeekerRepository;
import com.app.dao.UserRepository;
import com.app.dto.JobApplicationDTO;
import com.app.pojos.JobApplication;
import com.app.pojos.JobPost;
import com.app.pojos.JobSeeker;
import com.app.pojos.User;

@Service
@Transactional
public class JobSeekerImpl implements IJobSeekerService {

	@Autowired
	private JobSeekerRepository jobSeekerRepo;

	@Autowired
	private JobPostRepository jobPostRepo;
	
	@Autowired
	private JobApplicationRepository jobApplicationRepo;

	@Override
	public String applyForJobPost(JobApplicationDTO job) {
		// find job seeker by it's id
		JobSeeker jobSeeker = jobSeekerRepo.findById(job.getJobseekerId())
				.orElseThrow(() -> new ResourceNotFoundException("Invalid JobSeeker id!!!"));
		// find job post by it's id
		JobPost jobPost = jobPostRepo.findById(job.getId())
				.orElseThrow(() -> new ResourceNotFoundException("Invalid Job post  id!!!"));
		// create job application
		JobApplication application = new JobApplication();
		application.setApplyDate(LocalDate.now());
		application.setJobpost(jobPost);
		application.setJobseeker(jobSeeker);
		return "Job application successful with application Id "+jobApplicationRepo.save(application).getId();
	}

	@Override
	public JobSeeker registerJobSeeker(JobSeeker applicant) {

		return jobSeekerRepo.save(applicant);
	}

}
