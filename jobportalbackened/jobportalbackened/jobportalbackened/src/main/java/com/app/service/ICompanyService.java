package com.app.service;



import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.app.dto.CompanyDto;
import com.app.dto.JobPostDto;
import com.app.pojos.Company;
import com.app.pojos.JobPost;

public interface ICompanyService {
 
	Company addCompany(CompanyDto companydto);
 //   Company getCompanyByEmail(String companyEmail);
    String deleteCompanyById(long companyid);
    Company updateCompany(@RequestBody Company transientCompany);
    List<Company> getAllCompany();
}
