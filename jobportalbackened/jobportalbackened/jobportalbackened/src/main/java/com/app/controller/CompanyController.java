package com.app.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dao.JobPostRepository;
import com.app.dto.CompanyDto;
import com.app.dto.JobPostDto;
import com.app.pojos.Company;
import com.app.pojos.JobPost;
import com.app.service.CompanyServiceImpl;
import com.app.service.ICompanyService;
@RestController
@RequestMapping("/company")
@CrossOrigin
public class CompanyController {
	@Autowired
	private ICompanyService companyService;

	@PostMapping("/add/{employerId}")
	public Company addCompany(@PathVariable long employerId,@RequestBody CompanyDto companyDto) {
		companyDto.setEmployerId(employerId); 
		return companyService.addCompany(companyDto);
	}
//	@GetMapping("/search/{email}")
//	public Company getCompanyByEmail(@PathVariable String companyEmail) {
//		return companyService.getCompanyByEmail(companyEmail);
//	}
	@GetMapping("/delete/{id}")
	public String deleteCompanyById(@PathVariable("id") long companyid) {
		return companyService.deleteCompanyById(companyid);
	}
    

	@PostMapping("/update")
	public Company updateCompany(@RequestBody Company transientCompany) {
		return companyService.updateCompany(transientCompany);
	}
	@GetMapping("/get/all")
	public List<Company> getAllcompany() {
		
		return companyService.getAllCompany();
	}
	
	
}
