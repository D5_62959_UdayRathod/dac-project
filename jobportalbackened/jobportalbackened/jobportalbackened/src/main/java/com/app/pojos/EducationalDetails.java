package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
//@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
//@Table(name="educational_details")
@Embeddable
public class EducationalDetails /* extends BaseEntity */ {
 @Column(length=20,name="degree_name")	
 private String degreeName;
 @Column(length=20)
 private String specialization;
 @Column(length=20,name="institute_name")
 private String instituteName;
 @Column(length=20,name="start_date")
 private LocalDate startedDate;
 @Column(length=20,name="completion_name")
 private LocalDate completionDate;
 @Column(length=20)
 private double percentage;
 @Column(length=20)
 private double cgpa;
// @ManyToOne(fetch=FetchType.LAZY)
// @JoinColumn(name="user_id" ,nullable=false)
// private User jobseeker;
 
}
