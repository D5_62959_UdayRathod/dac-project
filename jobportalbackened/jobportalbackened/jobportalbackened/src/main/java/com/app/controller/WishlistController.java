package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.WishList;
import com.app.service.IWishlistService;

@RestController
@RequestMapping("/wishlist")
public class WishlistController {
	@Autowired
	private IWishlistService wishListService;

	@GetMapping("/add/{id}")
	public String addIntoWishlist(@PathVariable("id") long jobpostid) {

		return wishListService.addIntoWishlist(jobpostid);
	}
}
