package com.app.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.app.pojos.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class JobPostDto {
	@NotBlank(message = "jobPostTitle must be supplied")
	//@Length(min = 5,max=30,message = "Invalid length of chars for  jobPostTitle")
	private String jobPostTitle;
	private LocalDate createDate;
	@NotBlank(message = "jobDescription must be supplied")
	//@Length(min = 100,max=500,message = "Invalid length of chars for  jobDescription")
	private String jobDescription;
	@NotBlank(message = "jobType must be supplied")
	private String JobType;
	@NotBlank(message = "isActive status must be supplied")
	private boolean isActive;
	@NotBlank(message = "salary must be supplied")
	private double salary;
	@NotBlank(message = "vacantPost must be supplied")
	private int vacantPost;
	//@NotBlank(message = "companyId must be supplied")
	private long companyid;
	
}
	
