package com.app.service;

import java.util.List;

import com.app.dto.UserDto;
import com.app.pojos.Address;
import com.app.pojos.Company;
import com.app.pojos.User;

public interface IAdminService {
  
	//.Company addCompany(Company company,User userid,Address addressidSS);
	List<User> getAllUser();
	User findUserByEmail(String email);
	String deleteUser(long id);
    User updateUserdetails(User user);
}
