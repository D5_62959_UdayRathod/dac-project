
package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "users")
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
@Inheritance(strategy = InheritanceType.JOINED)
//@AttributeOverride(name = "id", column = @Column(name = "employer_id"))
public class User extends BaseEntity {
//@Id
//@GeneratedValue(strategy=GenerationType.IDENTITY)
//@Column(name="user_id")
//private long userId;
	@Column(length = 30, name = "first_name")
	private String firstName;
	@Column(length = 30, name = "last_name")
	private String lastName;
	@Column(length = 100, unique = true)
	private String email;
	@Column(length = 13, name = "mobile_number")
	private long mobileNumber;
	@Column(length = 20)
	@JsonProperty(access = Access.WRITE_ONLY) //to skip password during ser.
	private String password;
	@Column(length = 1)
	private String gender;
	@Column(name = "reg_date")
	private LocalDate registerDate;
	@Enumerated(EnumType.STRING)	
	private Role role;

}
