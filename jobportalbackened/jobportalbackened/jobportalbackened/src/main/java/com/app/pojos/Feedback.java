package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Feedback extends BaseEntity {
	private String comments;	
	@ManyToOne()
	@JoinColumn(name="user_id")
	private User user;
	@ManyToOne()
	@JoinColumn(name="company_id")
	private Company company;
	
	
}
