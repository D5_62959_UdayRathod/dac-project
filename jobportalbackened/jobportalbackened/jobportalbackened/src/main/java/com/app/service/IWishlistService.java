package com.app.service;


import com.app.pojos.JobPost;
import com.app.pojos.WishList;

public interface IWishlistService {
	String addIntoWishlist(long jobpostid);
}
