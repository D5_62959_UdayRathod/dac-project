package com.app.pojos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "job_seeker")
@AttributeOverride(name = "id", column = @Column(name = "job_seeker_id"))
public class JobSeeker extends User {
	@Column(length = 15, name = "birth_date")
	private LocalDate dateOfBirth;
	@Column(length = 200, name = "profile_photo")
//later need to change from string to blob
	private String profilePhoto;
//job seeker has a collection of edu details :
	@ElementCollection
	@CollectionTable(name = "educational_details", joinColumns = @JoinColumn(name = "job_seeker_id"))
	private List<EducationalDetails> eduDetails = new ArrayList<>();

	// job seeker has multiple skill sets
	@ElementCollection
	@CollectionTable(name = "skill_sets", joinColumns = @JoinColumn(name = "job_seeker_id"))
	@Column(length = 30, name = "skillset_name")
	private List<String> skillSets = new ArrayList<>();
	
	// job seeker has multiple exp details
	@ElementCollection
	@CollectionTable(name = "experience_details", joinColumns = @JoinColumn(name = "job_seeker_id"))
	@Column(length = 30, name = "exp_details")
	private List<ExperienceDetails> experienceDetails = new ArrayList<>();
}
