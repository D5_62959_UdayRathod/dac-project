package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.LoginDTO;
import com.app.pojos.User;
import com.app.service.IUserService;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {
	@Autowired
	private IUserService userService;
	@PostMapping("/registration")	
	public User userRegistration(@RequestBody User user)
	{ 
		System.out.println(user);
		return userService.RegisterUser(user);
		
	}
	@PostMapping("/signin")
	public User userSignin(@RequestBody LoginDTO login) {
	//	System.out.println(email);
		System.out.println(login);
		User authenicateUser = userService.authenticateUser(login);
		return authenicateUser;
	}
}
