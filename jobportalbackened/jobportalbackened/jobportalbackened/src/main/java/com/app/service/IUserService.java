package com.app.service;

import com.app.dto.LoginDTO;
import com.app.pojos.User;

public interface IUserService {
  
    User RegisterUser(User user);
    User authenticateUser(LoginDTO dto);
}
