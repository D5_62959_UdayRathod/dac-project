package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.UserDto;
import com.app.pojos.Address;
import com.app.pojos.Company;
import com.app.pojos.User;
import com.app.service.AdminServiceImpl;


@CrossOrigin
@RestController
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	private AdminServiceImpl adminservice;
    
    @GetMapping("/alluser")
    public List<User> getAllUser(){
    	System.out.println("In getall method");
    	return adminservice.getAllUser();
    }
    @GetMapping("/search/{email}")
    public User findUserByEmail(@PathVariable String email) {
    	System.out.println(email);
    	return adminservice.findUserByEmail(email);
    }
    @GetMapping("/delete/user/{id}")
    public String deleteUser(@PathVariable long id) {
    	return adminservice.deleteUser(id);
    }
    
	@PutMapping("/update/user")
	public User updateUserdetails(@RequestBody User user) {
		return adminservice.updateUserdetails(user);

	}
}

