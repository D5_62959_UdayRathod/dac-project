package com.app.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CompanyDto {
	@NotBlank(message = "companyName must be supplied")
	private String companyName;
	@NotBlank(message = "companyType must be supplied")
	private String companyType;
	@NotBlank(message = "companyDescription must be supplied")
	private String companyDescription;
	private LocalDate establishmentDate;
	@NotBlank(message = "companyEmail must be supplied")
	private String companyEmail;
	@NotBlank(message = "companyContactNo must be supplied")
	private int companyContactNo;
	@NotBlank(message = "companyImage must be supplied")
	private String companyImage;
	@NotBlank(message = "compnayWebsite must be supplied")
	private String companyWebsite;
	@NotBlank(message = "employer id must be supplied")
	private long employerId;
}
