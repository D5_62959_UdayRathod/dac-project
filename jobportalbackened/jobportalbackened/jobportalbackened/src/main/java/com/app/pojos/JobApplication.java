package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name="job_apply")
public class JobApplication extends BaseEntity {
	@Column (length = 20)
	private LocalDate applyDate;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id",nullable=false)
	private JobSeeker jobseeker;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="jobpost_id",nullable=false)
	private JobPost jobpost;
	

}
