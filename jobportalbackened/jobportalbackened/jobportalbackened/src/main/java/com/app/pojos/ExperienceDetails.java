package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

//@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
@ToString
//@Table(name="experience_details")
@Embeddable
public class ExperienceDetails /* extends BaseEntity */{
	@Column(length = 30,name="company_name")
	private String companyName;
	@Column(length = 30,name="iscurrent_job")
	private boolean isCurrentJob;
	@Column(length = 20,name="start_date")
	private LocalDate startDate;
	@Column(length = 20,name="end_date")
	private LocalDate endDate;
	@Column(length = 30,name="job_title")
	private String jobTitle;
	@Column(length = 30,name="job_locationcity")
	private String jobLocationCity;
	@Column(length = 30,name="job_locationstate")
	private String jobLocationState;
	@Column(length = 50,name="job_description")
	private String jobDescription;
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="user_id" ,nullable=false,unique=true)
//	private User jobseeker;
	

}
