package com.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dao.UserRepository;
import com.app.dto.LoginDTO;
import com.app.pojos.User;

@Service
@Transactional
public class UserServiceImpl implements IUserService {
	@Autowired
	private UserRepository userRepo;

	@Override
	public User RegisterUser(User user) {
		System.out.println(user);
		return userRepo.save(user);
	}

	@Override
	public User authenticateUser(LoginDTO dto) {
		return userRepo.findByEmailAndPassword(dto.getEmail(),dto.getPassword())
				.orElseThrow(() -> new ResourceNotFoundException("Invalid email or password !!!!!!!!!!"));
//		User user =optional.get();
//		if (user.getRole() == Role.ADMIN)
//			return user;
//		if (user.getRole() == Role.JOB_SEEKER)
//			return user ;
//		if(user.getRole()==Role.EMPLOYER)		
//		return user;
		// return optional.orElseThrow(() -> new ResourceNotFoundException("Invalid
		// email or password !!!!!!!!!!"));
	}

}
