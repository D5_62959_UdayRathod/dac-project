package com.app.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dao.AddressRepository;
import com.app.dao.AdminRepository;
import com.app.dao.CompanyRepository;
import com.app.dao.UserRepository;
import com.app.dto.UserDto;
import com.app.pojos.Address;
import com.app.pojos.Company;
import com.app.pojos.User;
@Service
@Transactional
public class AdminServiceImpl implements IAdminService {
	@Autowired
	AdminRepository adminRepo;
	@Autowired
	CompanyRepository companyRepo;
	@Autowired
	UserRepository userRepo;
	@Autowired
	AddressRepository addressRepo;

	@Override
	public List<User> getAllUser() {
		List<User> users = adminRepo.findAll();
		return users;
	}
	@Override
	public User findUserByEmail(String email) {
		return adminRepo.findByEmail(email);
	}
	
	@Override
	public String deleteUser(long id) {
		String message = "Failed to delete user detail!!!";
		if (userRepo.existsById(id)) {
			userRepo.deleteById(id);
			message = "user deleted ";
		}
		return message;
	}
	@Override
	public User updateUserdetails(User user) {
		userRepo.findById(user.getId()).orElseThrow(() -> new ResourceNotFoundException("Invalid User ID!!!!!! : Can't Update details"));
		return userRepo.save(user);
	}
	
}