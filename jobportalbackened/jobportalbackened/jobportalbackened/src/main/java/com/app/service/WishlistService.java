package com.app.service;

import java.time.LocalDate;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dao.JobPostRepository;
import com.app.dao.WishlistRepository;
import com.app.pojos.JobPost;
import com.app.pojos.WishList;

@Service
@Transactional
public class WishlistService implements IWishlistService {
	@Autowired
	private JobPostRepository jobPostRepo;
	@Autowired
	private WishlistRepository wishListRepo;
	@Autowired
	 private ModelMapper mapper;

		@Override
		public String addIntoWishlist(long jobpostid) {

			JobPost jobpost = jobPostRepo.findById(jobpostid)
					.orElseThrow(() -> new ResourceNotFoundException("Invalid JobPost Id!!!"));

			WishList wishlist = new WishList();
			wishlist.setJobpost(jobpost);
			wishlist.setSaveDate(LocalDate.now());
			wishListRepo.save(wishlist);
			return "Jobpost with id "+jobpost.getId()+"added succesfully";
		}

}
