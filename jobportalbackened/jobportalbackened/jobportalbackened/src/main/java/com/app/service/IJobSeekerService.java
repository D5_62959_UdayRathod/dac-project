package com.app.service;

import com.app.dto.JobApplicationDTO;
import com.app.pojos.JobPost;
import com.app.pojos.JobSeeker;

public interface IJobSeekerService {
//register new job seeker with the job portal
	JobSeeker registerJobSeeker(JobSeeker applicant);
	public String applyForJobPost(JobApplicationDTO job);
}
