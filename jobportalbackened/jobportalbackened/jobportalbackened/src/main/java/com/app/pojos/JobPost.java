package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name="job_post")
@JsonInclude(value = Include.NON_NULL)
public class JobPost extends BaseEntity {
	@Column(length = 30,name="jobpost_title")
	private String jobPostTitle;
	@Column(length = 20,name="created_date")
	private LocalDate createDate;
	@Column(length = 1000,name="job_desc")
	private String jobDescription;
	@Column(length=20,name="job_type")
	private String jobType;
	@Column(name="is_active")
	private boolean isActive;
	@Column(name="salary")
	private double salary;
	@Column(name="vacant_post")
	private int vacantPost;
	//currently removed : later can be added as extra FK
//	@ManyToOne(fetch=FetchType.LAZY)
//	@JoinColumn(name="employer_id")
//	private User employer;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="company_id")
	private Company company;
	
	

}
