package com.app.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.app.pojos.BaseEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class JobPostResponseDto {

	private String jobPostTitle;
	private LocalDate createDate;

	private String jobDescription;

	private String JobType;

	private boolean isActive;
	private double salary;
	private int vacantPost;
	private long companyid;

}
