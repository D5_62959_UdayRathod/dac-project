package com.app.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dao.AdminRepository;
import com.app.dao.CompanyRepository;
import com.app.dao.JobPostRepository;
import com.app.dao.UserRepository;
import com.app.dto.CompanyDto;
import com.app.dto.JobPostDto;
import com.app.pojos.Company;
import com.app.pojos.JobPost;
import com.app.pojos.User;
@Service
@Transactional
public class CompanyServiceImpl implements ICompanyService {
   
	@Autowired
	JobPostRepository jobPostRepo;
	@Autowired
	CompanyRepository companyRepo;
	@Autowired
	UserRepository userRepo;
    @Autowired
    private ModelMapper mapper;
	
	public Company addCompany(CompanyDto companydto) {	
	//	Company company =new Company();
		User employer= userRepo.findById(companydto.getEmployerId()).orElseThrow(()->new ResourceNotFoundException("invalid employer id"));
//		company.setUser(persistedUser);
//		company.setCompanyName(companydto.getCompanyName());
//		company.setCompanyType(companydto.getCompanyType());
//		company.setCompanyDescription(companydto.getCompanyDescription());
//		company.setEstablishmentDate(companydto.getEstablishmentDate());
//		company.setCompanyEmail(companydto.getCompanyEmail());
//		company.setCompanyContactNo(companydto.getCompanyContactNo());
//		company.setCompanyImage(companydto.getCompanyImage());
//		company.setCompnayWebsite(companydto.getCompnayWebsite());	
		Company company = mapper.map(companydto, Company.class);
		//establish uni dir asso between Company ---> User
		company.setEmployer(employer);
		return companyRepo.save(company);
	}
	
//	public Company getCompanyByEmail(String companyEmail) {
//
//		return companyRepo.findByEmail(companyEmail);
//
//	}
      
	public String deleteCompanyById(long companyid) {
		String message="deletion failed!!! wrong id";
		if(companyRepo.existsById(companyid)) {
			companyRepo.deleteById(companyid);
			message ="succesfully deleted company with given id";
		}
		
		return message;
	}
	
	public Company updateCompany(@RequestBody Company transientCompany) {

		companyRepo.findById(transientCompany.getId())
				.orElseThrow(() -> new ResourceNotFoundException("Invalid Company ID!!!!!! : Can't Update details"));
		User user =transientCompany.getEmployer();
		userRepo.save(user);
		System.out.println(user);
		System.out.println(transientCompany);
		return companyRepo.save(transientCompany);
	}
	
	@Override
	public List<Company> getAllCompany() {
		
		return companyRepo.findAll();
				
	}
	

}




