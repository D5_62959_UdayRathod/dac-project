package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.JobApplicationDTO;
import com.app.pojos.JobPost;
import com.app.pojos.JobSeeker;
import com.app.service.IJobSeekerService;
import com.app.service.IUserService;
@CrossOrigin
@RestController
@RequestMapping("/jobseeker")
public class JobSeekerController {
//	@Autowired
//	private IUserService userService;

	@Autowired
	private IJobSeekerService jobSeekerService;

	// job seeker registration
	@PostMapping("/profile/{id}")
	public ResponseEntity<?> registerJobSeeker(@PathVariable("id") long jobseekerid ,@RequestBody JobSeeker jobSeeker) {
		  jobSeeker.setId(jobseekerid);
		return ResponseEntity.status(HttpStatus.CREATED).body(jobSeekerService.registerJobSeeker(jobSeeker));
	}

	@PostMapping("/application")
	public String applyForJobPost(@RequestBody JobApplicationDTO application) {

		return jobSeekerService.applyForJobPost(application);
	}
	
	
}
