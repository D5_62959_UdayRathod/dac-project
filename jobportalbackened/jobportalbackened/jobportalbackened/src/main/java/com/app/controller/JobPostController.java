package com.app.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.JobPostDto;
import com.app.dto.JobPostResponseDto;
import com.app.pojos.JobPost;
import com.app.service.IJobPostService;
@CrossOrigin
@RestController
@RequestMapping("/jobpost")
public class JobPostController {
	@Autowired
	IJobPostService jobPostService;
		
	@PostMapping("/create/{companyid}")
	public JobPostResponseDto createJobPost(@RequestBody JobPostDto jobpostdto,@PathVariable long companyid) {
	    System.out.println(jobpostdto);
	    jobpostdto.setCompanyid(companyid);
		return jobPostService.createJobPost(jobpostdto);
	}
	
	@GetMapping("/get/{id}")
	public List<JobPost> getSelectedJobPostedByCompanyId(@PathVariable("id")long companyid) {
		
		return jobPostService.getSelectedJobPostedByCompanyId(companyid);
	}
	@GetMapping("/all")
	public List <JobPost> getAllJobPosted(){
		return jobPostService.getAllJobPosted();
	}

	@PutMapping("/update")
	public JobPost updateJobPosted(@RequestBody JobPost jobpost) {
		return jobPostService.updateJobPosted(jobpost);
	}

	@DeleteMapping("/delete/{id}")
	public String deleteJobPosted(@PathVariable("id") long jobpostid) {

		return jobPostService.deleteJobPosted(jobpostid);
	}

}
