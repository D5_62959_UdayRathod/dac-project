package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = false)
@Proxy(lazy = false)
@ToString
@Table(name = "company")
public class Company extends BaseEntity {
	@Column(length = 50, unique = true, name = "company_name")
	private String companyName;
	@Column(length = 30, name = "company_type")
	private String companyType;
	@Column(length = 200, name = "company_desc")
	private String companyDescription;
	@Column(length = 30, name = "est_date")
	private LocalDate establishmentDate;
	@Column(length = 50, unique = true, name = "company_email")
	private String companyEmail;
	@Column(length = 12, name = "company_contactno")
	private int companyContactNo;
	@Column(name = "company_image")
	private String companyImage;
	@Column(length = 50, name = "company_website")
	private String companyWebsite;
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "employer_id", nullable = false)
	private User employer;

}
