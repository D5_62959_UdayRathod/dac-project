package com.app.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JobApplicationDTO {
	private long jobseekerId;
	private long id;
}
