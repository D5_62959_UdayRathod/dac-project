package com.app.dao;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.pojos.JobPost;
public interface JobPostRepository extends JpaRepository<JobPost,Long> {
  
	 @Query("select p from JobPost p where p.company.id = ?1")
	  List<JobPost> findByid(long companyid);
	
}
