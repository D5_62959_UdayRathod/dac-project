package com.app.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dao.CompanyRepository;
import com.app.dao.JobPostRepository;
import com.app.dao.UserRepository;
import com.app.dto.JobPostDto;
import com.app.dto.JobPostResponseDto;
import com.app.pojos.Company;
import com.app.pojos.JobPost;
import com.app.pojos.User;

@Service
@Transactional
public class JobPostServiceImpl implements IJobPostService {
	@Autowired
	private JobPostRepository jobPostRepo;
	@Autowired
	private CompanyRepository companyRepo;
	@Autowired
	private ModelMapper mapper;
	@Autowired
	private UserRepository userRepo;

	@Override
	public JobPostResponseDto createJobPost(JobPostDto jobpostdto) {
		// JobPost jobpost =new JobPost();
		Company company = companyRepo.findById(jobpostdto.getCompanyid())
				.orElseThrow(() -> new ResourceNotFoundException("Invalid Company id!!! Try again"));

		JobPost jobpost = mapper.map(jobpostdto, JobPost.class);
		jobpost.setCompany(company);
//		Company Persistedcompany1= persistedcompany.get();
//		System.out.println(Persistedcompany1);
//		jobpost.setCompany(Persistedcompany1);
//		jobpost.setCreateDate(jobpostdto.getCreateDate());
//		jobpost.setActive(jobpostdto.isActive());
//		jobpost.setJobDescription(jobpostdto.getJobDescription());
//		jobpost.setJobPostTitle(jobpostdto.getJobPostTitle());		
//		jobpost.setJobType(jobpostdto.getJobType());
//		jobpost.setSalary(jobpostdto.getSalary());
//		jobpost.setUser(null);
//		jobpost.setVacantPost(jobpostdto.getVacantPost());
		JobPostResponseDto jobPostDtoResponse = mapper.map( jobPostRepo.save(jobpost), JobPostResponseDto.class);
		jobPostDtoResponse.setCompanyid(jobpostdto.getCompanyid());
		return jobPostDtoResponse;
	}

	@Override
	public List<JobPost> getSelectedJobPostedByCompanyId(long companyid) {

		List<JobPost> jobposts = jobPostRepo.findByid(companyid);

		return jobposts;
	}

	@Override
	public List<JobPost> getAllJobPosted() {
		return jobPostRepo.findAll();
	}
	//updating
	@Override
	public JobPost updateJobPosted(JobPost jobpost) {
        System.out.println(jobpost);
		JobPost existingJobPost = jobPostRepo.findById(jobpost.getId()).orElseThrow(() -> new ResourceNotFoundException("Invalid Jobpost id"));
	//	existingJobPost.setCompany(jobpost.getCompany());
		Company company = jobpost.getCompany();
		company.setEmployer(existingJobPost.getCompany().getEmployer());
		companyRepo.save(company);//updates company details
//		User user = jobpost.getJobseeker();
//		userRepo.save(user);
//		jobpost.setJobseeker(existingJobPost.getJobseeker());
		return jobPostRepo.save(jobpost);//updates job post dtls
	}
	
	@Override
	public String deleteJobPosted(long jobpostid) {
		String message = "JobPost Delation Failed!!!";
		if (jobPostRepo.existsById(jobpostid)) {
			jobPostRepo.deleteById(jobpostid);
			message = "jobpost deleted successfully!!!";
		}
		return message;
	}

}
