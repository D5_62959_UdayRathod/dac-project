package com.app.service;

import java.util.List;

import com.app.dto.JobPostDto;
import com.app.dto.JobPostResponseDto;
import com.app.pojos.JobPost;

public interface IJobPostService {
	 JobPostResponseDto createJobPost(JobPostDto jobpostdto);
	 List<JobPost> getSelectedJobPostedByCompanyId(long companyid);
	 List <JobPost> getAllJobPosted();
	 JobPost updateJobPosted(JobPost jobpost);
	 String deleteJobPosted(long jobpostid);
}
