package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name="wishlist")
public class WishList extends BaseEntity {
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobpost_id",nullable=false)
	private JobPost jobpost;
	@Column(name="save_date")
	private LocalDate saveDate;
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "jobpost_id",nullable=false)
//	private JobSeeker jobseeker;
}
