package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

public class SkillRequired {
	@Column(length = 50, name = "skill_name")
	private String skillName;
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="jobpost_id",nullable=false)
	private JobPost jobpost;
}
