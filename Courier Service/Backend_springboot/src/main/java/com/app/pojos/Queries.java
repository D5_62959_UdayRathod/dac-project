package com.app.pojos;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
//JPA annotations
@Entity
@Table(name="query_tbl")
public class Queries {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "query_id")
	private Long query_id;
	@Column(name="query",length = 100)
	private String query;
	@Column(name="query_status",length = 10)
	private String queryStatus;
	
	@ManyToOne
	@JoinColumn(name = "cust_id")
	private Customer cust_id;
	
	@ManyToOne
	@JoinColumn(name = "courier_id")
	private CourierDetails courier_id;
	
}
