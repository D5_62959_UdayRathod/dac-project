package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(exclude = "password")
//JPA annotations
@Entity
@Table(name="employee_tbl")
public class Employee  {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "emp_id")
	private Long id;
	@Column(name="emp_firstname",length = 30)
	private String firstName;
	@Column(name="emp_lastname",length = 30)
	private String lastName;
	@Column(name="emp_phone",length = 30)
	private String phone;
	@Column(name="emp_email",length = 30,unique = true)
	private String email;
	@Column(name="emp_password",nullable = false)
	private String password;
	@Column(name="designatation")
	private String designatation;
	
}
