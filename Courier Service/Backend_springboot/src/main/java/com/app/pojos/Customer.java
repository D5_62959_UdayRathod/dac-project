package com.app.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
//JPA annotations
@Entity
@Table(name="customer_tbl")
public class Customer  {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "cust_id")
	private Long cust_id;
	@Column(name="cust_firstname",length = 30)
	private String firstName;
	@Column(name="cust_lastname",length = 30)
	private String lastName;
	@Column(name="cust_phone",length = 30)
	private String phone;
	@Column(name="cust_email",length = 30,unique = true)
	private String email;
	@Column(name="cust_password",nullable = false)
	private String password;
	
}
