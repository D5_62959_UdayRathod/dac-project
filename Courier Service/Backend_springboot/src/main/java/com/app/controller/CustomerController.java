package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.pojos.Customer;
import com.app.pojos.Employee;
import com.app.service.ICustomerService;
import com.app.service.IEmployeeService;

@RestController //=@Controller + @ResponseBody
@RequestMapping("/api/customers") //resource : class level pattern
@CrossOrigin(origins = "http://localhost:3000")
public class CustomerController {
	//dep : service layer i/f
	
	@Autowired
	private ICustomerService custService;
	@Autowired
	private BCryptPasswordEncoder passwordEncodr;
	
	public CustomerController() {
		System.out.println("in ctor of "+getClass());
	}
	//add req handling method(REST API endpoint) to ret list of all emps
	@GetMapping
	public List<Customer> fetchAllCustDetails()
	{
		System.out.println("in fetch all ");
		return custService.getAllCustomers();
		
	}
	//add req handling method(REST API endpoint) to create a new resource : Emp
	@PostMapping
	public Customer addNewCustomer(@RequestBody  Customer cust)
	{
		cust.setPassword(passwordEncodr.encode(cust.getPassword()));
		System.out.println("in add new emp ");//id : null
		return custService.insertCustomerDetails(cust);
	}
	
	@PostMapping("/login")
	public ResponseEntity<?> getCustByEmail(@RequestBody  Customer cust)
	{
		System.out.println("in get  cust "+cust);//id : null
		Customer c = custService.findByEmail(cust.getEmail());
	System.out.println(c.getPassword());
	System.out.println(cust.getPassword());
		if(passwordEncodr.matches(cust.getPassword(), c.getPassword()) | cust.getPassword().equals(c.getPassword()))
		{ System.out.println("matching");
			c.setPassword(cust.getPassword());
		 return  ResponseEntity.ok().body(c);}
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
	}
	//add req handling method(REST API endpoint) to delete emp details
	@DeleteMapping("/{cid}")
	public String deleteCustomerDetails(@PathVariable long cid)
	{
		System.out.println("in del cust dtls "+cid);
		return custService.deleteCustomerDetails(cid);
	}
	//add req handling method(REST API endpoint) to get emp details by it's id
	@GetMapping("/{cust_id}")
	public Customer getCustomerDetails(@PathVariable long cust_id)
	{
		System.out.println("in get cust dtls");
		return custService.getCustomerDetails(cust_id);
	}
	//add req handling method(REST API endpoint) to update emp details
		@PutMapping
		public Customer updateCustomerDetails(@RequestBody Customer cust)
		{
			System.out.println(cust.getPassword());
		
			Customer customer = custService.getCustomerDetails(cust.getCust_id());
			System.out.println(customer.getPassword());
			if(!cust.getPassword().equals(customer.getPassword()))
			{cust.setPassword(passwordEncodr.encode(cust.getPassword()));
			System.out.println("in update  Customer dtls"+cust);}//not null id}
			Customer updateCustomerDetails = custService.updateCustomerDetails(cust);
			System.out.println(updateCustomerDetails.getPassword());
			return updateCustomerDetails;
		}
	
	
	

}
