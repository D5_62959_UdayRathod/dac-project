package com.app.controller;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.QueryDto;
import com.app.pojos.CourierDetails;
import com.app.pojos.Customer;
import com.app.pojos.Employee;
import com.app.pojos.Queries;
import com.app.service.ICourierDetailsService;
import com.app.service.ICustomerService;
import com.app.service.IEmployeeService;
import com.app.service.IQueryService;
import com.app.service.QueryServiceImp;

@RestController //=@Controller + @ResponseBody
@RequestMapping("/api/queries") //resource : class level pattern
@CrossOrigin(origins = "http://localhost:3000")
public class QueriesController {
	//dep : service layer i/f
	@Autowired
	private IQueryService queryService;
	@Autowired
	private QueryServiceImp queryServiceImpl;
	@Autowired
	private ModelMapper moddel;
	@Autowired
	private ICustomerService custService;
	@Autowired
	private ICourierDetailsService courierService;
	
	public QueriesController() {
		System.out.println("in ctor of "+getClass());
	}
	@GetMapping("/getall")
	public List<QueryDto> getAllQueries(){
		List<QueryDto> qdto=new ArrayList<QueryDto>(); 
		System.out.println("in getallqueries ");
		List<Queries> allQueries = queryService.getAllQueries();
		allQueries.forEach(q->{
			qdto.add(queryServiceImpl.queriesToQueryDto(q));
		});
		return qdto ;
	}
	
	@GetMapping("/{cust_id}")
	public List<QueryDto> getAllQueryByCustId(@PathVariable long cust_id) {
		
		List<QueryDto> qdto=new ArrayList<QueryDto>(); 
		List<Queries> allQueryByCustId = queryService.getAllQueryByCustId(cust_id);
		allQueryByCustId.forEach(q->{
			qdto.add(queryServiceImpl.queriesToQueryDto(q));
		});
		return qdto ;
	}
	
	@PostMapping("/insert")
	public QueryDto insertAQuery(@RequestBody QueryDto qdto)
	{System.out.println("in insert query "+qdto);
		Queries q = moddel.map(qdto,Queries.class);
		q.setQueryStatus("pending");
		Customer c=custService.getCustomerDetails(qdto.getCust_id());
		q.setCust_id(c);
		CourierDetails cd=courierService.getCourierDetails(qdto.getCourier_id());	
		q.setCourier_id(cd);
		System.out.println("in insert query "+q);
		Queries insertedQuery = queryService.insertAQuery(q);
		QueryDto queryDto = queryServiceImpl.queriesToQueryDto(insertedQuery);
		return queryDto;
	}
	@PutMapping("/update")
	public QueryDto updateAQuery(@RequestBody QueryDto qdto)
	{
		System.out.println("in update query "+qdto);
		Queries q = moddel.map(qdto,Queries.class);
		Customer c=custService.getCustomerDetails(qdto.getCust_id());
		q.setCust_id(c);
		CourierDetails cd=courierService.getCourierDetails(qdto.getCourier_id());	
		q.setCourier_id(cd);
		System.out.println("in update query "+q);
		Queries updatedAQuery = queryService.updateAQuery(q);
		QueryDto queryDto = queryServiceImpl.queriesToQueryDto(updatedAQuery);
		System.out.println("in update query "+queryDto);
		return queryDto;
	}
	@PatchMapping
	public QueryDto updateAnQuery(@RequestBody QueryDto qdto)
	{
		System.out.println("in update query "+qdto);
	
		int count = queryService.updateAnQuery(qdto.getQueryStatus(),qdto.getQuery_id());
		System.out.println("rows affected "+count);
		return qdto;
	}
}
