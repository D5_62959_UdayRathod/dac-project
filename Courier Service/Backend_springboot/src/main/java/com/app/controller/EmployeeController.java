package com.app.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.app.dto.AuthResp;
import com.app.jwt_utils.JwtUtils;
import com.app.pojos.Customer;
import com.app.pojos.Employee;
import com.app.service.IEmployeeService;

@RestController //=@Controller + @ResponseBody
@RequestMapping("/api/employees") //resource : class level pattern
@CrossOrigin(origins = "http://localhost:3000")
public class EmployeeController {
	//dep : service layer i/f
	@Autowired
	private JwtUtils utils;
	// dep : Auth mgr
	@Autowired
	private AuthenticationManager manager;
	@Autowired
	private IEmployeeService empService;
	@Autowired
	private BCryptPasswordEncoder passwordEncodr;
	
	public EmployeeController() {
		System.out.println("in ctor of "+getClass());
	}
	//add req handling method(REST API endpoint) to ret list of all emps
	@GetMapping("/getall")
	public List<Employee> fetchAllEmpDetails()
	{
		System.out.println("in fetch all ");
		return empService.getAllEmployees();
		
	}
	//add req handling method(REST API endpoint) to create a new resource : Emp
	@PostMapping
	public Employee addNewEmp(@RequestBody  Employee emp)
	{
		emp.setPassword(passwordEncodr.encode(emp.getPassword()));
		System.out.println("in add new emp "+emp);//id : null
		return empService.insertEmpDetails(emp);
	}
	//what is response entity
	//it consist whole response body
//	If you want to handle special cases like errors (Not Found, Conflict, etc.),
//	you can add a HandlerExceptionResolver to your Spring configuration. So in your code, 
//	you just throw a specific exception (NotFoundException for instance) and decide what to do in your Handler (setting the HTTP status to 404),
//	making the Controller code more clear.
	@PostMapping("/signin")
	public ResponseEntity<?> getEmpByEmail(@RequestBody  Employee emp)
	{
		System.out.println("in get  emp "+emp);//id : null
		Employee e = empService.findByEmail(emp.getEmail());
		 if(passwordEncodr.matches(emp.getPassword(), e.getPassword()))
		 {
			 e.setPassword(emp.getPassword());
		 return ResponseEntity.ok().body(e);}
		 return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
	}
	//add req handling method(REST API endpoint) to delete emp details
	@DeleteMapping("/{eid}")
	public String deleteEmpDetails(@PathVariable long eid)
	{
		System.out.println("in del emp dtls "+eid);
		return empService.deleteEmpDetails(eid);
	}
	//add req handling method(REST API endpoint) to get emp details by it's id
	@GetMapping("/{empId}")
	public Employee getEmpDetails(@PathVariable long empId)
	{
		Employee e = empService.getEmpDetails(empId);
		
		System.out.println("in get emp dtls");
		return e;
	}
	//add req handling method(REST API endpoint) to update emp details
		@PutMapping
		public Employee updateEmpDetails(@RequestBody Employee emp)
		{
			Employee empDetails = empService.getEmpDetails(emp.getId());
			
			if(!emp.getPassword().equals(empDetails.getPassword()))
			{
				emp.setPassword(passwordEncodr.encode(emp.getPassword()));
			}
				Employee e = empService.updateEmpDetails(emp);
			
//			Customer customer = custService.getCustomerDetails(cust.getCust_id());
//			System.out.println(customer.getPassword());
//			if(!cust.getPassword().equals(customer.getPassword()))
//			{cust.setPassword(passwordEncodr.encode(cust.getPassword()));
//			System.out.println("in update  Customer dtls"+cust);}//not null id}
//			Customer updateCustomerDetails = custService.updateCustomerDetails(cust);
//			System.out.println(updateCustomerDetails.getPassword());
//			return updateCustomerDetails;
//			 if(passwordEncodr.matches(emp.getPassword(), e.getPassword()))
//			 {
//				 e.setPassword(emp.getPassword());
//			 }
			
			System.out.println("in update  emp dtls"+emp);//not null id
			return e;
		}
	
		@PostMapping("/login")
		public ResponseEntity<?> validateEmpCreateToken(@RequestBody  Employee request) {
			// store incoming user details(not yet validated) into Authentication object
			// Authentication i/f ---> implemented by UserNamePasswordAuthToken
			UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(request.getEmail(),
					request.getPassword());
			System.out.println("auth details **********");
//			log.info("auth token before {}",authToken);
			try {
				// authenticate the credentials
				
				Authentication authenticatedDetails = manager.authenticate(authToken);
				Employee e=null;
				if(authenticatedDetails!=null)
				{
					 e = empService.findByEmail(request.getEmail());
					 
					System.out.println("auth details **********"+e);
				}
				 if(passwordEncodr.matches(request.getPassword(), e.getPassword()))
				 {
					 e.setPassword(request.getPassword());
				 }
				//log.info("auth token again {} " , authenticatedDetails);
				// => auth succcess
				//System.out.println("auth details **********"+authToken);
				return ResponseEntity.ok(new AuthResp( utils.generateJwtToken(authenticatedDetails),e.getId(),e.getFirstName(),e.getLastName(),e.getPhone(),e.getEmail(),e.getPassword(),e.getDesignatation()));
			} catch (BadCredentialsException e) { // lab work : replace this by a method in global exc handler
				// send back err resp code
				System.out.println("err "+e);
				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
			}

		}
	

}
