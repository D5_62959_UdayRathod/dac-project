package com.app.controller;

import java.time.LocalDate;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.dto.CourierDetailsDto;
import com.app.dto.CourierTrackingDto;
import com.app.pojos.CourierDetails;
import com.app.pojos.CourierTracking;
import com.app.pojos.Customer;
import com.app.pojos.DropAddress;
import com.app.pojos.Employee;
import com.app.pojos.PickupAddress;
import com.app.service.ICourierDetailsService;
import com.app.service.ICourierTrackingService;
import com.app.service.ICustomerService;
import com.app.service.IDropAddressService;
import com.app.service.IEmployeeService;
import com.app.service.IPickUpAddressService;



@RestController //=@Controller + @ResponseBody
@RequestMapping("/api/courierdetails") //resource : class level pattern
@CrossOrigin(origins = "http://localhost:3000")
public class CourierDetailsController {
	//service layer i/f
	@Autowired
	private ICourierDetailsService courierDetailsService;
	@Autowired
	private ICustomerService custService;
	@Autowired
	private IDropAddressService dropAddrService;
	@Autowired
	private IPickUpAddressService pickupAddrservice;
	@Autowired
	private ModelMapper mapper;
	@Autowired
	private ICourierTrackingService courierTrackingService;
	@Autowired
	private IEmployeeService empService;
	
	public CourierDetailsController() {
		System.out.println("in ctor of "+getClass());
	}
	//add req handling method(REST API endpoint) to ret list of all emps
	@GetMapping
	public List<CourierDetails> getAllCourierDetails()
	{
		System.out.println("in fetch all ");
		return courierDetailsService.getAllCourierDetails();
		
	}
	
	
	
	//add req handling method(REST API endpoint) to create a new resource : 
	@PostMapping
	public CourierDetails insertCourierDetails(@RequestBody  CourierDetailsDto transientCourierDetailsDto)
	{
		System.out.println("in add new CourierDetails "+transientCourierDetailsDto);
		transientCourierDetailsDto.setDropDate(transientCourierDetailsDto.getCourierPickUpDate().plusDays(5));
		CourierDetails cd1 = mapper.map(transientCourierDetailsDto,CourierDetails.class);
		Customer c1=custService.getCustomerDetails(transientCourierDetailsDto.getCustId());
		DropAddress d1=dropAddrService.findByDropAddrId(transientCourierDetailsDto.getDropAddrId());
		PickupAddress p1=pickupAddrservice.findByPickupAddrId(transientCourierDetailsDto.getPickUpAddrId());
		cd1.setDropAddr(d1);
		cd1.setPickAddr(p1);
		cd1.setCustomer(c1);
		CourierDetails cd = courierDetailsService.insertCourierDetails(cd1);
		System.out.println("in add new CourierDetails "+cd1);//id : null
		if(cd!=null)
		{
			CourierTracking ct=new CourierTracking();
			ct.setCourierDetails(cd);
			ct.setCustomer(c1);
			ct.setOrderStatus("pending");
			ct.setEmployee(empService.getEmpDetails(1));
			
			courierTrackingService.insertCourierTrackings(ct);
		}
		return cd;
	}
	
	
	
	//add req handling method(REST API endpoint) to delete emp details
	@DeleteMapping("/{courier_id}")
	public String deleteCourierDetails(@PathVariable long courier_id)
	{
		System.out.println("in del courier dtls "+courier_id);
		return courierDetailsService.deleteCourierDetails(courier_id);
	}
	//add req handling method(REST API endpoint) to get emp details by it's id
	@GetMapping("/{courier_id}")
	public CourierDetails getCourierDetails(@PathVariable long courier_id)
	{
		System.out.println("in get courier dtls");
		return courierDetailsService.getCourierDetails(courier_id);
	}
	
	//add req handling method(REST API endpoint) to update emp details
	@PostMapping("/allcourierbyid")
	public List<CourierDetails> getAllCourierDetailsByCustId(@RequestBody  CourierDetailsDto persistantCourierDetails)
	{
		
		System.out.println("in fetch all "+persistantCourierDetails);
		return courierDetailsService.getAllCourierDetailsByCustId(persistantCourierDetails.getCustId());
		
	}
	
	

}
