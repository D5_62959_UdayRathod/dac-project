package com.app.controller;

import java.util.List;
import java.util.Optional;

import org.apache.catalina.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.spi.MatchingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dto.AddressDto;
import com.app.pojos.Customer;
import com.app.pojos.DropAddress;
import com.app.pojos.Employee;
import com.app.service.ICustomerService;
import com.app.service.IDropAddressService;
import com.app.service.IEmployeeService;


@RestController //=@Controller + @ResponseBody
@RequestMapping("/api/dropaddress") //resource : class level pattern
@CrossOrigin(origins = "http://localhost:3000")
public class DropAddressController {
	//dep : service layer i/f
	@Autowired
	private IDropAddressService dropAddrService;
	@Autowired
	private ModelMapper mapper;
	@Autowired
	private ICustomerService custService;
	
	public DropAddressController() {
		System.out.println("in ctor of "+getClass());
	}
	//add req handling method(REST API endpoint) to ret list of all emps
	
	//add req handling method(REST API endpoint) to create a new resource : Emp
	@PostMapping
	public DropAddress insertDropAddrDetails(@RequestBody AddressDto transientDropAddr) {
		System.out.println("in add new dropaddr "+transientDropAddr);//id : null
		//mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
		DropAddress adr=mapper.map(transientDropAddr, DropAddress.class);
		System.out.println("it is before "+adr);
		Customer c1=custService.getCustomerDetails(transientDropAddr.getCust_id());
		adr.setCust_id(c1);
		
			System.out.println("it is after "+adr);
		return dropAddrService.insertDropAddrDetails(adr);
	}
	
	@PostMapping("/login")
	public List<DropAddress> getDropAddrDetailsByCustId(@RequestBody  AddressDto dropAddr)
	{
		System.out.println("in get  dropaddress "+dropAddr);//id : null
		
//		DropAddress adr=mapper.map(dropAddr, DropAddress.class);
//		System.out.println("it is before "+adr);
//		Customer c1=custService.getCustomerDetails(dropAddr.getCust_id());
//		adr.setCust_id(c1);
//		
//			System.out.println("it is after "+adr);
		
		 return  dropAddrService.getDropAddrDetails(dropAddr.getCust_id()) ;
	}

	//add req handling method(REST API endpoint) to delete emp details
	@DeleteMapping("/{drop_addr_id}")
	public String deleteDropAddrDetails(@PathVariable long drop_addr_id)
	{
		System.out.println("in del dropaddress dtls "+drop_addr_id);
		return dropAddrService.deleteDropAddrDetails(drop_addr_id);
	}
	
	//add req handling method(REST API endpoint) to get emp details by it's id
	@GetMapping("/{drop_addr_id}")
	public DropAddress  findByDropAddrId(@PathVariable long drop_addr_id)
	{
		System.out.println("in get dropaddress dtls "+drop_addr_id);
		//mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
		DropAddress d1 = dropAddrService.findByDropAddrId(drop_addr_id);
         
		//Address a1 = mapper.map(d1,Address.class);
//		System.out.println("   "+a1);
		return d1;
	}
	
	//add req handling method(REST API endpoint) to update emp details
		@PutMapping
		public DropAddress updateDropAddrDetails(@RequestBody AddressDto DropAddr)
		{
			DropAddress adr=mapper.map(DropAddr, DropAddress.class);
			System.out.println("it is before "+adr);
			Customer c1=custService.getCustomerDetails(DropAddr.getCust_id());
			
			adr.setCust_id(c1);
			
				System.out.println("it is after "+adr);
			
			System.out.println("in update  dropaddress dtls"+adr);//not null id
			return dropAddrService.updateDropAddrDetails(adr);
		}
	
	
	

}
