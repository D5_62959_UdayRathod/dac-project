package com.app.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.catalina.mapper.Mapper;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.spi.MatchingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dto.AddressDto;
import com.app.pojos.Customer;
import com.app.pojos.DropAddress;
import com.app.pojos.Employee;
import com.app.pojos.PickupAddress;
import com.app.service.ICustomerService;
import com.app.service.IDropAddressService;
import com.app.service.IEmployeeService;
import com.app.service.IPickUpAddressService;


@RestController //=@Controller + @ResponseBody
@RequestMapping("/api/pickupaddress") //resource : class level pattern
@CrossOrigin(origins = "http://localhost:3000")
public class PickUpAddressController {
	//dep : service layer i/f
	@Autowired
	private IPickUpAddressService pickupAddrService;
	@Autowired
	private ModelMapper mapper;
	@Autowired
	private ICustomerService custService;
	
	public PickUpAddressController() {
		System.out.println("in ctor of "+getClass());
	}
	//add req handling method(REST API endpoint) to ret list of all emps
	
	//add req handling method(REST API endpoint) to create a new resource : Emp
	@PostMapping
	public AddressDto insertPickupAddrDetails(@RequestBody AddressDto transientPickupAddr) {
		System.out.println("in add new Pickupaddr "+transientPickupAddr);//id : null
		//mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
		PickupAddress adr=mapper.map(transientPickupAddr, PickupAddress.class);
		System.out.println("it is before "+adr);
//		Customer c1=custService.getCustomerDetails(transientPickupAddr.getCust_id());
//		adr.setCust_id(c1);
		PickupAddress pickupAddr= pickupAddrService.insertPickupAddrDetails(adr);
		AddressDto pickupDto = mapper.map(pickupAddr,AddressDto.class);
			System.out.println("it is after "+pickupDto);
		return pickupDto;
	}
	
	@PostMapping("/login")
	public List<AddressDto> getPickupAddrDetailsByCustId(@RequestBody  AddressDto pickupAddr)
	{
		System.out.println("in get  pickupaddress "+pickupAddr);//id : null
		
		List<AddressDto> pAddrDto=new ArrayList<AddressDto>();
		List<PickupAddress> pickupAddrDetails = pickupAddrService.getPickupAddrDetails(pickupAddr.getCust_id()) ;
		System.out.println("it is before "+pickupAddrDetails.get(0));
		
		pickupAddrDetails.forEach(a->{
			AddressDto adrDto=mapper.map(a, AddressDto.class);
			pAddrDto.add(adrDto);
		});
		System.out.println("it is before "+pAddrDto.get(0));
     	
		
		 return  pAddrDto;
	}

	//add req handling method(REST API endpoint) to delete emp details
	@DeleteMapping("/{pickup_addr_id}")
	public String deleteDropAddrDetails(@PathVariable long pickup_addr_id)
	{
		System.out.println("in del dropaddress dtls "+pickup_addr_id);
		return pickupAddrService.deletePickupAddrDetails(pickup_addr_id);
	}
	
	//add req handling method(REST API endpoint) to get emp details by it's id
	@GetMapping("/{pickup_addr_id}")
	public AddressDto  findByPickupAddrId(@PathVariable long pickup_addr_id)
	{
		System.out.println("in get pickupaddress dtls "+pickup_addr_id);
		//mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
		PickupAddress d1 = pickupAddrService.findByPickupAddrId(pickup_addr_id);
         AddressDto adto = mapper.map(d1,AddressDto.class);
		//Address a1 = mapper.map(d1,Address.class);
//		System.out.println("   "+a1);
		return adto;
	}
	
	//add req handling method(REST API endpoint) to update emp details
		@PutMapping
		public AddressDto updatePickupAddrDetails(@RequestBody AddressDto pickupAddr)
		{
			PickupAddress adr=mapper.map(pickupAddr, PickupAddress.class);
			System.out.println("it is before "+adr);
			
			PickupAddress pdto = pickupAddrService.updatePickupAddrDetails(adr);
			AddressDto adto = mapper.map(pdto,AddressDto.class);
			
			System.out.println("in update  Pickupaddress dtls"+adr);//not null id
			return adto;
		}
	
	
	

}
