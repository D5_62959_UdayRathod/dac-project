package com.app.controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dto.CourierTrackingDto;
import com.app.pojos.CourierDetails;
import com.app.pojos.CourierTracking;
import com.app.pojos.Customer;
import com.app.pojos.DropAddress;
import com.app.pojos.Employee;
import com.app.pojos.PickupAddress;
import com.app.service.CourierTrackingServiceImpl;
import com.app.service.ICourierDetailsService;
import com.app.service.ICourierTrackingService;
import com.app.service.ICustomerService;
import com.app.service.IDropAddressService;
import com.app.service.IEmployeeService;
import com.app.service.IPickUpAddressService;


@RestController //=@Controller + @ResponseBody
@RequestMapping("/api/CourierTracking") //resource : class level pattern
@CrossOrigin(origins = "http://localhost:3000")
public class CourierTrackingController {
	//service layer i/f
	@Autowired
	private ICourierTrackingService courierTrackingService;
	@Autowired
	private ICustomerService custService;
	@Autowired
	private IDropAddressService dropAddrService;
	@Autowired
	private IPickUpAddressService pickupAddrService;
	@Autowired
	private ModelMapper mapper;
	@Autowired
	private ICourierDetailsService courierDetailsService;
	@Autowired
	private IEmployeeService employeeService;
	@Autowired
	private CourierTrackingServiceImpl courierImpl;
	
	public CourierTrackingController() {
		System.out.println("in ctor of "+getClass());
	}
	//add req handling method(REST API endpoint) to ret list of all emps
	@GetMapping("/getall")
	public List<CourierTrackingDto> getAllCourierTracking()
	{
		System.out.println("in fetch all ");
		List<CourierTrackingDto> dto = new ArrayList<>();
		List<CourierTracking> ct=courierTrackingService.getAllCourierTrackings();
		ct.forEach(c->{
		                           	dto.add(courierImpl.courierTrackingToCourierTrackingDto(c));
			                          
		                             });
		return dto;
		
	}
	
	
	
	//add req handling method(REST API endpoint) to create a new resource : 
	@PostMapping
	public CourierTrackingDto insertCourierTracking(@RequestBody  CourierTrackingDto transientCourierTrackingDto)
	{
		System.out.println("in add new CourierTracking " + transientCourierTrackingDto);

		CourierTracking ct1 = mapper.map(transientCourierTrackingDto, CourierTracking.class);
		System.out.println("in add new CourierTracking after" + ct1);
		Customer c1 = custService.getCustomerDetails(transientCourierTrackingDto.getCust_id());
		CourierDetails cd1 = courierDetailsService.getCourierDetails(transientCourierTrackingDto.getCourier_id());
		Employee e1 = employeeService.getEmpDetails(1);
		ct1.setCourierDetails(cd1);
		ct1.setEmployee(e1);
		ct1.setCustomer(c1);
		ct1.setOrderStatus("pending");
		CourierTracking ct=courierTrackingService.insertCourierTrackings(ct1);
		CourierTrackingDto ctd= courierImpl.courierTrackingToCourierTrackingDto(ct);
		
		System.out.println("in add new CourierTracking final"+ct1);//id : null
		return ctd;
	}
	
	
	
	//add req handling method(REST API endpoint) to delete emp details
	@DeleteMapping("/{tracking_id}")
	public String deleteCourierTracking(@PathVariable long tracking_id)
	{
		System.out.println("in del courier tracking dtls "+tracking_id);
		return courierTrackingService.deleteCourierTrackings(tracking_id);
	}
	//add req handling method(REST API endpoint) to get emp details by it's id
	@GetMapping("/{tracking_id}")
	public CourierTrackingDto getCourierTracking(@PathVariable long tracking_id)
	{
		System.out.println("in get courier tracking dtls");
          CourierTracking ct = courierTrackingService.getCourierTrackings(tracking_id);
  		return  courierImpl.courierTrackingToCourierTrackingDto(ct);
	}
	@GetMapping("/courierid/{courier_id}")
	public CourierTrackingDto getCourierTrackingByCourierId(@PathVariable long courier_id)
	{
		System.out.println("in get courier tracking dtls");
          CourierTracking ct = courierTrackingService.getCourierTrackingsByCourierId(courier_id);
  		return  courierImpl.courierTrackingToCourierTrackingDto(ct);
	}
	@GetMapping("/empid/{emp_id}")
	public List<CourierTrackingDto> getCourierTrackingByEmpId(@PathVariable long emp_id)
	{
		System.out.println("in get courier tracking dtls"+emp_id);
		List<CourierTrackingDto> ctd=new ArrayList<CourierTrackingDto>();
          List<CourierTracking> ct = courierTrackingService.getAllCourierTrackingsByEmpId(emp_id);
          System.out.println("in get courier tracking dtls "+ct);
          ct.forEach(s->{
        	 CourierTrackingDto cdto = courierImpl.courierTrackingToCourierTrackingDto(s); 
        	 ctd.add(cdto);
          });
          
  		return ctd;
	}
	
	
	//add req handling method(REST API endpoint) to update emp details
	@PostMapping("/allcouriertrackingbycustid")
	public List<CourierTrackingDto> getAllCourierTrackingByCustId(@RequestBody  CourierTrackingDto persistantCourierTracking)
	{
		
		System.out.println("in fetch all ");
		 
		List<CourierTrackingDto> dto = new ArrayList<>();
		List<CourierTracking> ct=courierTrackingService.getAllCourierTrackingsByCustId(persistantCourierTracking.getCust_id());
		ct.forEach(c->{
		                           	dto.add(courierImpl.courierTrackingToCourierTrackingDto(c));
			                          
		                             });
		return dto;
		
	}
	@PutMapping("/updatetracking")
	public CourierTrackingDto updateCourierTrackings(@RequestBody  CourierTrackingDto persistantCourierTrackingDto) {
		System.out.println("in update tracking before "+persistantCourierTrackingDto);
		CourierTracking ct1=mapper.map(persistantCourierTrackingDto, CourierTracking.class);
		System.out.println("in  update CourierTracking middle "+ct1);//id : null
		Customer c1=custService.getCustomerDetails(persistantCourierTrackingDto.getCust_id());
		CourierDetails cd1=courierDetailsService.getCourierDetails(persistantCourierTrackingDto.getCourier_id());
		Employee e1=employeeService.getEmpDetails(persistantCourierTrackingDto.getEmp_id());
	     ct1.setCourierDetails(cd1);
	     ct1.setEmployee(e1);
		ct1.setCustomer(c1);
		
		System.out.println("in  update CourierTracking after "+ct1);//id : null
	
		CourierTracking ct = courierTrackingService.updateCourierTrackings(ct1);
		
		return courierImpl.courierTrackingToCourierTrackingDto(ct);
	}
@PatchMapping
public  int updateStatus(@RequestBody  CourierTrackingDto persistantCourierTrackingDto) 
{
	String orderStatus = persistantCourierTrackingDto.getOrderStatus();
	Long tracking_id = persistantCourierTrackingDto.getTracking_id();
	Long emp_id=persistantCourierTrackingDto.getEmp_id();
	return courierTrackingService.updateStatus(orderStatus,emp_id,tracking_id);
}
}
