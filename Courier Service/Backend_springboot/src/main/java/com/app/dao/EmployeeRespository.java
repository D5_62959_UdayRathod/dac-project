package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.pojos.Employee;

public interface EmployeeRespository extends JpaRepository<Employee,Long> {

	@Query(value="select * from employee_tbl where designatation!='ROLE_MANAGER'",nativeQuery = true)
	 List<Employee> getAllEmployees();
	Employee findByEmailAndPassword(String email,String pasword);
	@Query(value = "select * from employee_tbl where emp_email=:email",nativeQuery = true)
	Employee findByEmail(@Param("email") String email);
	
}
