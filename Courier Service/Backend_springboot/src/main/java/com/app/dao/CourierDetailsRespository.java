package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.pojos.CourierDetails;
import com.app.pojos.Employee;

public interface CourierDetailsRespository extends JpaRepository<CourierDetails,Long> {

	@Query(value = "select * from courier_details_tbl where cust_id=:cust_id",nativeQuery = true)
	List<CourierDetails>  findAll(long cust_id);

	
	
}
