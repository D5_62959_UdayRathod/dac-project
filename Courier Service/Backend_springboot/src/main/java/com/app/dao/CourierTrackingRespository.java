package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.pojos.CourierDetails;
import com.app.pojos.CourierTracking;
import com.app.pojos.Employee;

public interface CourierTrackingRespository extends JpaRepository<CourierTracking,Long> {

	
@Query(value = "select * from courier_tracking_tbl where cust_id=:cust_id",nativeQuery = true)
public List<CourierTracking> findAll(long cust_id);
@Query(value = "select * from courier_tracking_tbl where emp_id=:emp_id",nativeQuery = true)
public List<CourierTracking> findAllByEmpId(long emp_id);
@Query(value = "select * from courier_tracking_tbl where courier_id=:courier_id",nativeQuery = true)
public CourierTracking getCourierTrackingsByCourierId(long courier_id);
	@Modifying
	@Query(value = "update courier_tracking_tbl set order_status=:orderStatus,emp_id=:emp_id where tracking_id=:tracking_id",nativeQuery = true)
	public int updateStatus(@Param("orderStatus") String orderStatus,@Param("emp_id") long emp_id ,@Param("tracking_id") long tracking_id );
	
}
