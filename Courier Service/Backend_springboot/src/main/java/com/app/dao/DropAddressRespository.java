package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.pojos.Customer;
import com.app.pojos.DropAddress;
import com.app.pojos.Employee;

public interface DropAddressRespository extends JpaRepository<DropAddress,Long> {
 
	@Query(value = "select * from drop_addr_tbl where cust_id=:custId",nativeQuery = true)
	List<DropAddress> findAll(Long custId);

	
	
}
