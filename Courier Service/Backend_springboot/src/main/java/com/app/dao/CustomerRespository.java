package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.pojos.Customer;
import com.app.pojos.Employee;

public interface CustomerRespository extends JpaRepository<Customer,Long> {

	@Query(value="select * from customer_tbl ",nativeQuery = true)
	 List<Customer> getAllCustomers();
	Customer findByEmail(String email);
}
