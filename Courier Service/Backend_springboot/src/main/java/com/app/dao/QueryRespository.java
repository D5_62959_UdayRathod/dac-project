package com.app.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.app.pojos.Customer;
import com.app.pojos.DropAddress;
import com.app.pojos.Employee;
import com.app.pojos.PickupAddress;
import com.app.pojos.Queries;

public interface QueryRespository extends JpaRepository<Queries,Long> {
 

	@Query(value = "select * from query_tbl where cust_id=:cust_id",nativeQuery = true)
	public List<Queries > getAllQueryByCustId(long cust_id);
	@Modifying
	@Query(value = "update query_tbl set query_status=:queryStatus where query_id=:query_id",nativeQuery = true)
	public int updateAnQuery(@Param("queryStatus") String queryStatus,@Param("query_id") long Query_id);
}
