package com.app.config;


import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.app.filters.JWTRequestFilter;
@EnableWebSecurity
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MyConfig {

	@Autowired
	private JWTRequestFilter filter;


	@Bean
	public BCryptPasswordEncoder passwordEncoder()
	{
		return new BCryptPasswordEncoder();
	}
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		http.cors().and().csrf().disable().
		exceptionHandling().
		authenticationEntryPoint((request, response, ex) -> {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, ex.getMessage());
		}).
		and().
		authorizeRequests()
		.antMatchers("/api/employees/getall").hasRole("MANAGER")
		.antMatchers("/api/queries/getall").hasRole("MANAGER")
		.antMatchers("/api/employees/*").permitAll()
		.antMatchers(HttpMethod.GET, "/api/CourierTracking/getall").hasRole("DELIVERY")
		.antMatchers("/api/CourierTracking/**").permitAll()
		.antMatchers("/api/courierdetails/**").permitAll()
		.antMatchers("/api/courierdetails/*").permitAll()
		.antMatchers("/api/pickupaddress/**").permitAll()
		.antMatchers("/api/dropaddress/**").permitAll()
		.antMatchers("/api/customers/**").permitAll()
		.antMatchers("/api/queries/**").permitAll()
		.antMatchers("/api/employees/**").hasRole("MANAGER")
		
		.antMatchers("/api/**").hasRole("DELIVERY")
		.antMatchers("/products/view", "/auth/**", "/swagger*/**", "/v*/api-docs/**").permitAll() // enabling global
																										// access to all
																										// urls with
																										// /auth
				// only required for JS clnts (react / angular)
		.antMatchers(HttpMethod.OPTIONS).permitAll().
		anyRequest().authenticated().
		and().
		sessionManagement()
		.sessionCreationPolicy(SessionCreationPolicy.STATELESS).
		and()
		.addFilterBefore(filter, UsernamePasswordAuthenticationFilter.class);

		return http.build();
	}

	// configure auth mgr bean : to be used in Authentication REST controller
	@Bean
	public AuthenticationManager authenticatonMgr(AuthenticationConfiguration config) throws Exception {
		return config.getAuthenticationManager();
	}
}
