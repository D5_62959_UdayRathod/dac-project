package com.app.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.EmployeeRespository;
import com.app.pojos.Employee;
@Service // or @Component also works!
@Transactional

public class EmployeeDetailsServiceImpl implements UserDetailsService{

	@Autowired
	private EmployeeRespository empRepo;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Employee emp = empRepo.findByEmail(username);
		if(emp==null)
			throw new UsernameNotFoundException("could not find employee");
		CustomEmployeeDetails customEmp=new CustomEmployeeDetails(emp);
		return customEmp;
	}

}
