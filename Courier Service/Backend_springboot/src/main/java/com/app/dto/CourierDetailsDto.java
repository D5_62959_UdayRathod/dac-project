package com.app.dto;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class CourierDetailsDto {
	
	private Long courierId;

	private Long custId;
	
	private Long pickUpAddrId;
	
	private Long dropAddrId;
	
	private String courierName;
	
	private Integer weight;
	
	private Double price;
	
	private LocalDate courierPickUpDate;
	
	private LocalDate dropDate;
	
}
