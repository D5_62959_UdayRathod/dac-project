package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dao.CourierDetailsRespository;
import com.app.dao.EmployeeRespository;
import com.app.pojos.CourierDetails;
import com.app.pojos.Employee;

@Service
@Transactional
public class CourierDetailsServiceImpl implements ICourierDetailsService {
	// dep : dao layer i/f
	@Autowired
	private CourierDetailsRespository courierDetailsRepo;

	@Override
	public List<CourierDetails> getAllCourierDetails() {
		// TODO Auto-generated method stub
		return courierDetailsRepo.findAll();
	}
	
	
	

	@Override
	public CourierDetails insertCourierDetails(CourierDetails transientCourierDetails) {
		// TODO Auto-generated method stub
		return courierDetailsRepo.save(transientCourierDetails);
	}// @Transaction method rets ---> tx boundary --> no run time exc --> tx.commit
		// --> hib performs auto dirty chking -->hib session.flush --> insert --> L1
		// cache destroyed , pooled out (Hikari) cn rets to db cp -> session closed!
		// Employee : DETACHED

	@Override
	public String deleteCourierDetails(long courier_id) {
		String mesg = "Deleting Courier details failed !!!!!";
		// if you want to confirm the id :
		if (courierDetailsRepo.existsById(courier_id)) {
			courierDetailsRepo.deleteById(courier_id);
			mesg = "Deleted Courier details of emp od " + courier_id;
		}
		return mesg;

	}
	
	@Override
	public CourierDetails getCourierDetails(long courier_id) {
		// TODO Auto-generated method stub
		return courierDetailsRepo.findById(courier_id).orElseThrow(() -> new ResourceNotFoundException("Invalid courier ID!!!!!!"));
	}


	@Override
	public List<CourierDetails> getAllCourierDetailsByCustId(long cust_id) {
		List<CourierDetails> e1 = courierDetailsRepo.findAll(cust_id);
		return e1;
	}

}
