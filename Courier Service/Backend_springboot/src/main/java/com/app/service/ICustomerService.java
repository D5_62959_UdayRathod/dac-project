package com.app.service;

import java.util.List;

import com.app.pojos.Customer;
import com.app.pojos.Employee;

public interface ICustomerService {

	List<Customer> getAllCustomers();
	Customer insertCustomerDetails(Customer transientEmp);
	String deleteCustomerDetails(long custId);
	Customer getCustomerDetails(long custId);
	Customer updateCustomerDetails(Customer detachedCustomer);
	Customer findByEmail(String email);
	
}
