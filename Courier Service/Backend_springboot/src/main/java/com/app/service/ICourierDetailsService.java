package com.app.service;

import java.util.List;

import com.app.pojos.CourierDetails;
import com.app.pojos.Customer;
import com.app.pojos.Employee;

public interface ICourierDetailsService {
	List<CourierDetails> getAllCourierDetails();
	CourierDetails insertCourierDetails(CourierDetails transientCourierDetails);
	String deleteCourierDetails(long courier_id);
	CourierDetails getCourierDetails(long courier_id);
	//CourierDetails updateCourierDetails(CourierDetails detachedCourierDetails);
	List<CourierDetails> getAllCourierDetailsByCustId(long cust_id);
	
}
