package com.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dao.DropAddressRespository;
import com.app.dao.EmployeeRespository;
import com.app.dao.PickUpAddressRespository;
import com.app.dto.AddressDto;
import com.app.pojos.DropAddress;
import com.app.pojos.Employee;
import com.app.pojos.PickupAddress;

@Service
@Transactional
public class PickUpAddressServiceImpl implements IPickUpAddressService {
	// dep : dao layer i/f
	@Autowired
	private PickUpAddressRespository pickupAddrAddrRepo;

	
	@Override
	public PickupAddress insertPickupAddrDetails(PickupAddress transientPickupAddr) {
		// TODO Auto-generated method stub
		return pickupAddrAddrRepo.save(transientPickupAddr);
	}// @Transaction method rets ---> tx boundary --> no run time exc --> tx.commit
		// --> hib performs auto dirty chking -->hib session.flush --> insert --> L1
		// cache destroyed , pooled out (Hikari) cn rets to db cp -> session closed!
		// Employee : DETACHED

	@Override
	public String deletePickupAddrDetails(long pickup_addr_id) {
		String mesg = "Deleting pickupAddr details failed !!!!!";
		// if you want to confirm the id :
		if (pickupAddrAddrRepo.existsById(pickup_addr_id)) {
			pickupAddrAddrRepo.deleteById(pickup_addr_id);
			mesg = "Deleted pickupAddr details of  DropAddr id " + pickup_addr_id;
		}
		return mesg;

	}

	@Override
	public List<PickupAddress> getPickupAddrDetails(Long custId) {
		// TODO Auto-generated method stub
		return pickupAddrAddrRepo.findAll(custId);
	}

	@Override
	public PickupAddress updatePickupAddrDetails(PickupAddress detachedPickupAddr) {
		pickupAddrAddrRepo.findById(detachedPickupAddr.getId()).orElseThrow(() -> new ResourceNotFoundException("Invalid PickupAddress ID!!!!!! : Can't UpdatePickupAddress details"));
		//=> valid emp id
		return pickupAddrAddrRepo.save(detachedPickupAddr);//update
	}

	@Override
	public PickupAddress  findByPickupAddrId(long pickup_addr_id) {
		PickupAddress d1 = pickupAddrAddrRepo.findById(pickup_addr_id).get();
		return d1;
	}
	public static AddressDto addressToAddressDto(PickupAddress pAddr)
	{
		AddressDto adto=new AddressDto();
		adto.setArea(pAddr.getArea());
		adto.setCity(pAddr.getCity());
		adto.setDistrict(pAddr.getDistrict());
		adto.setPincode(pAddr.getPincode());
		adto.setId(pAddr.getId());
		adto.setCust_id(pAddr.getCust_id().getCust_id());
		return adto;
	}
}
