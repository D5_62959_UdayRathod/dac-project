package com.app.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.app.pojos.CourierDetails;
import com.app.pojos.CourierTracking;
import com.app.pojos.Customer;
import com.app.pojos.Employee;

public interface ICourierTrackingService {
	List<CourierTracking> getAllCourierTrackings();
	CourierTracking insertCourierTrackings(CourierTracking transientCourierTrackings);
	String deleteCourierTrackings(long tracking_id);
	CourierTracking getCourierTrackings(long tracking_id);
	CourierTracking getCourierTrackingsByCourierId(long courier_id);
	CourierTracking updateCourierTrackings(CourierTracking detachedCourierTrackings);
	List<CourierTracking> getAllCourierTrackingsByCustId(long cust_id);
	List<CourierTracking> getAllCourierTrackingsByEmpId(long emp_id);
    int updateStatus(String orderStatus,long emp_id,long tracking_id );
}
