package com.app.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dao.CourierDetailsRespository;
import com.app.dao.CourierTrackingRespository;
import com.app.dao.EmployeeRespository;
import com.app.dto.CourierTrackingDto;
import com.app.pojos.CourierDetails;
import com.app.pojos.CourierTracking;
import com.app.pojos.DropAddress;
import com.app.pojos.Employee;

@Service
@Transactional
public class CourierTrackingServiceImpl implements ICourierTrackingService {
	// dep : dao layer i/f
	@Autowired
	private CourierTrackingRespository courierTrackingRepo;
	@Autowired
	private ModelMapper mapper;
	@Override
	public List<CourierTracking> getAllCourierTrackings() {
		// TODO Auto-generated method stub
		return courierTrackingRepo.findAll();
	}
	
	
	@Override
	public CourierTracking insertCourierTrackings(CourierTracking transientCourierTrackings) {
		// TODO Auto-generated method stub
		return courierTrackingRepo.save(transientCourierTrackings);
	}// @Transaction method rets ---> tx boundary --> no run time exc --> tx.commit
		// --> hib performs auto dirty chking -->hib session.flush --> insert --> L1
		// cache destroyed , pooled out (Hikari) cn rets to db cp -> session closed!
		// Employee : DETACHED

	@Override
	public String deleteCourierTrackings(long tracking_id) {
		String mesg = "Deleting Courier Trackings failed !!!!!";
		// if you want to confirm the id :
		if (courierTrackingRepo.existsById(tracking_id)) {
			courierTrackingRepo.deleteById(tracking_id);
			mesg = "Deleted Courier Trackings of tracking_id " + tracking_id;
		}
		return mesg;

	}

	
	@Override
	public CourierTracking updateCourierTrackings(CourierTracking detachedCourierTrackings) {
		courierTrackingRepo.findById(detachedCourierTrackings.getTracking_id()).orElseThrow(() -> new ResourceNotFoundException("Invalid tracking ID!!!!!! : Can't Update tracking"));
		//=> valid emp id
		return courierTrackingRepo.save(detachedCourierTrackings);//update
	}

	@Override
	public CourierTracking getCourierTrackings(long tracking_id) {
		// TODO Auto-generated method stub
		return courierTrackingRepo.findById(tracking_id).orElseThrow(() -> new ResourceNotFoundException("Invalid tracking ID!!!!!!"));
	}
	@Override
	public CourierTracking getCourierTrackingsByCourierId(long courier_id) {
		// TODO Auto-generated method stub
		return courierTrackingRepo.getCourierTrackingsByCourierId(courier_id);
	}

	@Override
	public List<CourierTracking> getAllCourierTrackingsByCustId(long cust_id) {
		List<CourierTracking> e1 = courierTrackingRepo.findAll(cust_id);
		return e1;
	}

	public CourierTrackingDto courierTrackingToCourierTrackingDto(CourierTracking ct)
	{
		//mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
		CourierTrackingDto ctd=mapper.map(ct,CourierTrackingDto.class);
		
		ctd.setCust_id(ct.getCustomer().getCust_id());
		ctd.setEmp_id(ct.getEmployee().getId());
		ctd.setCourier_id(ct.getCourierDetails().getCourierId());
		return ctd;
	}


	@Override
	public int updateStatus(String orderStatus, long emp_id,long tracking_id) {
		
		return courierTrackingRepo.updateStatus(orderStatus,emp_id, tracking_id);
	}


	@Override
	public List<CourierTracking> getAllCourierTrackingsByEmpId(long emp_id) {
		
		return courierTrackingRepo.findAllByEmpId(emp_id);
	}
	
}
