package com.app.service;

import java.util.List;
import java.util.Optional;

import com.app.pojos.Customer;
import com.app.pojos.Employee;

public interface IEmployeeService {
	List<Employee> getAllEmployees();
	Employee insertEmpDetails(Employee transientEmp);
	String deleteEmpDetails(long empId);
	Employee getEmpDetails(long empId);
	Employee updateEmpDetails(Employee detachedEmp);
	Employee findByEmail(String email);
	
}
//what is optional
//Every Java Programmer is familiar with NullPointerException. It can crash your code. 
//And it is very hard to avoid it without using too many null checks. So, to overcome this, 
//Java 8 has introduced a new class Optional in java.util package. It can help in writing a neat code without using too many null checks. 
//By using Optional, we can specify alternate values to return or alternate code to run. This makes the code more readable because 
//		the facts which were hidden are now visible to the developer.