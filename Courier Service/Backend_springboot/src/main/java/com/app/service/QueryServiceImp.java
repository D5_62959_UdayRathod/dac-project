package com.app.service;

import java.util.List;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.QueryRespository;
import com.app.dto.QueryDto;
import com.app.pojos.Queries;

@Service
@Transactional
public class QueryServiceImp implements IQueryService {

	@Autowired
	private QueryRespository queryRepo;
	@Autowired
	private ModelMapper mapper;
	@Override
	public List<Queries> getAllQueries() {
		 
		return queryRepo.findAll();
	}

	@Override
	public List<Queries> getAllQueryByCustId(long cust_id) {
		
		return queryRepo.getAllQueryByCustId(cust_id);
	}
	@Override
	public Queries insertAQuery(Queries q) {
		
		return queryRepo.save(q);
	}
	@Override
	public Queries updateAQuery(Queries q) {
		
		return queryRepo.save(q);
	}
	
	public QueryDto queriesToQueryDto(Queries q)
	{
		System.out.println("before moddel mapper "+q);
		QueryDto qdto=new QueryDto();
		qdto.setQuery_id(q.getQuery_id());
		qdto.setCust_id(q.getCust_id().getCust_id());
		qdto.setQuery(q.getQuery());
		qdto.setQueryStatus(q.getQueryStatus());
		qdto.setCourier_id(q.getCourier_id().getCourierId());
		System.out.println("after moddel mapper "+qdto);
	
		return qdto;
	}

	@Override
	public int updateAnQuery(String queryStatus,long query_id) {
		
		return queryRepo.updateAnQuery(queryStatus, query_id);
	}

	

	
}
