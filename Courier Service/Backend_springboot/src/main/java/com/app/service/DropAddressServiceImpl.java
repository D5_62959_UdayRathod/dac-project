package com.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dao.DropAddressRespository;
import com.app.dao.EmployeeRespository;
import com.app.dto.AddressDto;
import com.app.pojos.DropAddress;
import com.app.pojos.Employee;

@Service
@Transactional
public class DropAddressServiceImpl implements IDropAddressService {
	// dep : dao layer i/f
	@Autowired
	private DropAddressRespository dropAddrRepo;

	
	@Override
	public DropAddress insertDropAddrDetails(DropAddress transientDropAddr) {
		// TODO Auto-generated method stub
		return dropAddrRepo.save(transientDropAddr);
	}// @Transaction method rets ---> tx boundary --> no run time exc --> tx.commit
		// --> hib performs auto dirty chking -->hib session.flush --> insert --> L1
		// cache destroyed , pooled out (Hikari) cn rets to db cp -> session closed!
		// Employee : DETACHED

	@Override
	public String deleteDropAddrDetails(long drop_addr_id) {
		String mesg = "Deleting DropAddr details failed !!!!!";
		// if you want to confirm the id :
		if (dropAddrRepo.existsById(drop_addr_id)) {
			dropAddrRepo.deleteById(drop_addr_id);
			mesg = "Deleted DropAddr details of  DropAddr id " + drop_addr_id;
		}
		return mesg;

	}

	@Override
	public List<DropAddress> getDropAddrDetails(Long custId) {
		// TODO Auto-generated method stub
		return dropAddrRepo.findAll(custId);
	}

	@Override
	public DropAddress updateDropAddrDetails(DropAddress detachedDropAddr) {
		dropAddrRepo.findById(detachedDropAddr.getId()).orElseThrow(() -> new ResourceNotFoundException("Invalid DropAddress ID!!!!!! : Can't Update DropAddress details"));
		//=> valid emp id
		return dropAddrRepo.save(detachedDropAddr);//update
	}

	@Override
	public DropAddress  findByDropAddrId(long drop_addr_id) {
		DropAddress d1 = dropAddrRepo.findById(drop_addr_id).get();
		return d1;
	}

}
