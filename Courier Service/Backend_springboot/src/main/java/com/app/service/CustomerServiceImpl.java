package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dao.CustomerRespository;
import com.app.dao.EmployeeRespository;
import com.app.pojos.Customer;
import com.app.pojos.Employee;
@Service
@Transactional
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	private CustomerRespository custRepo;

	@Override
	public List<Customer> getAllCustomers() {
		// TODO Auto-generated method stub
		return custRepo.getAllCustomers();
	}

	@Override
	public Customer insertCustomerDetails(Customer transientCustomer) {
		// TODO Auto-generated method stub
		return custRepo.save(transientCustomer);
	}// @Transaction method rets ---> tx boundary --> no run time exc --> tx.commit
		// --> hib performs auto dirty chking -->hib session.flush --> insert --> L1
		// cache destroyed , pooled out (Hikari) cn rets to db cp -> session closed!
		// Employee : DETACHED

	@Override
	public String deleteCustomerDetails(long custId) {
		String mesg = "Deleting emp details failed !!!!!";
		// if you want to confirm the id :
		if (custRepo.existsById(custId)) {
			custRepo.deleteById(custId);
			mesg = "Deleted emp details of emp od " + custId;
		}
		return mesg;

	}

	@Override
	public Customer getCustomerDetails(long custId) {
		// TODO Auto-generated method stub
		return custRepo.findById(custId).orElseThrow(() -> new ResourceNotFoundException("Invalid Customer ID!!!!!!"));
	}

	@Override
	public Customer updateCustomerDetails(Customer detachedCustomer) {
		custRepo.findById(detachedCustomer.getCust_id()).orElseThrow(() -> new ResourceNotFoundException("Invalid Customer ID!!!!!! : Can't Update details"));
		//=> valid emp id
		return custRepo.save(detachedCustomer);//update
	}

	@Override
	public Customer findByEmail(String email) {
		Customer c1 = custRepo.findByEmail(email);
		return c1;
	}

}
