package com.app.service;

import java.util.List;
import java.util.Optional;

import com.app.dto.AddressDto;
import com.app.pojos.Customer;
import com.app.pojos.DropAddress;
import com.app.pojos.Employee;
import com.app.pojos.PickupAddress;

public interface IPickUpAddressService {
	
	PickupAddress insertPickupAddrDetails(PickupAddress transientPickupAddr);
	String deletePickupAddrDetails(long pickup_addr_id);
	List<PickupAddress> getPickupAddrDetails(Long custId);
	PickupAddress updatePickupAddrDetails(PickupAddress detachedPickupAddr);
	PickupAddress findByPickupAddrId(long pickup_addr_id);
	
}
