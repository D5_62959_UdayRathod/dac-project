package com.app.service;

import java.util.List;
import java.util.Optional;

import com.app.dto.AddressDto;
import com.app.pojos.Customer;
import com.app.pojos.DropAddress;
import com.app.pojos.Employee;

public interface IDropAddressService {
	
	DropAddress insertDropAddrDetails(DropAddress transientDropAddr);
	String deleteDropAddrDetails(long drop_addr_id);
	List<DropAddress> getDropAddrDetails(Long custId);
	DropAddress updateDropAddrDetails(DropAddress detachedDropAddr);
	DropAddress findByDropAddrId(long drop_addr_id);
	
}
