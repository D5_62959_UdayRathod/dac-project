package com.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dao.EmployeeRespository;
import com.app.pojos.Employee;

@Service
@Transactional
public class EmployeeServiceImpl implements IEmployeeService {
	// dep : dao layer i/f
	@Autowired
	private EmployeeRespository empRepo;

	@Override
	public List<Employee> getAllEmployees() {
		// TODO Auto-generated method stub
		return empRepo.getAllEmployees();
	}

	@Override
	public Employee insertEmpDetails(Employee transientEmp) {
		// TODO Auto-generated method stub
		return empRepo.save(transientEmp);
	}// @Transaction method rets ---> tx boundary --> no run time exc --> tx.commit
		// --> hib performs auto dirty chking -->hib session.flush --> insert --> L1
		// cache destroyed , pooled out (Hikari) cn rets to db cp -> session closed!
		// Employee : DETACHED

	@Override
	public String deleteEmpDetails(long empId) {
		String mesg = "Deleting emp details failed !!!!!";
		// if you want to confirm the id :
		if (empRepo.existsById(empId)) {
			empRepo.deleteById(empId);
			mesg = "Deleted emp details of emp od " + empId;
		}
		return mesg;

	}

	@Override
	public Employee getEmpDetails(long empId) {
		// TODO Auto-generated method stub
		return empRepo.findById(empId).orElseThrow(() -> new ResourceNotFoundException("Invalid Emp ID!!!!!!"));
	}

	@Override
	public Employee updateEmpDetails(Employee detachedEmp) {
		empRepo.findById(detachedEmp.getId()).orElseThrow(() -> new ResourceNotFoundException("Invalid Emp ID!!!!!! : Can't Update details"));
		//=> valid emp id
		return empRepo.save(detachedEmp);//update
	}

	@Override
	public Employee findByEmail(String email) {
		System.out.println(email);
		Employee e1 = empRepo.findByEmail(email);
		System.out.println(e1);
		return e1;
	}

}
