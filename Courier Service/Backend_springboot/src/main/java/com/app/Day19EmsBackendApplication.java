package com.app;


import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.app.dao.EmployeeRespository;
import com.app.pojos.Employee;

@SpringBootApplication
public class Day19EmsBackendApplication {
	
	
	public static void main(String[] args) {
		SpringApplication.run(Day19EmsBackendApplication.class, args);
		
		
	}
	@Bean
	public ModelMapper map()
	{
		return new ModelMapper();
	}
//	@Bean
//	public BCryptPasswordEncoder passwordEncoder()
//	{
//		return new BCryptPasswordEncoder();
//	}
}
