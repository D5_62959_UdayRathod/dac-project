import axios from 'axios';

export default axios.create({
  baseURL: 'http://localhost:8080/api/CourierTracking/',
  headers: {
    'Content-Type': 'application/json',
  },
});
