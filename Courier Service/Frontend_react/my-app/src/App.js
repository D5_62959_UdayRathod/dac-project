import { BrowserRouter, Route,Routes } from 'react-router-dom';
import EmployeeList from './components/EmployeesList';
import NotFound from './components/NotFound';
import 'bootstrap/dist/css/bootstrap.min.css';
import AddEmployee from './components/AddEmployee';
import Login from './components/EmpLogin';
import Header from './components/Header';
import Home from './components/Home';
import Track from './components/Track';
import ShowShipment from './components/ShowShipment';
import Courier from './components/Courier';
import CustLogin from './components/CustLogin';
import CustRegister from './components/CustRegister';
import CustDisplay from './components/CustDisplay';
import PickUpAddress from './components/Address/PickUpAddress';
import AddAddress from './components/Address/AddAddress';
import DropAddress from './components/Address/DropAddress';
import AddDropAddress from './components/Address/AddDropAddress';
import ViewCourier from './components/courier/ViewCourier';
import CourierForm from './components/courier/CourierForm';
import ViewQueries from './components/Query/ViewQueries';
import ViewAllQueries from './components/Query/ViewAllQueries';
import EmpDisplay from './components/Employee/EmpDisplay';
import EmpDispMenu from './components/Employee/EmpDispMenu';
import EmpCouriers from './components/Employee/EmpCouriers';
import EmpProfile from './components/Employee/EmpProfile';
import CustProfile from './components/CustProfile';
import CustProfileUpdate from './components/CustProfileUpdate';
import Footer from './components/Footer';

function App() {
  return (
    <BrowserRouter>
      <div>
        <div>
          <Header/>
          
          <Routes>
           
          <Route  path="/login" element={<Login/>} />
          <Route  path="/footer" element={<Footer/>} />
          <Route  path="/custprofileupdate/:id" element={<CustProfileUpdate/>} />
          <Route  path="/custprofile" element={<CustProfile/>} />
          <Route  path="/empprofile" element={<EmpProfile/>} />
          <Route  path="/empcouriers" element={<EmpCouriers/>} />
          <Route  path="/empdispmenu" element={<EmpDispMenu/>} />
          <Route  path="/empdisplay" element={<EmpDisplay/>} />
          <Route  path="/viewqueries" element={<ViewQueries/>} />
          <Route  path="/viewallqueries" element={<ViewAllQueries/>} />
          <Route  path="/courierform" element={<CourierForm/>} />
          <Route  path="/viewcouriers" element={<ViewCourier/>} />
          <Route  path="/addaddress" element={<AddAddress/>} />
          <Route  path="/addaddress/:id" element={<AddAddress/>} />
          <Route  path="/dropaddress" element={<DropAddress/>} />
          <Route  path="/adddropaddress" element={<AddDropAddress/>} />
          <Route  path="/adddropaddress/:id" element={<AddDropAddress/>} />
          <Route  path="/pickupaddress" element={<PickUpAddress/>} />
          <Route  path="/custdisplay" element={<CustDisplay/>} />
          <Route  path="/custregister" element={<CustRegister/>} />
          <Route  path="/custlogin" element={<CustLogin/>} />
          <Route  path="/courier/:courier_id" element={<Courier/>} />
          <Route  path="/showShipment/:trackingid" element={<ShowShipment/>} />
          <Route exact path="/" element={<Home/>} />
            <Route exact path="/a" element={<EmployeeList/>} />
            <Route path="/add" element={<AddEmployee/>} />
            <Route path="/employees/edit/:id" element={<AddEmployee/>} />
            <Route path="*" element={<NotFound/>} />
          </Routes>
        </div>
      </div>
      
    </BrowserRouter>
  );
}


export default App;
