import httpClient from '../http-common-query';

const getAll = (token) => {
  return httpClient.get('/getall',{ headers: {"Authorization" : `Bearer ${token}`} });
};

const create = (data) => {
  return httpClient.post('/insert', data);
};


const get = (custid) => {
  return httpClient.get(`${custid}`);
};


const update = (data) => {
  return httpClient.patch('', data);
};


const remove = (id) => {
  return httpClient.delete(`/${id}`);
};
export default { getAll, create, get, update, remove };
