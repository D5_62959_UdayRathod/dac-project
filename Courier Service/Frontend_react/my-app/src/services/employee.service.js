import httpClient from '../http-common';

const getAll = (token) => {
  return httpClient.get('/getall',{ headers: {"Authorization" : `Bearer ${token}`} });
};

const create = (data) => {
  return httpClient.post('', data);
};
const getByEmailAndPass = (data) => {
  return httpClient.post('/login', data);
};

const get = (id) => {
  return httpClient.get(`${id}`);
};

const update = (data) => {
  return httpClient.put('', data);
};

const remove = (id) => {
  return httpClient.delete(`/${id}`);
};
export default { getAll, create, get, update, remove,getByEmailAndPass };
