import httpClient from '../http-common-tracking';

const getAll = (token) => {
  return httpClient.get('/getall',{ headers: {"Authorization" : `Bearer ${token}`} });
};

const create = (data) => {
  return httpClient.post('', data);
};

const get = (id) => {
  return httpClient.get(`${id}`);
};
const getByCourierId = (id) => {
  return httpClient.get(`/courierid/${id}`);
};
const getByEmpId = (id) => {
  return httpClient.get(`/empid/${id}`);
};

const update = (data) => {
  return httpClient.put('', data);
};
const updatebypatch = (data) => {
  return httpClient.patch('', data);
};

const remove = (id) => {
  return httpClient.delete(`/${id}`);
};
export default { getAll, create, get,getByCourierId,getByEmpId, update,updatebypatch, remove };
