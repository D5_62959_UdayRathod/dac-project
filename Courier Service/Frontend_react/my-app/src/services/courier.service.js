import httpClient from '../http-common-courier'

const getAll = () => {
  return httpClient.get('');
};

const create = (data) => {
  return httpClient.post('', data);
};

const get = (id) => {
  return httpClient.get(`${id}`);
};

const getByCustId = (cust_id) => {
  return httpClient.post('/allcourierbyid', cust_id);
};

const remove = (id) => {
  return httpClient.delete(`/${id}`);
};
export default { getAll, create, get, getByCustId, remove };
