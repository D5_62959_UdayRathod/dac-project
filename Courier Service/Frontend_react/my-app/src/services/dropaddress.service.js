import httpClient from '../http-common-dropaddress';



const create = (data) => {
  return httpClient.post('', data);
};
const getByCustId = (data) => {
  return httpClient.post('/login', data);
};

const get = (dropid) => {
  return httpClient.get(`${dropid}`);
};

const update = (data) => {
  return httpClient.put('', data);
};

const remove = (dropid) => {
  return httpClient.delete(`/${dropid}`);
};
export default {create, get, update, remove,getByCustId };
