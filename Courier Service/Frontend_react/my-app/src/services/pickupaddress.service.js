import httpClient from '../http-common-pickupaddress';



const create = (data) => {
  return httpClient.post('', data);
};
const getByCustId = (data) => {
  return httpClient.post('/login', data);
};

const get = (pickupid) => {
  return httpClient.get(`${pickupid}`);
};

const update = (data) => {
  return httpClient.put('', data);
};

const remove = (pickupid) => {
  return httpClient.delete(`/${pickupid}`);
};
export default {create, get, update, remove,getByCustId };
