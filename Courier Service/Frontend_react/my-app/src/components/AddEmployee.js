import { useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { useEffect } from "react";
import employeeService from "../services/employee.service";

const AddEmployee = () => {
    const[firstName, setFirstname] = useState('');
    const[lastName, setLastname] = useState('');
    const[phone, setPhone] = useState('');
    const[email, setEmail] = useState('');
    const[password, setPassword] = useState('');
    const[designatation,setDesignatation]=useState('');
    const history = useNavigate();
    const {id} = useParams();

    const saveEmployee = (e) => {
        e.preventDefault();
        
        const employee = {firstName, lastName, phone, email,password,id,designatation};
        if (id) {
            //update
            employeeService.update(employee)
                .then(response => {
                    console.log('Employee data updated successfully', response.data);
                    history('/a');
                })
                .catch(error => {
                    console.log('Something went wrong', error);
                }) 
        } else {
            //create
            employeeService.create(employee)
            .then(response => {
                console.log("employee added successfully", response.data);
                history("/a");
            })
            .catch(error => {
                console.log('something went wroing', error);
            })
        }
    }

    useEffect(() => {
        if (id) {
            employeeService.get(id)
                .then(employee => {
                    setFirstname(employee.data.firstName);
                    setLastname(employee.data.lastName);
                    setPhone(employee.data.phone);
                    setEmail(employee.data.email);
                    setPassword(employee.data.password);
                    setDesignatation(employee.data.designatation)
                    
                })
                .catch(error => {
                    console.log('Something went wrong', error);
                })
        }
    },[])
    return(
        <div className="container">
            <h3>Add Employee</h3>
            <hr/>
            <form>
                <div className="form-group">
                    <input 
                        type="text" 
                        className="form-control col-4"
                        id="name"
                        value={firstName}
                        onChange={(e) => setFirstname(e.target.value)}
                        placeholder="Enter firstname"
                    />

                </div>
                <div className="form-group">
                    <input 
                        type="text" 
                        className="form-control col-4"
                        id="department"
                        value={lastName}
                        onChange={(e) => setLastname(e.target.value)}
                        placeholder="Enter lastname"
                    />

                </div>
                <div className="form-group">
                    <input 
                        type="text" 
                        className="form-control col-4"
                        id="location"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        placeholder="Enter email"
                    />
                </div>
                <div className="form-group">
                    <input 
                        type="text" 
                        className="form-control col-4"
                        id="phone"
                        value={phone}
                        onChange={(e) => setPhone(e.target.value)}
                        placeholder="Enter phone"
                    />
                </div>
                <div className="form-group">
                    <input 
                        type="text" 
                        className="form-control col-4"
                        id="designation"
                        value={designatation}
                        onChange={(e) => setDesignatation(e.target.value)}
                        placeholder="Enter designatation"
                    />
                </div>
                <div className="form-group">
                    <input 
                        type="text" 
                        className="form-control col-4"
                        id="password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        placeholder="Enter password"
                    />
                </div>
                <div >
                    <button onClick={(e) => saveEmployee(e)} className="btn btn-primary">Save</button>
                </div>
            </form>
            <hr/>
            <Link to="/a">Back to List</Link>
        </div>
    )
}

export default AddEmployee;