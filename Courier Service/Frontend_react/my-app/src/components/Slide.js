import React from 'react';
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css';
import './Slide.css'

const Slider = () => {
    const images = [
        "https://images.unsplash.com/photo-1509npm install --save styled-components721434272-b79147e0e708?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80",
        "https://images.unsplash.com/photo-1506710507565-203b9f24669b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1536&q=80",
        "https://images.unsplash.com/photo-1536987333706-fc9adfb10d91?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80",
    ];
    
    return (
        <Slide>
           
            <div className="each-slide-effect">
                <div style={{ 'backgroundImage': `url(${images[0]})` }}>
                <img src='https://img.freepik.com/free-vector/loading-workman-carrying-boxes_74855-14096.jpg?w=1000'></img>
                   {/* <span>Slide 1</span> */}
                </div>
            </div>
            <div className="each-slide-effect">
                <div style={{ 'backgroundImage': `url(${images[1]})` }}>
                <img src='https://thumbs.dreamstime.com/b/delivery-driver-checking-his-list-tablet-managers-large-warehouse-49902201.jpg'></img>
                    {/* <span>Slide 2</span> */}
                </div>
            </div>
            <div className="each-slide-effect">
                <div style={{ 'backgroundImage': `url(${images[2]})` }}>
                  <img src='https://i.pinimg.com/736x/76/08/8f/76088f2b0c1a09a971bd1cfbcc33d336.jpg'></img>
                    {/* <span>Slide 3</span> */}
                </div>
          </div>
          
        </Slide>
           );
 };
export default Slider;