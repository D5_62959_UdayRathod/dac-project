
import React from 'react'
import { NavLink } from 'react-router-dom'
import { Button, ListGroup, ListGroupItem } from 'reactstrap'

const CustDisplayMenu = () => {
 
  return (
    <div>
      <ListGroup >
      <ListGroupItem>
          <NavLink className="nav-link" >
            <h5>
              <i className="fa fa-dashboard"></i>
              <p> DashBoard</p>
            </h5>
          </NavLink>
        </ListGroupItem>
        
      <ListGroupItem
          style={{
            display: 'flex',
            justifyContent: 'left',
          }}>
          <NavLink className="nav-link" exact to="/pickupaddress">
            
            <Button outline color="info" >
              PickUp Address<br></br>
              <i className="fa fa-user-circle fa-2x" aria-hidden="true"></i>
            </Button>
          </NavLink>
        </ListGroupItem>

        <ListGroupItem>
          <NavLink className="nav-link" exact to="/dropaddress">
            <Button outline color="secondary">
              Drop Address<br></br>
              <i className="fa fa-users fa-2x" aria-hidden="true"></i>
            </Button>
          </NavLink>
        </ListGroupItem>

        <ListGroupItem
          style={{
            display: 'flex',
            justifyContent: 'left',
          }}>
          <NavLink className="nav-link" exact to="/viewcouriers">
            <Button outline color="info">
              View Your Couriers <br></br>
              <i className="fa fa-user-circle fa-2x" aria-hidden="true"></i>
            </Button>
          </NavLink>
        </ListGroupItem>

       

        

        <ListGroupItem>
          <NavLink className="nav-link" exact to="/viewqueries">
            <Button outline color="secondary">
            View Queries
            <i className="fa fa-volume-control-phone fa-3x"></i>
            </Button>
          </NavLink>
        </ListGroupItem>

      
    
        <ListGroupItem>
          <NavLink className="nav-link" exact to="/custprofile">
            <Button outline color="danger">
            View Profile<i className="fa fa-cog fa-spin fa-3x fa-fw"></i>
            </Button>
          </NavLink>
        </ListGroupItem>

      </ListGroup>
    </div>
  )
}

export default CustDisplayMenu
 