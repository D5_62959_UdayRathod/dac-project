import React, { useState } from "react";

import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";
import employeeService from "../services/employee.service";
import { useNavigate } from "react-router-dom";



function Login() {
  // React States
  const [errorMessages, setErrorMessages] = useState({});
  const [isSubmitted, setIsSubmitted] = useState(false);
  const history = useNavigate();
  // User Login info
 

  const errors = {
    uname: "invalid username",
    pass: "invalid password"
  };

  const handleSubmit = (event) => {
    //Prevent page reload
    event.preventDefault();

    const { uname, pass } = document.forms[0];
    const email=uname.value;
    const password=pass.value;
    const employee={email,password}
    console.log(employee)
    employeeService.getByEmailAndPass(employee).then(e=>{
      
      if (e.data) {
        console.log(e.data.jwt)
        sessionStorage.setItem("empjwt",`${e.data.jwt}`)
        if (e.data.password !==password) {
          //Invalid password
          setErrorMessages({ name: "pass", message: errors.pass });
        } 
        else {
         const emp_id=`${e.data.id}`
         console.log(emp_id)
          sessionStorage.setItem("emp",emp_id)
          setIsSubmitted(true);
          if(e.data.designatation==='ROLE_MANAGER')
      history('/a');
      else
      history('/empdispmenu')
       }
      } else {
        // Username not found
        setErrorMessages({ name: "uname", message: errors.uname });
      }
      
    })
    

    //Compare user info
   
  };

  // Generate JSX code for error message
  const renderErrorMessage = (name) =>
    name === errorMessages.name && (
      <div className="error">{errorMessages.message}</div>
    );

  // JSX code for login form
  const renderForm = (
    <div className="form">
      <form onSubmit={handleSubmit}>
        <div className="input-container">
          <label>Username </label>
          <input type="text" name="uname" required />
          {renderErrorMessage("uname")}
        </div>
        <div className="input-container">
          <label>Password </label>
          <input type="password" name="pass" required />
          {renderErrorMessage("pass")}
        </div>
        <div className="button-container">
          <input type="submit" />
        </div>
      </form>
    </div>
  );

  return (
    <div className="app">
      <div className="login-form">
        <div className="title">Sign In</div>
        {isSubmitted ? <div>User is successfully logged in</div> : renderForm}
      </div>
    </div>
  );
}

export default Login;