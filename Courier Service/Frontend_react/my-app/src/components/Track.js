import { useState } from "react";
import ShowShipment from './ShowShipment';
import {useNavigate,Link} from "react-router-dom"
import courierService from "../services/courierTracking.service";
import employeeService from "../services/employee.service";

function Track(){
    const[trackingid, setTrackingid] = useState('');
    const[employeeid, setEmployeeid] = useState('');
    let history=useNavigate()
   
return(
    <section className="vh-100" style={{backgroundColor: "#9A616D"}}>
    <div className="container py-5 h-100 ">
    
      <div className="row d-flex justify-content-center align-items-center h-100">
      
        <div className="col col-xl-10">
        <h1><b><i>Courier Service Management</i></b></h1>
          <div className="card" style={{borderRadius: "1rem"}}>
            <div className="row g-0" >
              <div className="col-md-6 col-lg-5 d-none d-md-block">
                <img src="https://media.istockphoto.com/photos/modern-courier-delivery-at-home-shopogolic-and-online-shopping-picture-id1287828123?k=20&amp;m=1287828123&amp;s=612x612&amp;w=0&amp;h=vNEUWHMCqPWr5ceGY_M2P6sJOgZkewX8Y-D8dnqZvz0="
                  alt="login form" className="img-fluid" style={{borderRadius: "1rem 0 0 1rem"}} />
              </div>
              <div className="col-md-6 col-lg-7 d-flex align-items-center" >
                <div className="card-body p-4 p-lg-5 text-black">
  
                  <form >
  
                    <div className="d-flex align-items-center mb-3 pb-1">
                      <i className="fas fa-cubes fa-2x me-3" style={{color: "#ff6219"}}></i>
                      <span className="h1 fw-bold mb-0">Track Your Shipment</span>
                    </div>
  
  
                    <div className="form-outline mb-4">
                      <input type="text" value={trackingid} onChange={(e) => setTrackingid(e.target.value)} id="form2Example17" className="form-control form-control-lg" placeholder="Enter your shipment no" />
                   
                    </div>
  
                   
  
                    <div className="pt-1 mb-4">
                      {/* <button onClick={routeChange} className="btn btn-info btn-lg btn-block" type="button" >Track Your Shipment</button> */}
                     <Link  className="btn btn-info btn-lg btn-block" to={`/showShipment/${trackingid}`}  >Track Your Shipment</Link>
                     
                    </div>
  
                    
                  </form>
  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

)
}
export default Track;