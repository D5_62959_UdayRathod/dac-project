
import { useEffect, useState } from 'react';
import queryservice from "../../services/query.service"
import { Link } from 'react-router-dom';
import ShowShipment from './../ShowShipment';
import Courier from './../Courier';
import AddQuery from './AddQuery';
import CustomerInQuery from './../Admin/CustomerInQuery';
import courierTrackingService from '../../services/courierTracking.service';
import Emp from './../Emp';
import UpdateQuery from './UpdateQuery';
function ViewAllQueries(){
   const[query,setQuery] = useState([]);
   const[data,setData]=useState('')
   const[courier_id,setCourier_id]=useState('')
   const[emp_id,setEmp_id]=useState('')
   const[query_id,setQuery_id]=useState('')
   const[customer_id,setCustomer_id]=useState('')
    const init = () => {
      const token=sessionStorage.getItem("empjwt")
        // courierTracking.get(trackingid)
        queryservice.getAll(token)
          .then(response => {
            console.log('Printing employees data', response.data);
            setQuery(response.data)
           
          })
          
          .catch(error => {
            console.log('Something went wrong', error);
          })
          
          
      }
      
      useEffect(() => {
        init();
       
      }, []);
      
     const trackinginfo=(courier_id)=>{
        
        courierTrackingService.getByCourierId(courier_id)
        .then(response => {
          console.log('Printing courier data', response.data);
          setCustomer_id(response.data.cust_id)
          setData('customer');
        })
        
        .catch(error => {
          console.log('Something went wrong', error);
        })
     }
     const empinfo=(courier_id)=>{
        
      courierTrackingService.getByCourierId(courier_id)
      .then(response => {
        console.log('Printing courier again data', response.data);
        setEmp_id(response.data.emp_id)
        setData('emp');
      })
      
      .catch(error => {
        console.log('Something went wrong', error);
      })
   }
   const queryinfo=(query_id)=>{
    setQuery_id(query_id);
    console.log(query_id)
    setData('addquery')

   }
return(
    <div>
<table className="table table-bordered table-striped">
          <thead className="thead-dark">
            <tr>
          

              <th>QueryDetails</th>
              <th>QueryStatus </th>
              <th>CourierDetails </th>
              <th>CustomerDetails </th>
              <th>EmployeeDetails </th>
              <th>actions </th>
              
            </tr>
          </thead>
          <tbody>
           
             {
                query.map(q => (
                  <tr key={q.query_id}>
                <td>{q.query}</td>
                <td>{q.queryStatus}</td>
                
                <td><button className="btn btn-primary" onClick={()=>{setData('track');setCourier_id(q.courier_id)}}>View Details</button> </td>
                <td><button className="btn btn-primary" onClick={()=>{trackinginfo(q.courier_id);}}>View Details</button> </td>
                
                <td><button className="btn btn-primary" onClick={()=>{empinfo(q.courier_id)}}>View Details</button> </td>
                
                <td><button className="btn btn-info" onClick={()=>{queryinfo(q.query_id)}}>Update</button> </td>

                
              </tr>
            ))}
                
          </tbody>
        </table>
        {data==='track' && <Courier courier_id={courier_id}/> }
        {data==='customer' && <CustomerInQuery customer_id={customer_id}/> }
        {data==='emp' && <Emp emp_id={emp_id}/> }
        {data==='addquery' && <UpdateQuery query_id={query_id}/> }
        <Link to="/a" >Back</Link>
    </div>
)
}
export default ViewAllQueries;