import { useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { useEffect } from "react";

import queryService from "../../services/query.service";
import courierService from "../../services/courier.service";

const UpdateQuery = (props) => {
    const navigate=useNavigate()
    const[queryStatus, setQueryStatus] = useState('');
    
    
    
    const id = props.query_id
    console.log(id)
   const init=()=>{
   
    
   }
    
    const saveQuery = (e) => {
        e.preventDefault();
        
        
        console.log(id)

        const query = {queryStatus:queryStatus,query_id:id};
        
            
            queryService.update(query)
                .then(response => {
                    console.log('query data added successfully', response.data);
                    window.location.reload('/viewallqueries')
                    
                })
                .catch(error => {
                    console.log('Something went wrong', error);
                }) 
        
    }

    useEffect(() => {
        
        init();}
    ,[props])
    return(
        <div className="container">
            <h3>Add Employee</h3>
            <hr/>
            <form>
                <div className="form-group">
                    <input 
                        type="text" 
                        className="form-control col-4"
                        id="name"
                        
                        onChange={(e) => setQueryStatus(e.target.value)}
                        placeholder="Enter status"
                    />

                </div>
                
                <div >
                    <button onClick={(e) => saveQuery(e)} className="btn btn-primary">Save</button>
                </div>
            </form>
            <hr/>
           
        </div>
    )
}

export default UpdateQuery;