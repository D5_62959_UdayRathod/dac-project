
import { useEffect, useState } from 'react';
import queryservice from "../../services/query.service"
import { Link } from 'react-router-dom';
import ShowShipment from './../ShowShipment';
import Courier from './../Courier';
import AddQuery from './AddQuery';
import Sidebar from './../Sidebar';
function ViewQueries(){
   const custid= JSON.parse(sessionStorage.getItem("customer")).cust_id
   const[query,setQuery] = useState([]);
   const[data,setData]=useState('')
   const[courier_id,setCourier_id]=useState('')
    const init = () => {
        // courierTracking.get(trackingid)
        queryservice.get(custid)
          .then(response => {
            console.log('Printing employees data', response.data);
            setQuery(response.data)
           
          })
          
          .catch(error => {
            console.log('Something went wrong', error);
          })
          
          
      }
      
      useEffect(() => {
        init();
       
      }, []);
      
   
    
return(
  <div style={{display:'flex'}}>
  <div><Sidebar/></div>
       <div> <button  onClick={()=>{setData('addquery');}} className="btn btn-warning mb-2">Send New Query</button>
       </div><table className="table table-bordered table-striped">
          <thead className="thead-dark">
            <tr>
          

              <th>QueryDetails</th>
              <th>QueryStatus </th>
              <th>CourierDetails </th>
              
            </tr>
          </thead>
          <tbody>
           
             {
                query.map(q => (
                  <tr key={q.query_id}>
                <td>{q.query}</td>
                <td>{q.queryStatus}</td>
                
                <td><button className="btn btn-primary" onClick={()=>{setData('track');setCourier_id(q.courier_id)}}>CourierDetails</button> </td>
                
                
              </tr>
            ))}
                
          </tbody>
        </table>
        {data==='track' && <Courier courier_id={courier_id}/> }
        {data==='addquery' && <AddQuery courier_id={courier_id}/> }
        <Link to="/custdisplay" >Back</Link>
    </div>
)
}
export default ViewQueries;