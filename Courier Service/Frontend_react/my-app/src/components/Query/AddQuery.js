import { useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { useEffect } from "react";

import queryService from "../../services/query.service";
import courierService from "../../services/courier.service";

const AddQuery = (props) => {
    const[queryDetails, setQueryDetails] = useState('');
    const[courierDetails, setCourierDetails] = useState('');
    const[courier, setCourier] = useState([]);
    const[cust_id, setCust_id] = useState('');
    
    const history = useNavigate();
    const id = props.query_id
   const init=()=>{
   
    const cid=JSON.parse(sessionStorage.getItem("customer")).cust_id
    setCust_id(cid)
    const cust={custId:cid}
    console.log(cust)
    courierService.getByCustId(cust)
    .then(response => {
        console.log("query retrived successfully", response.data);
        setCourier(response.data)
    })
    .catch(error => {
        console.log('something went wroing', error);
    })
   }
    
    const saveQuery = (e) => {
        e.preventDefault();
        const courier_id=courierDetails.target.value
        console.log(cust_id)
        const query = {query:queryDetails,courier_id,id,cust_id};
        if (id) {
            //update
            queryService.update(query)
                .then(response => {
                    console.log('query data added successfully', response.data);
                    history('/a');
                })
                .catch(error => {
                    console.log('Something went wrong', error);
                }) 
        } else {
            //create
            queryService.create(query)
            .then(response => {
                console.log("query data added successfully", response.data);
                history("/custdisplay");
            })
            .catch(error => {
                console.log('something went wroing', error);
            })
            
        }
    }

    useEffect(() => {
        if (id) {
            queryService.get(id)
                .then(employee => {
                    setQueryDetails(employee.data.query);
                    setCourierDetails(employee.data.queryStatus);
                   
                    
                })
                .catch(error => {
                    console.log('Something went wrong', error);
                })
        };init();
    },[props])
    return(
        <div className="container">
            <h3>Add Employee</h3>
            <hr/>
            <form>
                <div className="form-group">
                    <input 
                        type="text" 
                        className="form-control col-4"
                        id="name"
                        value={queryDetails}
                        onChange={(e) => setQueryDetails(e.target.value)}
                        placeholder="Enter queryDetails"
                    />

                </div>
                <div className="input_field select_option">
                <select name="pickupAddress" onChange={e=>{setCourierDetails(e)}}>
                <option>Select courier whose regarding query has to be raised</option>
                    {
                        courier.map(a=>{
                                   return <option value={a.courierId} >{a.courierName}|{a.weight}|{a.price}|{a.courierPickUpDate}|{a.dropDate}</option>
                        })
                    }
               </select>
                <div className="select_arrow"></div>
              </div>
                <div >
                    <button onClick={(e) => saveQuery(e)} className="btn btn-primary">Save</button>
                </div>
            </form>
            <hr/>
           
        </div>
    )
}

export default AddQuery;