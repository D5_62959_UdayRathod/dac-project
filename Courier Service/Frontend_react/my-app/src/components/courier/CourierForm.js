import './CourierFormcss.css'
import  pickupaddressservice from "../../services/pickupaddress.service";
import { useEffect,useState } from 'react';
import dropaddressService from "../../services/dropaddress.service";
import courierService from "../../services/courier.service"
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import Sidebar from './../Sidebar';
function CourierForm(){
    const navigate=useNavigate()
const cust_id=JSON.parse(sessionStorage.getItem("customer")).cust_id
console.log(cust_id)
const data={cust_id:cust_id}
const[pickupaddress,setPickupaddress] = useState([])
const[pickupaddressId,setPickupaddressId] = useState('')
const[dropaddress,setDropaddress] = useState([])
const[dropaddressId,setDropaddressId] = useState('')
const[weight,setWeight] = useState('')

    const init = () => {
        // courierTracking.get(trackingid)
        pickupaddressservice.getByCustId(data)
          .then(response => {
            console.log('Printing Address*************** data', response.data);
            setPickupaddress(response.data)
          })
          
          .catch(error => {
            console.log('Something went wrong', error);
          })
          dropaddressService.getByCustId(data)
          .then(response => {
            console.log('Printing Address*************** data', response.data);
            setDropaddress(response.data)
          })
          
          .catch(error => {
            console.log('Something went wrong', error);
          })
          
      }
      
    
      useEffect(() => {
        init();
       
      },[]);
 function submithandle(event)
{
    event.preventDefault()
    
    const { courierName,weight,price,pickupDate,pickupAddress,dropAddress } = document.forms[0];
    const courierDetails={courierName:courierName.value,weight:weight.value,
        price:price.value,courierPickUpDate:pickupDate.value,pickUpAddrId:pickupaddressId.target.value,
        dropAddrId:dropaddressId.target.value,custId:cust_id}
    console.log(courierDetails)
    courierService.create(courierDetails)
          .then(response => {
            console.log('Printing added courier*************** data', response.data);
            alert("sent request successfully")
            navigate('/custdisplay')
          })
          
          .catch(error => {
            console.log('Something went wrong', error);
          })
}
return(
  <div style={{display:'flex'}}>
  <div><Sidebar/></div>
        <div className="container-fluid px-1 py-5 mx-auto">
    <div className="row d-flex justify-content-center">
        <div className="col-xl-7 col-lg-8 col-md-9 col-11 text-center">
            <h3>Request a Demo</h3>
            <p className="blue-text">Just answer a few questions<br/> so that we can personalize the right experience for you.</p>
            <div className="card">
                <h5 className="text-center mb-4">Powering world-className companies</h5>
                <form className="form-card" onSubmit={submithandle}>
                    <div className="row justify-content-between text-left">
                        <div className="form-group col-sm-6 flex-column d-flex "> <label className="form-control-label ">Courier name<span className="text-danger"> *</span></label> <input type="text" id="fname" name="courierName" placeholder="Enter courier name" /> </div>
                        <div className="form-group col-sm-6 flex-column d-flex"> <label className="form-control-label px-3">Weight<span className="text-danger"> *</span></label> <input type="number" value={weight} onChange={(e) => setWeight(e.target.value)} id="lname" name="weight" placeholder="Enter weight of courier.(gm)" /> </div>
                    </div>
                    <div className="row justify-content-between text-left">
                        <div className="form-group col-sm-6 flex-column d-flex"> <label className="form-control-label px-3">Amount<span className="text-danger"> *</span></label> <input type="number" value={50+weight*0.1} id="mob" name="price" placeholder="" /> </div>
                        <div className="form-group col-sm-6 flex-column d-flex"> <label className="form-control-label px-3">PickUp Date<span className="text-danger"> *</span></label> <input type="date" id="email" name="pickupDate" placeholder="Pickup date" /> </div>
                    
                    
                   
                    </div>
                    <div className="input_field select_option">
                <select name="pickupAddress" onChange={e=>{setPickupaddressId(e)}}>
                <option>Select Pickup Address</option>
                    {
                        pickupaddress.map(a=>{
                                   return <option value={a.id} >Area:{a.area},City:{a.city},District{a.district},Pincode:{a.pincode}</option>
                        })
                    }
                  
                </select>
                <div className="select_arrow"></div>
              </div>
              <div className="input_field select_option">
                <select name="dropAddress" onChange={e=>{setDropaddressId(e)}}>
                  <option>Select Drop Address</option>
                  {
                        dropaddress.map(a=>{
                                   return <option value={a.id}>Area:{a.area},City:{a.city},District{a.district},Pincode:{a.pincode}</option>
                        })
                    }
                </select>
                <div className="select_arrow"></div>
              </div>
                    <div className="row justify-content-end">
                        <div className="form-group col-sm-6"> <button type="submit" className="btn block btn btn-primary">Send Request </button> </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
    </div>
)
}
export default CourierForm