import { useState } from "react";
import {  useParams,Link } from "react-router-dom";
import React from "react";

import { useEffect } from "react";

import employeeService from "../../services/employee.service";
import courierTracking from "../../services/courierTracking.service";
import courierservice from "../../services/courier.service";
import DropAddress from "../Address/DropAddress";
import PickUpAddress from "../Address/PickUpAddress";
import Track from './../Track';
import ShowShipment from './../ShowShipment';
import CourierDropAddress from './../Address/CourierDropAddress';
import CourierPickUpAddress from './../Address/CourierPickUpAddress';
import Sidebar from './../Sidebar';

function ViewCouier(){
    const cust = JSON.parse(sessionStorage.getItem("customer"));
    const custid={custId:cust.cust_id}
    console.log(custid)
    const[employee,setEmployee] = useState('');
    const[courier,setCourier] = useState([])
   const[courier_id,setCourier_id]=useState('')
   const[pickup_id,setPickup_id]=useState('')
   const[drop_id,setDrop_id]=useState('')
   const[data,setData]=useState('')
    const init = () => {
      // courierTracking.get(trackingid)
      courierservice.getByCustId(custid)
        .then(response => {
          console.log('Printing employees data', response.data);
          
          setCourier(response.data);
          
          //employeeService.get(response.data.emp_id).then(e=>{setEmployee(e.data)});
         
        })
        
        .catch(error => {
          console.log('Something went wrong', error);
        })
        
         
        
    }
    
      
      
    
  
    useEffect(() => {
      init();
     
    }, []);
    
  
     
     return(
      <div style={{display:'flex'}}>
      <div><Sidebar/></div>
            <table className="table table-bordered table-striped">
          <thead className="thead-dark">
            <tr>
          

              <th>courierName</th>
              <th>PickUpDate </th>
              <th>expected_drop_date </th>
              <th>weight </th>
              <th>price </th>
              <th>pickup_address </th>
              <th>drop_address </th>
              <th>TrackYourCourier</th>
            </tr>
          </thead>
          <tbody>
           
             {
                courier.map(courier => (
                  <tr key={courier.courier_id}>
                <td>{courier.courierName}</td>
                <td>{courier.courierPickUpDate}</td>
                <td>{courier.dropDate}</td>
                <td>{courier.weight}</td>
                <td>{courier.price}</td>
                <td><button className="btn btn-primary" onClick={()=>{setData('pickup');
                setPickup_id(courier.pickAddr.id)}}>pickup_address</button> </td>
                
                <td><button className="btn btn-warning" onClick={()=>{setData('drop');
            setDrop_id(courier.dropAddr.id)}}>drop_address</button> </td>
                <td><button className="btn btn-success" onClick={()=>{setData('track');setCourier_id(courier.courierId)}}>Track</button> </td>
                
              </tr>
            ))}
                
          </tbody>
        </table>
        {data==='drop' && <CourierDropAddress drop_id={drop_id}/>}
        {data==='pickup' && <CourierPickUpAddress pickup_id={pickup_id}/> }
        {data==='track' && <ShowShipment courierId={courier_id} /> }
        <Link to="/custdisplay" >Back</Link>
        </div>
     )
     
}
export default ViewCouier;