import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import employeeService from '../services/employee.service';
import { useNavigate } from 'react-router-dom';

const EmployeeList = () => {

  const [employees, setEmployees] = useState([]);
const navigate=useNavigate()
  const init = () => {
    const token=sessionStorage.getItem("empjwt")
    console.log(token)
    employeeService.getAll(token)
      .then(response => {
        console.log('Printing employees data', response.data);
        setEmployees(response.data);
      })
      .catch(error => {
        console.log('Something went wrong', error);
      }) 
  }

  useEffect(() => {
    init();
  }, []);

  const handleDelete = (id) => {
    console.log('Printing id', id);
    employeeService.remove(id)
      .then(response => {
        console.log('employee deleted successfully', response.data);
        init();
      })
      .catch(error => {
        console.log('Something went wrong', error);
      })
  }
const logout=()=>{
  sessionStorage.removeItem("empjwt")
navigate("/")
}
  return (
    <div className="container">
      <h3>List of Employees</h3>
      <hr/>
      <div>
        <Link to="/add" className="btn btn-primary mb-2">Add Employee</Link>
        <Link to="/viewallqueries" style={{marginLeft:"100px"}} className="btn btn-warning mb-2">View Customer Queries</Link>
        
        
        <table className="table table-bordered table-striped">
          <thead className="thead-dark">
            <tr>
              <th>firstName</th>
              <th>lastname</th>
              <th>phone</th>
              <th>email</th>
              
              <th>designation</th>
              <th>actions</th>
            </tr>
          </thead>
          <tbody>
          {
            employees.map(employee => (
              <tr key={employee.id}>
                <td>{employee.firstName}</td>
                <td>{employee.lastName}</td>
                <td>{employee.phone}</td>
                <td>{employee.email}</td>
              
                <td>{employee.designatation}</td>
                <td>
                  <Link className="btn btn-info" to={`/employees/edit/${employee.id}`}>Update</Link>
                  
                  <button className="btn btn-danger ml-2" onClick={() => {
                    handleDelete(employee.id);
                  }}>Delete</button>
                </td>
              </tr>
            ))
          }
          </tbody>
        </table>
        <button className="btn btn-warning" onClick={logout}>Logout</button>
      </div>
    </div>
  );
}

export default EmployeeList;
