import { Link ,useNavigate} from 'react-router-dom';
import { useEffect, useState } from 'react';
import dropaddressservice from '../../services/dropaddress.service'


function CourierDropAddress(props)
{
  console.log(props.drop_id)
    const [address, setAddress] = useState([]);
    

    let c=sessionStorage.getItem("customer")
 console.log("customer obj : "+c)
c=JSON.parse(c)

    const init = () => {
      dropaddressservice.get(props.drop_id)
        .then(response => {
          console.log('Printing dropaddress data by id', response.data);
          setAddress(response.data);
        })
        .catch(error => {
          console.log('Something went wrong', error);
        }) 
    }
  
    useEffect(() => {
      init();
    },[props]);
  
    
   
   
    return (
      <div className="container">
        <h3>List of address</h3>
        <hr/>
        <div>
         
          
          
          <table className="table table-bordered table-striped">
            <thead className="bg-warning">
              <tr>
                <th>area</th>
                <th>city</th>
                <th>district</th>
                <th>pincode</th>
                
                
              </tr>
            </thead>
            <tbody>
            
             
                <tr key={address.id}>
                  <td>{address.area}</td>
                  <td>{address.city}</td>
                  <td>{address.district}</td>
                  <td>{address.pincode}</td>
                
                  
                </tr>
              
            
            </tbody>
          </table>
          
        </div>
        
      </div>
    );
}
export default CourierDropAddress