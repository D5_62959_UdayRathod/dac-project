import { useState } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { useEffect } from "react";
import dropaddressService from "../../services/dropaddress.service";


const AddDropAddress = () => {
    const[area, setArea] = useState('');
    const[city, setCity] = useState('');
    const[district, setDistrict] = useState('');
    const[pincode, setPincode] = useState('');
    
    
    
    const navigate = useNavigate();
   
    const {id}=useParams()
    console.log("addr "+id)
   const cust=sessionStorage.getItem("customer")
       const cust_id=JSON.parse(cust).cust_id
         console.log("cust "+cust_id)
    
    const saveEmployee = (e) => {
        e.preventDefault();
        
        const address = {area, city, district, pincode,id,cust_id};
        if (id) {
            //update
            dropaddressService.update(address)
                .then(response => {
                    console.log('dropaddress data updated successfully', response.data);
                    navigate('/dropaddress');
                })
                .catch(error => {
                    console.log('Something went wrong', error);
                }) 
        } else {
            //create
            dropaddressService.create(address)
            .then(response => {
                console.log("dropaddress added successfully", response.data);
                navigate("/dropaddress");
            })
            .catch(error => {
                console.log('something went wroing', error);
            })
        }
    }

    useEffect(() => {
        if (id) {
            dropaddressService.get(id)
                .then(employee => {
                    setArea(employee.data.area);
                    setCity(employee.data.city);
                    setDistrict(employee.data.district);
                    setPincode(employee.data.pincode);
                   
                    
                })
                .catch(error => {
                    console.log('Something went wrong', error);
                })
        }
    },[])
    return(
        <div className="container">
            <h3>Add dropaddress</h3>
            <hr/>
            <form>
                <div className="form-group">
                    <input 
                        type="text" 
                        className="form-control col-4"
                        id="area"
                        value={area}
                        onChange={(e) => setArea(e.target.value)}
                        placeholder="Enter area"
                    />

                </div>
                <div className="form-group">
                    <input 
                        type="text" 
                        className="form-control col-4"
                        id="city"
                        value={city}
                        onChange={(e) => setCity(e.target.value)}
                        placeholder="Enter city"
                    />

                </div>
                <div className="form-group">
                    <input 
                        type="text" 
                        className="form-control col-4"
                        id="district"
                        value={district}
                        onChange={(e) => setDistrict(e.target.value)}
                        placeholder="Enter district"
                    />
                </div>
                <div className="form-group">
                    <input 
                        type="text" 
                        className="form-control col-4"
                        id="pincode"
                        value={pincode}
                        onChange={(e) => setPincode(e.target.value)}
                        placeholder="Enter pincode"
                    />
                </div>
               
                <div >
                    <button onClick={(e) => saveEmployee(e)} className="btn btn-primary">Save</button>
                </div>
            </form>
            <hr/>
            <Link to="/dropaddress" >Back to List</Link>
        </div>
    )
}

export default AddDropAddress;