import { Link ,useNavigate} from 'react-router-dom';
import { useEffect, useState } from 'react';
import dropaddressservice from 'C:/pc/DAC/study/class_work/Adv_java_classwork/react front end/my-app/src/services/dropaddress.service.js'
import Sidebar from './../Sidebar';


function DropAddress()
{
  
    const [address, setAddress] = useState([]);
    

    let c=sessionStorage.getItem("customer")
 console.log("customer obj : "+c)
c=JSON.parse(c)

    const init = () => {
      dropaddressservice.getByCustId(c)
        .then(response => {
          console.log('Printing dropaddress data', response.data);
          setAddress(response.data);
        })
        .catch(error => {
          console.log('Something went wrong', error);
        }) 
    }
  
    useEffect(() => {
      init();
    },[]);
  
    const handleDelete = (id) => {
      console.log('Printing id', id);
      dropaddressservice.remove(id)
        .then(response => {
          console.log('dropaddress deleted successfully', response.data);
          init();
        })
        .catch(error => {
          console.log('Something went wrong', error);
        })
    }
   
   
    return (
      <div style={{display:'flex'}}>
      <div><Sidebar/></div>
      <div className="container">
        <h3>List of Drop Address</h3>
        <hr/>
        <div>
          <Link to={`/adddropaddress`} className="btn btn-primary">Add Drop Address</Link>
          
          
          <table className="table table-bordered table-striped">
            <thead className="bg-warning">
              <tr>
                <th>area</th>
                <th>city</th>
                <th>district</th>
                <th>pincode</th>
                
                
                <th>actions</th>
              </tr>
            </thead>
            <tbody>
            {
              address.map(address => (
                <tr key={address.id}>
                  <td>{address.area}</td>
                  <td>{address.city}</td>
                  <td>{address.district}</td>
                  <td>{address.pincode}</td>
                
                  
                  <td>
                    <Link className="btn btn-info" to={`/adddropaddress/${address.id}`}>Update</Link>
                    
                    <button style={{margin:"20px"}} className="btn btn-danger ml-2" onClick={() => {
                      handleDelete(address.id);
                    }}>Delete</button>
                  </td>
                </tr>
              ))
            }
            </tbody>
          </table>
          
        </div>
        <Link to="/custdisplay" >Back to List</Link>
      </div>
      </div>
    );
}
export default DropAddress