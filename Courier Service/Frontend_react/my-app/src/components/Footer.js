import React from "react";
import {
Box,
Container,
Row,
Column,
FooterLink,
Heading,
} from "./FooterStyles";
import './Footerstyle.css'
const Footer = () => {
return (
	<div class="container" >
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="title_group">
                    <h2>About Us :</h2>
                </div>
                <div class="about_footer">
                    <p></p><p>Keeping up with world-wide technological developments in the Logistics industry, We have been investing extensively in ramping up its infrastructure, since it is the backbone for holding together the operational service excellence.</p>
<p></p>
                   
                </div>
            </div>

           
           
            <div class="col-md-3 col-sm-6 addr_col" style={{marginLeft:"100px"}}>
			<div className="title_group">
                     <h2>Contact Us :</h2>
 					<h4>Head Office</h4>
                 </div>
                 <p> B/1101, Maan Phase-2, Hinjewadi, Pune 411057</p>
               
                 </div>
				 <div class="col-md-3 col-sm-6 pull-right Useful_link_sec" style={{marginLeft:"100px"}}>
                <div class="title_group">
                    <h2>Social Media :</h2> 
					<p> Email :    courierservice@gmail.com</p>
					<p> Facebook :    courierservice/facebook.com</p>
					<p> Twitter :    courierservice/twitter.com</p>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6">

                     
                    </div>
                    <div class="col-md-6 col-sm-6">
                       
                    </div>
                </div>
            </div>
            </div>
        </div>
    

)
}
export default Footer;
