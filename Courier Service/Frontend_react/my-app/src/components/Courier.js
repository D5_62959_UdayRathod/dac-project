import React from "react";

import {useEffect,useState} from 'react'
import courierService from "../services/courier.service";
function Courier(props){
    
    console.log(props.courier_id)
    const[courier,setCourier]=useState('')
   
   
    
    const init = () => {
        // courierTracking.get(trackingid)
        courierService.get(props.courier_id)
          .then(response => {
            console.log('Printing courier*************** data', response.data);
            setCourier(response.data)
          })
          
          .catch(error => {
            console.log('Something went wrong', error);
          })
          
      }
      
    
      useEffect(() => {
        init();
       
      },[props]);
    return <div>
    <table className="table table-bordered table-striped">
  <thead className="thead-dark">
    <tr>
  

      <th>courier_name</th>
      <th>weight</th>
      <th>price</th>
      <th>pickupdate</th>
      <th>dropdatedate</th>
      
      
    </tr>
  </thead>
  <tbody>
   {
    
      <tr>
        <td>{courier.courierName}</td>
        <td>{courier.weight}</td>
        <td>{courier.price}</td>
        <td>{courier.courierPickUpDate}</td>
        <td>{courier.dropDate}</td>
        
      </tr>
    
  } 
  </tbody>
</table>
</div>
}
export default Courier;