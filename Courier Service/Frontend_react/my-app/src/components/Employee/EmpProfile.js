
import React from "react";

import { useEffect, useState } from 'react'

import employeeService from "../../services/employee.service";
import { Link } from 'react-router-dom';

const EmpProfile =()=>  {
  const [result, setResult] = React.useState('');
  


  React.useEffect(() => {
    const emp_id=sessionStorage.getItem("emp")
     employeeService.get(emp_id).then((response) => {
        
        setResult(response.data);
        
    });
  }, []);



  const styles = {
    h6: {
      textAlign: 'center',
      margin: 20,
      fontFamily: "Lucida Sans Unicode",
      fontStyle: 'normal',
      
      fontWeight: 700
      
    },
    container: {
      width: 810,
      height: 550,
      padding: 20,
      position: 'relative',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      margin: 'auto',
      borderColor: 'rgb(127, 231, 58)',
      borderRadius: 10,
      broderWidth: 1,
      borderStyle: 'solid',
      boxShadow: '1px 1px 20px 5px #C9C9C9',
    },

    p: {
      textAlign: 'center',
      fontFamily: "Lucida Sans Unicode",
      fontStyle: 'normal',
      fontSize: 'large',
      fontWeight: 580
    },
    head: {
      textAlign: 'center',
      fontFamily: "Lucida Sans Unicode",
      fontStyle: 'normal',
      fontSize: 'x-large',
      fontWeight: 580
    },
  }

  return (
    <div className="container" style={{ marginTop: 60 }}>
    
     
          <div style={styles.container}>
              <h1  style={styles.head}>Profile Details</h1>
              <hr/>
              <h6 style={styles.h6}>First Name :</h6>
            <p style={styles.p}>{result.firstName}</p>
            <h6 style={styles.h6}>Last Name :</h6>
            <p style={styles.p}>{result.lastName}</p>
            <h6 style={styles.h6}>Phone</h6>
            <p style={styles.p}>{result.phone}</p>
            <h6 style={styles.h6}>Email :</h6>
            <p style={styles.p}>{result.email}</p>
            <h6 style={styles.h6}>Designation :</h6>
            <p style={styles.p}>{result.designatation}</p>
          </div>
        
    
     <Link to="/empdispmenu" >Back</Link>
    </div>
  )
}

export default EmpProfile