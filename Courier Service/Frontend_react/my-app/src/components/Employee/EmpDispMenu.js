
import { ListGroup, ListGroupItem, Button } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
function EmpDispMenu()
{
  const navigate=useNavigate()
  const logout=()=>{
    sessionStorage.removeItem("empjwt")
  navigate("/")
  }
    return (
        <div>
          <ListGroup >
          <ListGroupItem>
              <NavLink className="nav-link" >
                <h5>
                  <i className="fa fa-dashboard"></i>
                  <p> DashBoard</p>
                </h5>
              </NavLink>
            </ListGroupItem>
            
          <ListGroupItem
              style={{
                display: 'flex',
                justifyContent: 'left',
              }}>
              <NavLink className="nav-link" exact to="/empdisplay">
                
                <Button outline color="info" >
                  Attend A Couriers<br></br>
                  <i className="fa fa-user-circle fa-2x" aria-hidden="true"></i>
                </Button>
              </NavLink>
            </ListGroupItem>
    
            <ListGroupItem>
              <NavLink className="nav-link" exact to="/empcouriers">
                <Button outline color="secondary">
                  View Attending Couriers<br></br>
                  <i className="fa fa-users fa-2x" aria-hidden="true"></i>
                </Button>
              </NavLink>
            </ListGroupItem>
    
            <ListGroupItem>
              <NavLink className="nav-link" exact to="/empprofile">
                <Button outline color="danger">
                View Profile<i className="fa fa-cog fa-spin fa-3x fa-fw"></i>
                </Button>
              </NavLink>
              <button className="btn btn-warning" onClick={logout}>Logout</button>
            </ListGroupItem>
            
          </ListGroup>
        </div>
      )
}
export default EmpDispMenu