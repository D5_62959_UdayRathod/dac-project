
import { useEffect, useState } from 'react';
import queryservice from "../../services/query.service"
import { Link } from 'react-router-dom';

import Courier from './../Courier';

import CustomerInQuery from './../Admin/CustomerInQuery';
import courierTrackingService from '../../services/courierTracking.service';
import Emp from './../Emp';
import UpdateTracking from '../UpdateTracking';
import { useNavigate } from 'react-router-dom';

function EmpDisplay(){
  const navigate=useNavigate()
   const[tracking,setTracking] = useState([]);
   const[data,setData]=useState('')
   const[courier_id,setCourier_id]=useState('')
   const[tracking_id,setTracking_id]=useState('')
   let[trackingStatus,setTrackingStatus]=useState('')
   const[customer_id,setCustomer_id]=useState('')
  
    const init = () => {
      const token=sessionStorage.getItem("empjwt")
        // courierTracking.get(trackingid)
        courierTrackingService.getAll(token)
          .then(response => {
            console.log('Printing employees data', response.data);
            setTracking(response.data)
           
          })
          
          .catch(error => {
            console.log('Something went wrong', error);
          })
          
          
      }
      const trackinginfo=(courier_id)=>{
        
        courierTrackingService.getByCourierId(courier_id)
        .then(response => {
          console.log('Printing courier data', response.data);
          
          setCustomer_id(response.data.cust_id)
          setData("customer")
        })
        
        .catch(error => {
          console.log('Something went wrong', error);
        })
     }
   
      useEffect(() => {
        init();
       
       
      }, []);
      
    const updateinfo=()=>
    {
        const {status}=document.forms[0]
        console.log(status)
        setData("updatetracking");
    }
   
return(
    <div>
<table className="table table-bordered table-striped">
          <thead className="thead-dark">
            <tr>
          

              <th>TrackingId</th>
              <th>OrderStatus </th>
              <th>CourierDetails </th>
              <th>CustomerDetails </th>
              <th>OrderStatus</th>
              <th>Action</th>
              
            </tr>
          </thead>
          <tbody>
           
             {
                tracking.map(q => (
                  <tr key={q.tracking_id}>
                <td>{q.tracking_id}</td>
                <td>{q.orderStatus}</td>
                
                <td><button className="btn btn-primary" onClick={()=>{setData("track");setCourier_id(q.courier_id)}}>View Details</button> </td>
                <td><button className="btn btn-primary" onClick={()=>{trackinginfo(q.courier_id);}}>View Details</button> </td>
                
                
             
               <td><input 
                        type="text" 
                        className="form-control col-4"
                        id="designation"
                        
                        onChange={(e) => setTrackingStatus(e.target.value)}
                        placeholder="Enter Status"
                    />
                    </td>
                <td>
                  <button className="btn btn-info" onClick={()=>{setTracking_id(q.tracking_id);setData("updatetracking");}} type='submit' >Update</button>
                  
                </td>
               
                
              </tr>
            ))}
                
          </tbody>
        </table>
        {data==='track' && <Courier courier_id={courier_id}/> }
        {data==='customer' && <CustomerInQuery customer_id={customer_id}/> }
        {data==='updatetracking' && <UpdateTracking tracking_id={tracking_id} trackingStatus={trackingStatus}/> }
        
        <Link to="/empdispmenu" >Back</Link>
    </div>
)
}
export default EmpDisplay;