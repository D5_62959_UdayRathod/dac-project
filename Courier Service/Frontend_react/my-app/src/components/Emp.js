import { useState } from "react"
import employeeService from "../services/employee.service"
import { useEffect } from "react"
function Emp(props)
{
    console.log(props.emp_id)
    const[courier,setCourier]=useState('')
   
   
    
    const init = () => {
        // courierTracking.get(trackingid)
        employeeService.get(props.emp_id)
          .then(response => {
            console.log('Printing employees data', response.data);
            setCourier(response.data)
          })
          
          .catch(error => {
            console.log('Something went wrong', error);
          })
          
      }
      
    
      useEffect(() => {
        init();
       
      },[props]);
    return <div>
    <table className="table table-bordered table-striped">
  <thead className="thead-dark">
    <tr>
  
    
      <th>emp_firstname</th>
      <th>emp_lastname</th>
      <th>emp_phone</th>
      
      
    </tr>
  </thead>
  <tbody>
   {
    
      <tr>
        <td>{courier.firstName}</td>
        <td>{courier.lastName}</td>
        <td>{courier.phone}</td>
        
      </tr>
    
  } 
  </tbody>
</table>
</div>
}
export default Emp;