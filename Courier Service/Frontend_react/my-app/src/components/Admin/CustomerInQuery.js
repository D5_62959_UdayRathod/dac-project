import { useState } from "react"

import { useEffect } from "react"

import customerService from "../../services/customer.service"

function CustomerInQuery(props)
{
    console.log(props.customer_id)
    
    const[customer,setCustomer]=useState('')

   
   
    
    const init = () => {
        // courierTracking.get(trackingid)
        customerService.get(props.customer_id)
          .then(response => {
            console.log('Printing customer data', response.data);
            setCustomer(response.data)
          })
          
          .catch(error => {
            console.log('Something went wrong', error);
          })
          
      }
      
    
      useEffect(() => {
        init();
       
      },[props]);
    return <div>
    <table className="table table-bordered table-striped">
  <thead className="thead-dark">
    <tr>
  
    
      <th>customer_firstname</th>
      <th>customer_lastname</th>
      <th>customer_email</th>
      <th>customer_phone</th>
      
      
    </tr>
  </thead>
  <tbody>
   {
    
      <tr>
        <td>{customer.firstName}</td>
        <td>{customer.lastName}</td>
        <td>{customer.email}</td>
        <td>{customer.phone}</td>
        
      </tr>
    
  } 
  </tbody>
</table>
</div>
}
export default CustomerInQuery;