import customerService from '../services/customer.service';
import './CustLogin.css'
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



function CustLogin(){
    const [errorMessages, setErrorMessages] = useState({});
    const [isSubmitted, setIsSubmitted] = useState(false);
    
    const history = useNavigate();
    // User Login info
   
  
    const errors = {
      uname: "invalid username",
      pass: "invalid password"
    };

   
    const handleSubmit = (event) => {
        //Prevent page reload
        event.preventDefault();
          
        const { uname, pass } = document.forms[0];
        const email=uname.value;
        const password=pass.value;
        const customer={email,password}
        
        customerService.getByEmailAndPass(customer).then(e=>{
            if (e.data) {
                if (e.data.password !==password) {
                  // Invalid password
                  setErrorMessages({ name: "pass", message: errors.pass });
                } else {
                  setIsSubmitted(true);
                  
                  sessionStorage.setItem("customer",JSON.stringify(e.data))
                 
                   toast(" Hey ,thankyou for joining us ")
                    
              history('/custdisplay')
                }
              } else {
                // Username not found
                setErrorMessages({ name: "uname", message: errors.uname });
              }
        })
    }
    const renderErrorMessage = (name) =>
    name === errorMessages.name && (
      <div className="error">{errorMessages.message}</div>
    );

return(<div>
    <section className="h-100 gradient-form" style={{backgroundColor: "#eee"}}>
  <div className="container py-5 h-100">
    <div className="row d-flex justify-content-center align-items-center h-100">
      <div className="col-xl-10">
        <div className="card rounded-3 text-black">
          <div className="row g-0">
            <div className="col-lg-6">
              <div className="card-body p-md-5 mx-md-4">

                <div className="text-center">
                  <img src="https://t4.ftcdn.net/jpg/04/13/20/97/360_F_413209771_ZA2YI8qDK721nma8N5erkn5OIzct6hPW.jpg"
                    style={{width: "185px"}} alt="logo"/>
                  <h4 className="mt-1 mb-5 pb-1">Faster than Light</h4>
                </div>

                <form onSubmit={handleSubmit}> 
                  <p>Please login to your account</p>

                  <div className="form-outline mb-4">
                    <input type="email" name="uname" id="form2Example11" className="form-control"
                      placeholder="Phone number or email address" />
                      {renderErrorMessage("uname")}
                    <label className="form-label" htmlFor="form2Example11">Username</label>
                  </div>

                  <div className="form-outline mb-4">
                    <input type="password" id="form2Example22" name="pass" placeholder="Enter password" 
                    className="form-control" />
                    <label className="form-label" 
                     htmlFor="form2Example22">Password</label>
                     {renderErrorMessage("pass")}
                  </div>

                  <div className="text-center pt-1 mb-5 pb-1">
                    <button className="btn btn-primary btn-block fa-lg gradient-custom-2 mb-3" type="submit">Log
                      in</button>
                      <ToastContainer />
                    {/* <a className="text-muted" href="#!">Forgot password?</a> */}
                    <ToastContainer />
                  </div>

                  <div className="d-flex align-items-center justify-content-center pb-4">
                    <p className="mb-0 me-2">Don't have an account?</p>
                    <Link to="/custregister" type="button" className="btn btn-outline-danger">Create new</Link>
                  </div>

                </form>

              </div>
            </div>
            <div className="col-lg-6 d-flex align-items-center gradient-custom-2">
              <div className="text-white px-3 py-4 p-md-5 mx-md-4">
                <h4 className="mb-4">We are more than just a company</h4>
                <p className="small mb-0">We are the pioneers in the field of delivery & pickup with real time tracking & cost effective.Fast,Easy and reliable courier service.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>)
}
export default CustLogin;