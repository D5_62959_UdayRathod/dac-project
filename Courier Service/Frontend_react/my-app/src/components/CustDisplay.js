import React from 'react'

import { Card, CardTitle, CardGroup, CardBody, Row, Col } from 'reactstrap'
import { NavLink } from 'react-router-dom'
import { useNavigate } from 'react-router-dom'
import { Button, ListGroup, ListGroupItem } from 'reactstrap'
import DashMenuBar from './CustDisplayMenu'
import  Sidebar  from './Sidebar';


function CusDisplay() {
   
  return (

    <div style={{ backgroundImage:`url("https://img.freepik.com/free-vector/loading-workman-carrying-boxes_74855-14096.jpg?w=1000")`,backgroundSize:"100%", 
    
    }}>
      
      <br></br><br></br><br></br>
      <div className="py-5">
        <Row>
          <Col md={2}>
            <div>
              <Sidebar />
            </div>
          </Col>
          <Col >
            <CardGroup>
              
              
            </CardGroup>
            <br></br>
            <br></br>
            <br></br>
            <div>
            <ListGroupItem
          style={{
            display: 'flex',
            justifyContent: 'center',
          }}>
          <NavLink  exact to="/courierform">
            
            <Button outline color="danger" >
              Send A Courier<br></br>
              <i className="fa fa-user-circle fa-2x" aria-hidden="true"></i>
            </Button>
          </NavLink>
        </ListGroupItem>
            </div>
          </Col>
        </Row>
      </div>
      
    </div>
  )
              }


export default CusDisplay
