import customerService from '../services/customer.service';
import './CustLogin.css'
import { useNavigate } from 'react-router-dom';


function CustRegister(){
    const history = useNavigate();
    const handleSubmit = (event) => {
        //Prevent page reload
        event.preventDefault();
    
        let { firstName,lastName,phone, pass,email } = document.forms[0];
      firstName=firstName.value
      lastName=lastName.value
      phone=phone.value
      email=email.value
      let password=pass.value;
      const customer={firstName,lastName,phone, password,email}
      customerService.create(customer).then(e=>{
                                      console.log(e.data)
                                      history('/custlogin');
                                     alert("you have successfully registered ")
                                                 })
    }
    return(
        <div>
            
            <section className="vh-100 bg-image"
  style={{backgroundImage: "url('https://mdbcdn.b-cdn.net/img/Photos/new-templates/search-box/img4.webp')"}}>
  <div className="mask d-flex align-items-center h-100 gradient-custom-3">
    <div className="container h-100">
      <div className="row d-flex justify-content-center align-items-center h-100">
        <div className="col-12 col-md-9 col-lg-7 col-xl-6">
          <div className="card" style={{borderRadius: "15px"}}>
            <div className="card-body p-5">
              <h2 className="text-uppercase text-center mb-5">Create an account</h2>

              <form onSubmit={handleSubmit}>

                <div className="form-outline mb-4">
                  <input type="text" name="firstName" id="form3Example1cg" className="form-control form-control-lg" />
                  <label className="form-label" for="form3Example1cg">Your First Name</label>
                </div>
                <div className="form-outline mb-4">
                  <input type="text" name="lastName" id="form3Example1cg2" className="form-control form-control-lg" />
                  <label className="form-label" for="form3Example1cg">Your Last Name</label>
                </div>
                <div className="form-outline mb-4">
                  <input type="tel" name="phone" id="form3Example3cg2" className="form-control form-control-lg" />
                  <label className="form-label" for="form3Example3cg">Your Phone</label>
                </div>

                <div className="form-outline mb-4">
                  <input type="email" name="email" id="form3Example3cg" className="form-control form-control-lg" />
                  <label className="form-label" for="form3Example3cg">Your Email</label>
                </div>

                <div className="form-outline mb-4">
                  <input type="password" name="pass" id="form3Example4cg" className="form-control form-control-lg" />
                  <label className="form-label" for="form3Example4cg">Password</label>
                </div>


                <div className="d-flex justify-content-center">
                  <button type="submit"
                    className="btn btn-success btn-block btn-lg gradient-custom-4 text-body">Register</button>
                </div>

                <p className="text-center text-muted mt-5 mb-0">Have already an account? <a href="/custlogin"
                    className="fw-bold text-body"><u>Login here</u></a></p>

              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

        </div>
    )
}
export default CustRegister;