

     import React from 'react';
     import { Button, ListGroup, ListGroupItem } from 'reactstrap'
    import './Sidebar.css'
    import {
        CDBSidebar,
        CDBSidebarContent,
        CDBSidebarFooter,
        CDBSidebarHeader,
        CDBSidebarMenu,
        CDBSidebarMenuItem,
    } from 'cdbreact';
    import { NavLink } from 'react-router-dom';
    import { useNavigate } from 'react-router-dom';

    
     const Sidebar = () => {
      const navigate=useNavigate()
  const logout=()=>{
    sessionStorage.clear()
    navigate('/custlogin')
  }
        return (
            <div style={{ display: 'flex' }}>
          <CDBSidebar textColor="#fff" backgroundColor="black">
            <CDBSidebarHeader prefix={<i className="fa fa-bars fa-large"></i>}>
              <a href="/" className="text-decoration-none" style={{ color: 'inherit' }}>
                Sidebar
              </a>
            </CDBSidebarHeader>
            <ListGroup >
      <ListGroupItem>
          <NavLink className="nav-link" >
            <h5>
              <i className="fa fa-dashboard"></i>
              <p> DashBoard</p>
            </h5>
          </NavLink>
        </ListGroupItem>
        
      <ListGroupItem
          style={{
            display: 'flex',
            justifyContent: 'left',
          }}>
          <NavLink className="nav-link" exact to="/pickupaddress">
            
            <Button outline color="info" >
              PickUp Address<br></br>
              <i className="fa fa-user-circle fa-2x" aria-hidden="true"></i>
            </Button>
          </NavLink>
        </ListGroupItem>

        <ListGroupItem>
          <NavLink className="nav-link" exact to="/dropaddress">
            <Button outline color="secondary">
              Drop Address<br></br>
              <i className="fa fa-users fa-2x" aria-hidden="true"></i>
            </Button>
          </NavLink>
        </ListGroupItem>

        <ListGroupItem
          style={{
            display: 'flex',
            justifyContent: 'left',
          }}>
          <NavLink className="nav-link" exact to="/viewcouriers">
            <Button outline color="info">
              View Your Couriers <br></br>
              <i className="fa fa-user-circle fa-2x" aria-hidden="true"></i>
            </Button>
          </NavLink>
        </ListGroupItem>

       

        

        <ListGroupItem>
          <NavLink className="nav-link" exact to="/viewqueries">
            <Button outline color="secondary">
            View Queries
            <i className="fa fa-volume-control-phone fa-3x"></i>
            </Button>
          </NavLink>
        </ListGroupItem>

      
    
        <ListGroupItem>
          <NavLink className="nav-link" exact to="/custprofile">
            <Button outline color="danger">
            View Profile<i className="fa fa-cog fa-spin fa-3x fa-fw"></i>
            </Button>
          </NavLink>
        </ListGroupItem>

    

        <ListGroupItem>
        <NavLink className="nav-link" exact to="/custlogin">
            <Button outline color="danger">
            Logout<i className="fa fa-user-circle fa-2x"></i>
            </Button>
          </NavLink>
        </ListGroupItem>
      </ListGroup>
    

            <CDBSidebarContent className="sidebar-content">
              <CDBSidebarMenu>
                 {/* <NavLink exact to="/" activeClassName="activeClicked">
                  <CDBSidebarMenuItem icon="columns">Dashboard</CDBSidebarMenuItem>
                </NavLink>  */}

                 {/* <NavLink   activeClassName="activeClicked"
                className="nav-link" exact to="/demo"> */}
                  
                  {/* <Button outline color="info">
               <br></br>
              <i className="" aria-hidden="true"></i> 
            </Button>  */}
        {/* <CDBSidebarMenuItem icon="user">See Your Couriers</CDBSidebarMenuItem> */}
                 {/* </NavLink>  */}
                {/* <NavLink exact to="/pickupaddress" activeClassName="activeClicked">
                  <CDBSidebarMenuItem icon="fa fa-user-circle fa-2x">pickupaddress</CDBSidebarMenuItem>
                </NavLink>
                <NavLink exact to="/analytics" activeClassName="activeClicked">
                <Button outline color="info">
              Drop Address <br></br>
              <i className="fa fa-user-circle fa-2x" aria-hidden="true"></i>
            </Button>
                  
                 </NavLink>  */}
    
                 {/* <NavLink exact to="/hero404" activeClassName="activeClicked">
                <Button outline color="info">
              view enquires <br></br>
              <i className="fa fa-user-circle fa-2x" aria-hidden="true"></i>
            </Button>  */}
                  {/* <CDBSidebarMenuItem icon="user">view enquires</CDBSidebarMenuItem> */}
                
               {/* </NavLink>  */}
                
                 {/* <NavLink exact to="/hero404"  activeClassName="activeClicked">
                <Button outline color="info">
            View Profile <br></br>
              <i className="fa fa-user-circle fa-2x" aria-hidden="true"></i>
            </Button>
                  {/* <CDBSidebarMenuItem icon="user">View Profile</CDBSidebarMenuItem> */}
                 {/* </NavLink>   */}

                {/* <NavLink exact to="/hero404"  activeClassName="activeClicked">
                <Button outline color="info">
              Logout <br></br>
              <i className="fa fa-user-circle fa-2x" aria-hidden="true"></i>
            </Button>
                  <CDBSidebarMenuItem icon="user">Logout</CDBSidebarMenuItem>
                </NavLink> */}
              </CDBSidebarMenu>
            </CDBSidebarContent>
    
            <CDBSidebarFooter style={{ textAlign: 'center' }}>
              <div
                style={{
                    padding: '20px 5px',
                }}
                >
               
              </div>
            </CDBSidebarFooter>
          </CDBSidebar>
        </div>
    
    
    );
}

  export default Sidebar;