import customerService from '../services/customer.service';
import './CustLogin.css'
import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';


function CustProfileUpdate(){
    const history = useNavigate();
    const {id}=useParams()
    const[firstName, setFirstname] = useState('');
    const[lastName, setLastname] = useState('');
    const[phone, setPhone] = useState('');
    const[email, setEmail] = useState('');
    const[password, setPassword] = useState('');
    const handleSubmit = (event) => {
        //Prevent page reload
        event.preventDefault();
    
        let { firstName,lastName,phone, pass,email } = document.forms[0];
      firstName=firstName.value
      lastName=lastName.value
      phone=phone.value
      email=email.value
      let password=pass.value;
      const customer={cust_id:id,firstName,lastName,phone, password,email}
      console.log(id)

      customerService.update(customer).then(e=>{
                                      console.log(e.data)
                                      history('/custprofile');
                                     alert("you have successfully updated profile ")
                                                 })
    }
    useEffect(() => {
        if (id) {
      console.log(id)
            customerService.get(id)
                .then(employee => {
                    setFirstname(employee.data.firstName);
                    setLastname(employee.data.lastName);
                    setPhone(employee.data.phone);
                    setEmail(employee.data.email);
                    setPassword(employee.data.password);
                    
                    
                })
                .catch(error => {
                    console.log('Something went wrong', error);
                })
        }
    },[])
    return(
        <div>
            
            <section className="vh-100 bg-image"
  style={{backgroundImage: "url('https://mdbcdn.b-cdn.net/img/Photos/new-templates/search-box/img4.webp')"}}>
  <div className="mask d-flex align-items-center h-100 gradient-custom-3">
    <div className="container h-100">
      <div className="row d-flex justify-content-center align-items-center h-100">
        <div className="col-12 col-md-9 col-lg-7 col-xl-6">
          <div className="card" style={{borderRadius: "15px"}}>
            <div className="card-body p-5">
              <h2 className="text-uppercase text-center mb-5">Update Profile</h2>

              <form onSubmit={handleSubmit}>

                <div className="form-outline mb-4">
                  <input type="text" value={firstName}
                        onChange={(e) => setFirstname(e.target.value)} name="firstName" id="form3Example1cg" className="form-control form-control-lg" />
                  <label className="form-label" for="form3Example1cg">Your First Name</label>
                </div>
                <div className="form-outline mb-4">
                  <input type="text" value={lastName}
                        onChange={(e) => setLastname(e.target.value)} name="lastName" id="form3Example1cg2" className="form-control form-control-lg" />
                  <label className="form-label" for="form3Example1cg">Your Last Name</label>
                </div>
                <div className="form-outline mb-4">
                  <input type="tel" name="phone" value={phone}
                        onChange={(e) => setPhone(e.target.value)} id="form3Example3cg2" className="form-control form-control-lg" />
                  <label className="form-label" for="form3Example3cg">Your Phone</label>
                </div>

                <div className="form-outline mb-4">
                  <input type="email" value={email}
                        onChange={(e) => setEmail(e.target.value)} name="email" id="form3Example3cg" className="form-control form-control-lg" />
                  <label className="form-label" for="form3Example3cg">Your Email</label>
                </div>

                <div className="form-outline mb-4">
                  <input type="password" name="pass" value={password} onChange={(e) => setPassword(e.target.value)} id="form3Example4cg" className="form-control form-control-lg" />
                  <label className="form-label" for="form3Example4cg">Enter Password</label>
                </div>


                <div className="d-flex justify-content-center">
                  <button type="submit"
                    className="btn btn-success btn-block btn-lg gradient-custom-4 text-body">Update</button>
                </div>

                
              </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

        </div>
    )
}
export default CustProfileUpdate;