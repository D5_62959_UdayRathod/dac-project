import { useState } from "react";
import {  useParams } from "react-router-dom";
import React from "react";
import Courier from "./Courier";
import { useEffect } from "react";
import Emp from "./Emp";
import employeeService from "../services/employee.service";
import courierTracking from "../services/courierTracking.service";
function ShowShipment(props){
    let {trackingid} = useParams();
    const courierid=props.courierId
    const[employee,setEmployee] = useState('');
    const[courier,setCourier] = useState('')
   const[customer,setCustomer]=useState('')
   const[data,setData]=useState('')
   
    const init = () => {
                         if(courierid)
                         {
                          courierTracking.getByCourierId(courierid).then(response => {
                            console.log('Printing courier tracking data', response.data);
                            
                            setCourier(response.data);
                            
                            employeeService.get(response.data.emp_id).then(e=>{setEmployee(e.data)});
                           
                          })
                          
                          .catch(error => {
                            console.log('Something went wrong', error);
                          })
                         }else{
                          courierTracking.get(trackingid)
        .then(response => {
          console.log('Printing employees data', response.data);
          
          setCourier(response.data);
          
          employeeService.get(response.data.emp_id).then(e=>{setEmployee(e.data)});
         
        })
        
        .catch(error => {
          console.log('Something went wrong', error);
        })
        
         
                         }
     
      
        
    }
    
      
      
    
  
    useEffect(() => {
      init();
     
    }, []);
    
  
     
     return(
        <div>
            <table className="table table-bordered table-striped">
          <thead className="thead-dark">
            <tr>
          

              <th>tracking_id</th>
              <th>order_status</th>
              <th>courierdetails</th>
              
              <th>empDetails</th>
              
              
            </tr>
          </thead>
          <tbody>
           {
            
              <tr>
                <td>{courier.tracking_id}</td>
                <td>{courier.orderStatus}</td>
                {/* <td> <Link to={`/courier/${courier.courier_id}`} className="btn btn-primary">courierdetail</Link></td> */}
                <td><button className="btn btn-primary" onClick={()=>{setData('courierdetails')}}>courierdetail</button> </td>
                <td><button className="btn btn-primary" onClick={()=>{setData('empdetail')}}>empdetail</button> </td>
               
                
                
                
              </tr>
            
          } 
          </tbody>
        </table>
        {data==='courierdetails' && <Courier courier_id={courier.courier_id}/>}
        {data==='empdetail' && <Emp emp_id={courier.emp_id} />}
        </div>
     )
     
}
export default ShowShipment;