const validation = (values) => {

    let errors = {};

    if (!values.firstName) {
        errors.firstName = "firstName is Required."
    }

    if (!values.lastName) {
        errors.lastName = "lastName is Required."
    }

    if (!values.mobileNumber) {
        errors.contactNumber = "contact Number is Required."
    }

    if (!values.email) {
        errors.userEmail = "Email is Required."
    } else if (!/\S+@\S+\.\S+/.test(values.userEmail)) {
        errors.userEmail = "Email is invalid."
    }

    if (!values.password) {
        errors.password = "Password is Required."
    } else if (values.password.length < 5) {
        errors.password = "Password must be more then 5 charecters."
    }

    if (!values.gender) {
        errors.role = "role is required"
    }
    if (!values.registerDate) {
        errors.role = "register date is required"
    }
    if (!values.role) {
        errors.role = "role is required"
    }


    return errors;
}
export default validation;