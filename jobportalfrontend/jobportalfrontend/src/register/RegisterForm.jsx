import React, { useState, useEffect } from "react";
import validation from "./validation";
//import custService from "../../services/customer.service";
import { useNavigate } from 'react-router-dom';
import "./style.css";
import userServices from '../services/userServices'


const RegisterForm = () => {
  const navigate = useNavigate();

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNumber, setmobileNumber] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [gender, setGender] = useState("");
  const [registerDate, setRegisterDate] = useState("");
  const [role, setRole] = useState("");

  const [errors, setErrors] = useState({});
  const [dataIsCorrect, setDataIsCorrect] = useState(false);
  const [dataAdded, setDataAdded] = useState('');

  const sleep = (milliseconds) => {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }


  const user = { firstName, lastName, email, password, gender, registerDate, role };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    //setRole("CUSTOMER");
    setErrors(validation(user));
    // if(Object.keys(errors).length === 0){
    //   setDataIsCorrect(true);
    // }
    console.log("sending", user);
    if (dataIsCorrect === true) {
      userServices.create(user)
        .then(response => {
          console.log("User added successfully", response.data);
          setDataAdded("Registration Successful");
          sleep(3000).then(r => {
            navigate('/signin');
          });
        })
        .catch(error => {
          console.log('Something went wrong', error);
          setDataAdded("Error While Registration");
        })
    }
  };
  useEffect(() => {
    if (Object.keys(errors).length === 0 && !dataIsCorrect) {
      setDataIsCorrect(true)
    }
  }, [dataIsCorrect, errors]);
  // debugger
  return (
    <>
      {dataAdded && <h3 class="alert alartbox alert-primary" role="alert">{dataAdded}</h3>}
      <div className="wrapper">

        <div className="app-wrapper">
          <div>
            <h2 className="title">Register Account</h2>
          </div>
          <form className="form-wrapper">
            <div className="row">
              <div className="col">
                <div className="name">
                  <label className="label">First Name</label>
                  <input
                    className="input"
                    type="text"
                    name="firstName"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                  />
                  {errors.firstName && <p className="error">{errors.firstName}</p>}
                </div>
              </div>
              <div className="name">
                <label className="label">Last Name</label>
                <input
                  className="input"
                  type="text"
                  name="lastName"
                  value={lastName}
                  onChange={(e) => setLastName(e.target.value)}
                />
                {errors.lastName && <p className="error">{errors.lastName}</p>}
              </div>
            </div>
            <div className="name">
              <label className="label">mobile Number</label>
              <input
                className="input"
                type="text"
                name="mobileNumber"
                value={mobileNumber}
                onChange={(e) => setmobileNumber(e.target.value)}
              />
              {errors.mobileNumber && (
                <p className="error">{errors.mobileNumber}</p>
              )}
            </div>
            <div className="email">
              <label className="label">Email</label>
              <input
                className="input"
                type="text"
                name="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              {errors.userEmail && <p className="error">{errors.email}</p>}
            </div>
            <div className="password">
              <label className="label">Password</label>
              <input
                className="input"
                type="password"
                name="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              {errors.password && <p className="error">{errors.password}</p>}
            </div>
            <div className="name">
              <label className="label">Gender</label>
              <input
                className="input"
                type="text"
                name="gender"
                value={gender}
                onChange={(e) => setGender(e.target.value)}
              />
              {errors.gender && <p className="error">{errors.gender}</p>}
              <div className="name">
                <label className="label">RegisterDate</label>
                <input
                  className="input"
                  type="date"
                  name="RegisterDate"
                  value={registerDate}
                  onChange={(e) => setRegisterDate(e.target.value)}
                />
                {errors.registerDate && <p className="error">{errors.registerDate}</p>}
              </div>
              <div className="name">
                <label className="label" > Role </label>
                <div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio"
                      name="role"
                      value="JOB_SEEKER"
                      onChange={(e) => setRole(e.target.value)}
                    />
                  &nbsp;
                  <label class="form-check-label" for="inlineRadio1"></label>
                    <label className="label" > JobSeeker </label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio"
                      name="role"
                      value="EMPLOYER"
                      onChange={(e) => setRole(e.target.value)} />
                       &nbsp;
                  <label class="form-check-label" for="inlineRadio2"></label>
                    <label className="label" >Employer</label>
                  </div>
                  <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" id="inlineRadio3"
                      name="role"
                      value="ADMIN"
                      onChange={(e) => setRole(e.target.value)} />
                       &nbsp;
                  <label class="form-check-label" for="inlineRadio3"></label>
                    <label className="label" >Admin</label>
                  </div>

                </div>
              </div>
            </div>

            <div>
              <button className="submit" onClick={handleFormSubmit}>
                Register
            </button>
            </div>
          </form>
        </div>
      </div>
    </>

  );
};

export default RegisterForm;
