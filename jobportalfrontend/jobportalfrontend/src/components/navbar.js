import { Link } from 'react-router-dom'
import { useState, useEffect } from 'react'

const Navbar = () => {
  const [userobj, setUserobj] = useState(false);
  const myuserobj = localStorage.getItem("user");
  const user = JSON.parse(myuserobj);

  useEffect(() => {
    const loggedIn = localStorage.getItem("user");
    if (loggedIn) {
      setUserobj(true);
    }
  }, []);

  const logout = () => {
    localStorage.removeItem("user");
    setUserobj(false);
  };


  return (
    <nav className='navbar navbar-expand-lg bg-light navbar-light shadow sticky-top p-0'>
      <a
        href='/home'
        className='navbar-brand d-flex align-items-center text-center py-0 px-4 px-lg-5'>
        <h1 className='m-0 text-primary'>Job Portal</h1>
      </a>
      <button
        type='button'
        className='navbar-toggler me-4'
        data-bs-toggle='collapse'
        data-bs-target='#navbarCollapse'>
        <span className='navbar-toggler-icon' />
      </button>
      <div className='collapse navbar-collapse' id='navbarCollapse'>
        <div className='navbar-nav ms-auto p-4 p-lg-0'>
          {!userobj ? (
            <li className='nav-item'>
              <Link className='nav-link active' aria-current='page' to='/home'>
                Home
            </Link>
            </li>
          ) : ("")}
          <div>
            {!userobj ? (
              <li className='nav-item'>
                <Link className='nav-link active' aria-current='page' to='/about'>
                  About
              </Link>

              </li>
            ) : ("")}
          </div>


          {!userobj ? (<a href='contact.html' className='nav-link active'  >
            Contact Us
          </a>) : ("")}
          {!userobj ? (
            <li className='nav-item'>
              <Link className='nav-link active' aria-current='page' to='user/signin'>
                Signin
            </Link>
            </li>) : (

              "")}
          {userobj ? (
            <li className='nav-item'>
              <Link className='nav-link active' aria-current='page' to='/home' onClick={logout}>
                Logout
            </Link>
            </li>) : (

              "")}
        </div>
        {!userobj | (userobj && (user.role == "JOB_SEEKER")) ? (
          <Link
            to='/postedjob'
            className='btn btn-primary rounded-0 py-4 px-lg-5 d-none d-lg-block'>
            Find Jobs
            <i className='fa fa-arrow-right ms-3' />
          </Link>
        ) : ("")}
      </div>
    </nav>
  )
}

export default Navbar
