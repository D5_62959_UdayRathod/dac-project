import httpClient from '../http-common'

const addCompany = (data, employerId) => {
    return httpClient.post(`/company/add/${employerId}`, data);
};

const allCompany = () => {
    return httpClient.get('/company/get/all');
};


export default { addCompany, allCompany }