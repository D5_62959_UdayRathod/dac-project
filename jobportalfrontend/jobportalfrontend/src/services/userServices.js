import httpClient from '../http-common'

const getAll = () => {
  return httpClient.get('/admin/alluser')
}

const create = (data) => {
  return httpClient.post('user/registration', data)
}

const login = (data) => {
  return httpClient.post('user/signin', data)
}

const get = (id) => {
  return httpClient.get('user/signin', id)
}

const update = (data) => {
  return httpClient.put('', data)
}

const remove = (id) => {
  return httpClient.delete(`/${id}`)
}
export default { getAll, create, get, update, remove, login }
