import httpClient from '../http-common'

const postjob = (data, companyid) => {
    return httpClient.post(`/jobpost/create/${companyid}`, data);
};

const getAllJob = () => {
    return httpClient.get('/jobpost/all');
};

export default { postjob, getAllJob }