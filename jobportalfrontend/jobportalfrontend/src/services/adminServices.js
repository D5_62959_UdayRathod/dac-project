
import httpClient from '../http-common'

const get = (id) => {
    return httpClient.get('/admin/alluser', `/${id}`)
}

const updateUser = (data) => {
    return httpClient.put('/admin/update/user', data)
}



export default { get, updateUser }

