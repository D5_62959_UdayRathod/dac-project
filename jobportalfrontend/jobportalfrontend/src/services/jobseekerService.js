import httpClient from '../http-common'

const applyToJobPost = (data) => {
    return httpClient.post('/jobseeker/application', data);
};

const jobseekerRegistration = (data, id) => {
    return httpClient.post(`/jobseeker/profile/${id}`, data);
};


export default { applyToJobPost, jobseekerRegistration }