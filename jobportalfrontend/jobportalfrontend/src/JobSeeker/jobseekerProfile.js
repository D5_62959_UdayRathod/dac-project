//import RegisterForm from './register/RegisterForm'


const Registration = () => {
    const [companyName, setCompanyName] = useState('')
    const [companyType, setCompanyType] = useState('')
    const [companyDescription, setCompanyDescription] = useState('')
    const [establishmentDate, setEstablishmentDate] = useState('')
    const [companyEmail, setCompanyEmail] = useState('')
    const [companyContactNo, setCompanyContactNo] = useState('')
    const [companyImage, setCompanyImage] = useState('')
    const [companyWebsite, setCompanyWebsite] = useState('')
    const [dataAdded, setDataAdded] = useState('');
    const companyData = { companyName, companyType, companyDescription, establishmentDate, companyEmail, companyContactNo, companyImage, companyWebsite }
    const navigate = useNavigate();
    const [dataIsCorrect, setDataIsCorrect] = useState(false);
    const [errors, setErrors] = useState({})
    const user = localStorage.getItem("user");
    const userobj = JSON.parse(user)


    return (
        <section style={{ backgroundColor: '#eee' }}>
            <div className='container py-5'>
                <div className='row'>
                    <div className='col-8'>
                        <div className='card mb-4'>
                            <div className='card-body'>
                                <h5>Personal Details</h5>
                                <hr></hr>
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>First Name</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Last Name</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>email</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Mobile Number</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Date of birth</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="date"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>profile photo </p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="img"></input>
                                    </div>
                                </div>
                                <hr></hr>
                                <h5>Education Details</h5>
                                <hr></hr>
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Degree Name</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Specialization</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Institute Name</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"></input>>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Start date</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="date"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Completion Date</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="date"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Percentage</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="number"></input>
                                    </div>
                                </div>
                                <hr />
                                <h5>skills Set</h5>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Skill Name</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"></input>>
                                    </div>
                                </div>
                                <hr />
                                <h5>Experience Details</h5>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Company Name</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Is Currently Active</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Start Date</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="date"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>End Date</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="date"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Job Title</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>City</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>State</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Job Description</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <textarea></textarea>
                                    </div>
                                </div>
                                <hr />
                                <div className='col-sm-12'>
                                    <button style={{ backgroundColor: "DodgerBlue", padding: "10px 32px" }} type="submit">submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}


export default Registration