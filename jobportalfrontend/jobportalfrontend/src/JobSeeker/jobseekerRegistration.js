//import RegisterForm from './register/RegisterForm'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import jobseekerService from '../services/jobseekerService';

const Registration = () => {
    const [dateOfBirth, setDateOfBirth] = useState('')
    const [profilePhoto, setProfilePhoto] = useState('')
    const [degreeName, setDegreeName] = useState('')
    const [specialization, setSpecialization] = useState('')
    const [instituteName, setInstituteName] = useState('')
    const [startedDate, setStartedDate] = useState("")
    const [completionDate, setCompletionDate] = useState('')
    const [percentage, setPercentage] = useState('')
    const [cgpa, setCgpa] = useState('')

    const [companyName, setCompanyName] = useState('')
    const [isCurrentJob, setIsCurrentJob] = useState('')
    const [startDate, setStartDate] = useState('')
    const [endDate, setEndDate] = useState('')
    const [jobTitle, setJobTitle] = useState("")
    const [jobLocationCity, setJobLocationCity] = useState('');
    const [jobLocationState, setJobLocationState] = useState('')
    const [jobDescription, setJobDescription] = useState('')
    const user = localStorage.getItem("user");
    const userobj = JSON.parse(user)
    const id = userobj.id;
    const [skillSets, setSkillSets] = useState([])


    const jobSeekerData = {
        dateOfBirth,
        profilePhoto,
        eduDetails: [{
            degreeName,
            specialization,
            instituteName,
            startedDate,
            completionDate,
            percentage,
            cgpa
        },
        ],
        skillSets,
        experienceDetails: [{
            companyName,
            isCurrentJob,
            startDate,
            endDate,
            jobTitle,
            jobLocationCity,
            jobLocationState,
            jobDescription
        }
        ]

    }
    const navigate = useNavigate();
    // const [dataIsCorrect, setDataIsCorrect] = useState(false);
    // const [errors, setErrors] = useState({})
    const handleFormSubmit = (event) => {
        event.preventDefault();
        jobseekerService
            .jobseekerRegistration(jobSeekerData, id)
            .then(response => {
                console.log(" jobseeker data added successfully", response.data);
                // setDataAdded("Registration Successful")
                sleep(3000).then(r => {
                    navigate("/userhome");
                });
            })
            .catch(error => {
                console.log('Something went wrong', error);
                // setDataAdded("Error While Registration");
            })
    }


    return (
        <section style={{ backgroundColor: '#eee' }}>
            <div className='container py-5'>
                <div className='row'>
                    <div className='col-8'>
                        <div className='card mb-4'>
                            <div className='card-body'>
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Date of birth</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="date"
                                            name='dateOfBirth'
                                            value={dateOfBirth}
                                            onChange={(e) => setDateOfBirth(e.target.value)}></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>profile photo </p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"

                                            name='profilePhoto'
                                            value={profilePhoto}
                                            onChange={(e) => setProfilePhoto(e.target.value)}></input>
                                    </div>
                                </div>
                                <hr></hr>
                                <h5>Education Details</h5>
                                <hr></hr>
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Degree Name</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"

                                            name='degreeName'
                                            value={degreeName}
                                            onChange={(e) => setDegreeName(e.target.value)}></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Specialization</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"

                                            name='specialization'
                                            value={specialization}
                                            onChange={(e) => setSpecialization(e.target.value)}></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Institute Name</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"

                                            name='instituteName'
                                            value={instituteName}
                                            onChange={(e) => setInstituteName(e.target.value)}></input>>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Start date</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="date"

                                            name='startDate'
                                            value={startedDate}
                                            onChange={(e) => setStartedDate(e.target.value)}></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Completion Date</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="date"

                                            name='completionDate'
                                            value={completionDate}
                                            onChange={(e) => setCompletionDate(e.target.value)}></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Percentage</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="number"

                                            name='percentage'
                                            value={percentage}
                                            onChange={(e) => setPercentage(e.target.value)}></input>
                                    </div>
                                </div>
                                <hr />
                                <h5>skills Set</h5>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Skill Name</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"

                                            name='skillSets'
                                            value={skillSets}
                                            onChange={(e) => setSkillSets(e.target.value)}></input>>
                                    </div>
                                </div>
                                <hr />
                                <h5>Experience Details</h5>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Company Name</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"

                                            name='companyName'
                                            value={companyName}
                                            onChange={(e) => setCompanyName(e.target.value)} ></input>

                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Is Currently Active</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"

                                            name='isCurrentJob'
                                            value={isCurrentJob}
                                            onChange={(e) => setIsCurrentJob(e.target.value)}></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Start Date</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="date"

                                            name='startDate'
                                            value={startDate}
                                            onChange={(e) => setStartDate(e.target.value)}></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>End Date</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="date"

                                            name='endDate'
                                            value={endDate}
                                            onChange={(e) => setEndDate(e.target.value)}></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Job Title</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"

                                            name='jobTitle'
                                            value={jobTitle}
                                            onChange={(e) => setJobTitle(e.target.value)}></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>City</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"

                                            name='jobLocationCity'
                                            value={jobLocationCity}
                                            onChange={(e) => setJobLocationCity(e.target.value)}></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>State</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <input type="text"

                                            name='jobLocationState'
                                            value={jobLocationState}
                                            onChange={(e) => setJobLocationState(e.target.value)}></input>
                                    </div>
                                </div>
                                <hr />
                                <div className='row'>
                                    <div className='col-sm-3'>
                                        <p className='mb-0'>Job Description</p>
                                    </div>
                                    <div className='col-sm-9'>
                                        <textarea

                                            name='jobDescription'
                                            value={jobDescription}
                                            onChange={(e) => setJobDescription(e.target.value)}></textarea>
                                    </div>
                                </div>
                                <hr />
                                <div className='col-sm-12'>
                                    <button style={{ backgroundColor: "DodgerBlue", padding: "10px 32px" }} type="submit" onClick={handleFormSubmit}>submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    )
}


export default Registration