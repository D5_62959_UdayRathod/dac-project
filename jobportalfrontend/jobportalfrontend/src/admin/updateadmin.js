import adminServices from "../services/adminServices";
import { useNavigate } from "react-router-dom";
import { useState } from 'react'

const UpdateAdmin = () => {
    //const navigate = useNavigate();

    const userobj = localStorage.getItem('user');
    const myuser = JSON.parse(userobj);
    const id = myuser.id;
    const [firstName, setFirstName] = useState(myuser.firstName);
    const [lastName, setLastName] = useState(myuser.lastName);
    const [mobileNumber, setMobileNumber] = useState(myuser.mobileNumber);
    const [email, setEmail] = useState(myuser.email);
    const gender = myuser.gender;
    const registerDate = myuser.registerDate

    const [role, setRole] = useState(myuser.role);
    const user = { id, firstName, lastName, email, mobileNumber, gender, registerDate, role };

    const handleFormSubmit = (event) => {
        event.preventDefault();

        // setErrors(validation(user));

        console.log("sending", user);
        //  if (dataIsCorrect === true) {
        adminServices.updateUser(user)
            .then(response => {
                //setDataAdded("Updation Done Successfully");
                localStorage.removeItem('user');
                localStorage.setItem('user', JSON.stringify(response.data));
                console.log("successfully updated admin deta")
            })
            .catch(error => {
                console.log("Error While Updating Account");
            })
    }


    return (
        <>

            <div className="wrapper">
                <div className="app-wrapper">
                    <div>
                        <h2 className="title">Update Profile</h2>
                    </div>
                    <form className="form-wrapper">
                        <div className="row">
                            <div className="col">
                                <div className="name">
                                    <label className="label">First Name</label>
                                    <input
                                        className="input"
                                        type="text"
                                        name="firstName"
                                        value={firstName}
                                        onChange={(e) => setFirstName(e.target.value)}
                                    />

                                </div>


                                <div className="name">
                                    <label className="label">Last Name</label>
                                    <input
                                        className="input"
                                        type="text"
                                        name="lastName"
                                        value={lastName}
                                        onChange={(e) => setLastName(e.target.value)}
                                    />

                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="name">
                                <label className="label">Contact Number</label>
                                <input
                                    className="input"
                                    type="text"
                                    name="mobileNumber"
                                    value={mobileNumber}
                                    onChange={(e) => setMobileNumber(e.target.value)}
                                />

                            </div>
                        </div>
                        <div className="col">
                            <div className="email">
                                <label className="label">Email</label>
                                <input
                                    className="input"
                                    type="text"
                                    name="email"
                                    value={email}
                                    onChange={(e) => setEmail(e.target.value)}
                                />

                            </div>
                            <div className="col">
                                <div className="email">
                                    <label className="label"></label>
                                    <input
                                        className="input"
                                        type="text"
                                        name="role"
                                        value={role}
                                        onChange={(e) => setRole(e.target.value)}
                                    />

                                </div>

                            </div>
                        </div>

                        <div>
                            <button className="submit" onClick={handleFormSubmit}>
                                Update
                           </button>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
};

export default UpdateAdmin;
