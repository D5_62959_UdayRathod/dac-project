
import { useState } from 'react'
import adminServices from '../services/adminServices'
import './finduser.css'

const FindUser = () => {
    const [email, setEmail] = useState('')

    const handleFormSubmit = (event) => {
        event.preventDefault()
        adminServices.get(email).then((response) => {
            console.log(response.email)
        })
    }
    return (
        <div className="container text-center">
            <div className="row">
                <div className="col-6" style={{ marginTop: 150 }}>
                    <label>Email</label>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="text" value={email}></input>
                </div>
                <div className="col-4" style={{ marginTop: 150, width: 20 }}>
                    <input type="button" value="submit" style={{ width: 150 }}></input>
                </div>
            </div>
        </div>
    )
}
export default FindUser