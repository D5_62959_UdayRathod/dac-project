import shahid from '../images/shahidkapoor.jpg'
const Adminprofile = () => {

    const admin = localStorage.getItem("user");
    const adminObj = JSON.parse(admin)
    return (
        <div className="container emp-profile">
            <form>
                <div className="row">
                    <div className="col-md-4">
                        <img src={shahid} style={{ height: 200, width: 200 }} alt="profile" />
                    </div>

                    <div className="col-md-6">
                        <div className="profile-head">
                            <h5>{adminObj.firstName}</h5>
                            <h6>{adminObj.lastName}</h6>

                            &nbsp;&nbsp;
                            <ul className="nav nav-tabs">
                                <li className="nav-item">
                                    <a className="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab">about</a>
                                </li>
                                &nbsp;&nbsp;
                                <li className="nav-item">
                                    <a className="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab">Timeline</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div className="col-md-2">
                        <input type="submit" className="profile-edit-btn" name="btn" value="Edit profile"></input>
                    </div>

                </div>
                <div className="row">
                    <div className="col-md-4">
                        <div className="profile-work">
                            <p>Work Link</p>
                            <a href="https://www.youtube.com/watch?v=X4TyG9grBSw" target="_blank">Youtube</a><br></br>
                            <a href="https://www.youtube.com/watch?v=X4TyG9grBSw" target="_blank">Linkedln</a><br></br>
                            <a href="https://www.youtube.com/watch?v=X4TyG9grBSw" target="_blank">instgram</a><br></br>
                            <a href="https://www.youtube.com/watch?v=X4TyG9grBSw" target="_blank">Facebook</a><br></br>
                            <a href="https://www.youtube.com/watch?v=X4TyG9grBSw" target="_blank">Tweeter</a><br></br>
                            <a href="https://www.youtube.com/watch?v=X4TyG9grBSw" target="_blank">Gmail</a><br></br>
                            <a href="https://www.youtube.com/watch?v=X4TyG9grBSw" target="_blank">snapchat</a><br></br>
                        </div>
                    </div>

                    <div className="col-md-8 pl-5 about-info">
                        <div className="tab-content profile-tab" id="myTabContent">
                            <div className="tab-pane fade show active " id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div className="row">
                                    <div className="col-md-6">
                                        <label> user id</label>
                                    </div>
                                    <div className="col-md-6">
                                        <p>{adminObj.id}</p>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <label>FirstName</label>
                                    </div>
                                    <div className="col-md-6">
                                        <p>{adminObj.firstName}</p>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <label> LastName</label>
                                    </div>
                                    <div className="col-md-6">
                                        <p>{adminObj.lastName}</p>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <label>Email Id</label>
                                    </div>
                                    <div className="col-md-6">
                                        <p>{adminObj.email}</p>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-6">
                                        <label>Mobile Number</label>
                                    </div>
                                    <div className="col-md-6">
                                        <p>{adminObj.mobileNumber}</p>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>



            </form>
        </div>
    )

}
export default Adminprofile