import { useState } from 'react';
import UserList from './getusers';
import './UserHome.css'
import Adminprofile from '../admin/adminprofile'
import FindUser from '../admin/finduser';
import CompanyForm from '../company/companyform';
import JobPost from "../pages/jobs/jobPost"
import Registration from '../JobSeeker/jobseekerRegistration'
import GetAllCompany from '../company/getallcompany';
import UpdateAdmin from '../admin/updateadmin'
import PostedJob from '../alljobpost'

function UserHome() {
    //const user = useSelector((state) => state.user.user)
    const userobj = localStorage.getItem('user');
    const user = JSON.parse(userobj);
    console.log(userobj)
    const [rightComponent, setRightComponent] = useState("")


    if (user.role === "JOB_SEEKER") {
        return (
            <div className="row">
                <div className="col-3 side-panel p-3">
                    <nav className="mynav flex-column" >
                        <span className="nav-link mb-3" style={{ cursor: "pointer" }} onClick={() => { setRightComponent(<Registration />) }}>My Profile</span>
                        <hr className='myline' />
                        <span className="nav-link mb-3" style={{ cursor: "pointer" }} onClick={() => { setRightComponent() }}>Update Profile</span>
                        <hr className='myline' />
                        <span className="nav-link mb-3" style={{ cursor: "pointer" }} onClick={() => { setRightComponent() }}>Find Job</span>
                        <hr className='myline' />
                        <span className="nav-link mb-3" style={{ cursor: "pointer" }} onClick={() => { setRightComponent() }}>Applied Job </span>
                        <hr className='myline' />
                        <span className="nav-link mb-3" className="pointer" onClick={() => { setRightComponent() }}>WishList </span>
                    </nav>
                </div>
                <div className="col-9 box-height">
                    {rightComponent}
                </div>
            </div>
        )
    }
    if (user.role === "ADMIN") {
        return (
            <div className="row">
                <div className="col-3 side-panel p-3">
                    <nav className="mynav flex-column">
                        <span className="nav-link mb-3 heading" style={{ cursor: "pointer" }} onClick={() => { setRightComponent(<Adminprofile />) }}>My Profile</span>
                        <hr className='myline' />
                        <span className="nav-link mb-3" style={{ cursor: "pointer" }} onClick={() => { setRightComponent(<UpdateAdmin />) }}>Update Profile</span>
                        <hr className='myline' />
                        <span className="nav-link mb-3 heading" style={{ cursor: "pointer" }} onClick={() => { setRightComponent(<UserList />) }}>All User</span>
                        <hr className='myline' />
                        <span className="nav-link mb-3 heading" style={{ cursor: "pointer" }} onClick={() => { setRightComponent() }}>Verifiy User</span>
                        <hr className='myline' />
                        <span className="nav-link mb-3 heading" style={{ cursor: "pointer" }} onClick={() => { setRightComponent(<PostedJob />) }}>All JobPosted</span>
                        <hr className='myline' />
                        <span className="nav-link mb-3 heading" style={{ cursor: "pointer" }} onClick={() => { setRightComponent(<GetAllCompany />) }}> All Registered Company</span>
                        <hr className='myline' />
                        <span className="nav-link mb-3 heading" style={{ cursor: "pointer" }} onClick={() => { setRightComponent(<FindUser />) }}>Search</span>

                    </nav>



                </div>
                <div className="col-9 box-height">
                    {rightComponent}
                </div>
            </div>
        )
    }

    if (user.role === "EMPLOYER") {
        return (
            <div className="row">
                <div className="col-3 side-panel p-3">
                    <nav className="mynav flex-column">
                        <span className="nav-link mb-3 heading" style={{ cursor: "pointer" }} onClick={() => { setRightComponent() }}>My Profile</span>
                        <hr className='myline' />
                        <span className="nav-link mb-3" style={{ cursor: "pointer" }} onClick={() => { setRightComponent() }}>Update Profile</span>
                        <hr className='myline' />
                        <span className="nav-link mb-3 heading" style={{ cursor: "pointer" }} onClick={() => { setRightComponent(<CompanyForm />) }}>Company Registration</span>
                        <hr className='myline' />
                        <span className="nav-link mb-3 heading" style={{ cursor: "pointer" }} onClick={() => { setRightComponent(<JobPost />) }}>JobPost Registration</span>
                        <hr className='myline' />
                        <span className="nav-link mb-3 heading" style={{ cursor: "pointer" }} onClick={() => { setRightComponent() }}>All JobPosted</span>
                        <hr className='myline' />
                        <span className="nav-link mb-3 heading" style={{ cursor: "pointer" }} onClick={() => { setRightComponent() }}> All Registered Company</span>
                        <hr className='myline' />
                        <span className="nav-link mb-3 heading" style={{ cursor: "pointer" }} onClick={() => { setRightComponent(<FindUser />) }}>Search</span>

                    </nav>



                </div>
                <div className="col-9 box-height">
                    {rightComponent}
                </div>
            </div>
        )
    }
}

export default UserHome;