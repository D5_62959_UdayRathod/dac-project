import React from 'react'
//import "./style.css";
import { useEffect } from 'react'
import userServices from '../services/userServices'
import { useState } from 'react'
import './getusers.css'

const UserList = () => {
  const [users, setUsers] = useState([])

  useEffect(() => {
    getAllUser()
  }, [])

  const getAllUser = () => {
    userServices.getAll().then((response) => {
      const result = response.data
      setUsers(result)
      console.log(result)
    })
  }

  return (
    <div className='container'>
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>Id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Mobile Number</th>
            <th>Gender</th>
            <th>Register Date</th>
            <th>Role</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => {
            return (
              <tr>
                <td>{user.id}</td>
                <td>{user.firstName}</td>
                <td>{user.lastName}</td>

                <td>{user.email}</td>
                <td>{user.mobileNumber}</td>
                <td>{user.gender}</td>
                <td>{user.registerDate}</td>
                <td>{user.role}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}

export default UserList