import { useState } from 'react'
import jobpostservice from '../../services/jobpostservice'
import { useNavigate } from 'react-router-dom'

const JobPost = () => {
  const [jobPostTitle, setJobPostTitle] = useState('')
  const [createDate, setCreateDate] = useState('')
  const [jobDescription, setJobDescription] = useState('')
  const [jobType, setJobType] = useState('')
  const [vacantPost, setVacantPost] = useState('')
  const [salary, setSalary] = useState('')
  const [Active, setIsActive] = useState('')
  const [dataAdded, setDataAdded] = useState('');
  const company = localStorage.getItem("company");
  const companyobj = JSON.parse(company)
  const { id } = companyobj
  const [dataIsCorrect, setDataIsCorrect] = useState(true);
  console.log(id)
  const jobpostdata = {
    jobPostTitle,
    createDate,
    jobDescription,
    jobType,
    vacantPost,
    salary,
    Active,
    id,
  }
  const sleep = ms => new Promise(r => setTimeout(r, ms));
  const navigate = useNavigate();

  const handleFormSubmit = (event) => {
    event.preventDefault();
    // setErrors(validation(companyData));
    // if (Object.keys(errors).length === 0) {
    //     setDataIsCorrect(true);
    // }
    console.log("sending", jobpostdata);
    if (dataIsCorrect === true) {
      jobpostservice
        .postjob(jobpostdata, id)
        .then(response => {
          console.log("jobpost added successfully", response.data);
          setDataAdded("Registration Successful")
          sleep(3000).then(r => {
            navigate("/userhome");
          });
        })
        .catch(error => {
          console.log('Something went wrong', error);
          setDataAdded("Error While Registration");
        })
    }
  };

  return (
    <div style={{ marginTop: 10 }}>
      <div className='wrapper'>
        <div className='app-wrapper'>
          <div>
            <h2 className='title'>Register Job Post</h2>
          </div>
          <form className='form-wrapper'>
            <div className='row'>
              <div className='col'>
                <div className='name'>
                  <label className='label'>Title</label>
                  <input
                    className='input'
                    type='text'
                    name='jobPostTitle'
                    value={jobPostTitle}
                    onChange={(e) => setJobPostTitle(e.target.value)}
                  />
                </div>

                <div className='name'>
                  <label className='label'>Post Date</label>
                  <input
                    className='input'
                    type='date'
                    name='createDate'
                    value={createDate}
                    onChange={(e) => setCreateDate(e.target.value)}
                  />
                </div>

                <div className='name'>
                  <label className='label'>Job Description</label>
                  <input
                    className='input'
                    type='text'
                    name='jobDescription'
                    value={jobDescription}
                    onChange={(e) => setJobDescription(e.target.value)}
                  />
                </div>
                <div className='name'>
                  <label className='label'>Type of Job Post</label>
                  <input
                    className='input'
                    type='text'
                    name='jobType'
                    value={jobType}
                    onChange={(e) => setJobType(e.target.value)}
                  />
                </div>
                <div className='name'>
                  <label className='label'>Vacant post</label>
                  <input
                    className='input'
                    type='number'
                    name='vacantPost'
                    value={vacantPost}
                    onChange={(e) => setVacantPost(e.target.value)}
                  />
                </div>
                <div className='name'>
                  <label className='label'>Salary</label>
                  <input
                    className='input'
                    type='number'
                    name='salary'
                    value={salary}
                    onChange={(e) => setSalary(e.target.value)}
                  />
                </div>
                <div className='name'>
                  <label className='label'> Active </label>
                  <div>
                    <div class='form-check form-check-inline'>
                      <input
                        class='form-check-input'
                        type='radio'
                        name='isActive'
                        value='yes'
                        onChange={(e) => setIsActive(e.target.value)}
                      />
                      &nbsp;
                      <label
                        class='form-check-label'
                        for='inlineRadio1'></label>
                      <label className='label'> YES </label>
                    </div>
                    <div class='form-check form-check-inline'>
                      <input
                        class='form-check-input'
                        type='radio'
                        name='isActive'
                        value='no'
                        onChange={(e) => setIsActive(e.target.value)}
                      />
                      &nbsp;
                      <label
                        class='form-check-label'
                        for='inlineRadio1'></label>
                      <label className='label'> NO </label>
                    </div>
                    <div>
                      <button className='submit' onClick={handleFormSubmit}>Register</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

const styles = {
  container: {
    width: 400,
    height: 800,
    padding: 20,
    position: 'relative',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: 'auto',
    borderColor: '#1E90FF',
    borderRadius: 10,
    broderWidth: 1,
    borderStyle: 'solid',
    boxShadow: '1px 1px 20px 5px #C9C9C9',
  },
  signinButton: {
    position: 'relative',
    width: '100%',
    height: 40,
    backgroundColor: '#1E90FF',
    color: 'white',
    borderRadius: 5,
    border: 'none',
    marginTop: 10,
  },
}

export default JobPost