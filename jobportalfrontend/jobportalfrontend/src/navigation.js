import React, { useEffect, useState } from "react";
import { Nav, Container } from "react-bootstrap";
import { BrowserRouter, Routes, Route, Link, NavLink } from "react-router-dom";
import About from "./components/about";
import Home from "./components/home"
import RegisterForm from "./register/RegisterForm";
import Login from "./login/Login";
import UserHome from "./user/UserHome";
import Footer from './components/footer'
import ErrorPage from "./error/ErrorPage";
import Navbar from "./components/navbar"
import Adminprofile from "./admin/adminprofile";
import FindUser from "./admin/finduser";
import PostedJob from './alljobpost'
import JobPost from "./pages/jobs/jobPost";
import Registration from "./JobSeeker/jobseekerRegistration";
import GetAllCompany from "./company/getallcompany";
import UpdateAdmin from "./admin/updateadmin";

const Navigation = () => {
    const myuserobj = localStorage.getItem("user");
    const user = JSON.parse(myuserobj);

    const [userobj, setUserobj] = useState(false);

    useEffect(() => {
        const loggedIn = localStorage.getItem("user");
        if (loggedIn) {
            setUserobj(true);
        }
    }, []);

    const logout = () => {
        localStorage.removeItem("user");
        setUserobj(false);
    };
    return (
        <div>
            <BrowserRouter>
                <Navbar />
                <div>
                    <div>
                        <Routes>
                            <Route path="/home" element={<Home />} />
                            <Route path="/about" element={<About />} />
                            <Route path="/user/signin" element={<Login />} />
                            <Route path="/admin/profile" element={<Adminprofile />} />
                            <Route path="/find/email" element={<FindUser />}></Route>
                            <Route path="/postedjob" element={<PostedJob />}></Route>
                            <Route path="/jobpost" element={<JobPost />}></Route>
                            <Route path="/jobseeker/registration" element={<Registration />}></Route>
                            <Route path="get/allcompany" element={<GetAllCompany />} ></Route>
                            <Route path="update/admin/profile" element={<UpdateAdmin />} ></Route>


                            <Route
                                path="/register"
                                element={<RegisterForm />}
                            />

                            {userobj && (user.role == "ADMIN" | user.role == "JOB_SEEKER" | user.role == "EMPLOYER") ? <Route
                                path="/userhome"
                                element={<UserHome />}
                            /> : <Route
                                    path="user/signin"
                                    element={<Login />}
                                />}
                            {/* <Route path="/bookservice" element={<BookServices />} />
                            <Route path="/feedback" element={<FeedbackForm />} /> */}
                            {/* <Route
                                path="/api/psd/employee"
                                element={<RegisterEmp />}
                            /> */}
                            {/* <Route
                                path="/api/psd/forgetpassword"
                                element={<ForgetPassword />}
                            /> */}
                            {/* {userobj && (user.role == "ADMIN") ? <Route path="addprofession" element={<AddProfession />} /> : <Route path="addprofession" element={<NotAuthorized />} />} */}
                            <Route path="*" element={<ErrorPage />} />
                        </Routes>
                    </div>
                    <Footer />
                </div>
            </BrowserRouter>
        </div >
    );
};

export default Navigation;