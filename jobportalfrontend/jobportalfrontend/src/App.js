
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Navigation from './navigation'


function App() {
    return (
        <>
            <Navigation />
        </>
    )
}

export default App
