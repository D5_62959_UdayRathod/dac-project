import jobpostservice from "./services/jobpostservice"
import { useEffect, useState } from 'react'
import jobseekerService from "./services/jobseekerService";


//import react from 'react'
const PostedJob = () => {

    const [allPostedJob, SetallPostedJob] = useState([])
    const [dataAdded, setDataAdded] = useState("")

    const user = localStorage.getItem("user");
    const jobseeker = JSON.parse(user);

    const jobseekerId = jobseeker.id;



    const getPostedJob = () => {

        jobpostservice.getAllJob()
            .then(Response => {

                const result = Response.data
                SetallPostedJob(result)
                console.log(result)

            }).catch(error => {
                console.log('Something went wrong', error);
                setDataAdded("Error While Registration");
            })

    }

    useEffect(() => {
        getPostedJob()
    }, []);

    const handleSubmit = (id) => {
        console.log(id);
        const jobapplicationObj = { id, jobseekerId }
        jobseekerService.applyToJobPost(jobapplicationObj)
            .then(Response => {
                const result = Response.data
                console.log(result)
            })

    }


    return (

        allPostedJob.map((jobpost) => {
            return (
                <div className="container">
                    <div className="row">

                        <div className="col-sm-6">

                            <div className="card" style={{ width: "30rem", marginTop: 50, marginLeft: 280, borderColor: "blue" }} >

                                <div className="card-body">
                                    <h5 className="card-title">{jobpost.jobPostTitle}</h5>
                                    <h5 className="card-subtitle mb-2 text-muted">{jobpost.company.companyName}</h5>
                                    <h6 style={{ color: "green" }}>{jobpost.salary}</h6>
                                    <h6 style={{ color: "green" }}>{jobpost.jobType}</h6>
                                    <h6 style={{ color: "green" }}>No of Post :{jobpost.vacantPost}</h6>

                                    <p className="card-text">
                                        {jobpost.jobDescription}
                                    </p>

                                    <p >job is posted on {jobpost.createDate}</p>
                                    <button style={{ color: "white", backgroundColor: "blue", padding: "10px 32px" }} name="applybtn" onClick={() => {
                                        handleSubmit(jobpost.id)
                                    }}> Apply now</button>

                                </div>
                            </div>

                        </div>

                    </div>

                </div >

            )
        })

    )

}

export default PostedJob