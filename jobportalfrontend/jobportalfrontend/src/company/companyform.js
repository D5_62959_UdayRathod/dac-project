
import { useState } from 'react'
import validation from "../company/validation"
import { useNavigate } from "react-router-dom"
import empService from '../services/employerServices'
const CompanyForm = () => {
    const [companyName, setCompanyName] = useState('')
    const [companyType, setCompanyType] = useState('')
    const [companyDescription, setCompanyDescription] = useState('')
    const [establishmentDate, setEstablishmentDate] = useState('')
    const [companyEmail, setCompanyEmail] = useState('')
    const [companyContactNo, setCompanyContactNo] = useState('')
    const [companyImage, setCompanyImage] = useState('')
    const [companyWebsite, setCompanyWebsite] = useState('')
    const [dataAdded, setDataAdded] = useState('');
    const companyData = { companyName, companyType, companyDescription, establishmentDate, companyEmail, companyContactNo, companyImage, companyWebsite }
    const navigate = useNavigate();
    const [dataIsCorrect, setDataIsCorrect] = useState(false);
    const [errors, setErrors] = useState({})
    const user = localStorage.getItem("user");
    const userobj = JSON.parse(user)



    const { id } = userobj;
    const sleep = ms => new Promise(r => setTimeout(r, ms));


    const handleFormSubmit = (event) => {
        event.preventDefault();
        setErrors(validation(companyData));
        if (Object.keys(errors).length === 0) {
            setDataIsCorrect(true);
        }
        console.log("sending", companyData);
        if (dataIsCorrect === true) {
            empService
                .addCompany(companyData, id)
                .then(response => {
                    console.log("companyData added successfully", response.data);
                    setDataAdded("Registration Successful");
                    localStorage.setItem('company', JSON.stringify(response.data));
                    sleep(3000).then(r => {
                        navigate("/userhome");
                    });
                })
                .catch(error => {
                    console.log('Something went wrong', error);
                    setDataAdded("Error While Registration");
                })
        }
    };


    return (
        <>
            {dataAdded && <h3 class="alert alartbox alert-primary" role="alert">{dataAdded}</h3>}
            <div style={{ marginTop: 10 }}>
                <div className='wrapper'>
                    <div className='app-wrapper'>
                        <div>
                            <h2 className='title'>Register Company Details</h2>
                        </div>
                        <form className='form-wrapper'>


                            <div className='row'>
                                <div className='col'>

                                    <div className='name'>
                                        <label className='label'>Name</label>
                                        <input
                                            className='input'
                                            type='text'
                                            name='companyName'
                                            value={companyName}
                                            onChange={(e) => setCompanyName(e.target.value)}
                                        />
                                        {errors.firstName && <p className="error">{errors.companyName}</p>}
                                    </div>

                                    <div className='name'>
                                        <label className='label'>Company Type</label>
                                        <input
                                            className='input'
                                            type='text'
                                            name='companyType'
                                            value={companyType}
                                            onChange={(e) => setCompanyType(e.target.value)}
                                        />
                                        {errors.companyType && <p className="error">{errors.companyType}</p>}
                                    </div>
                                    <div className='name'>
                                        <label className='label'>Description</label>
                                        <textarea
                                            className='input'
                                            type='textarea'
                                            name='companyDescription'
                                            value={companyDescription}
                                            onChange={(e) => setCompanyDescription(e.target.value)}
                                        />
                                        {errors.companyDescription && <p className="error">{errors.companyDescription}</p>}
                                    </div>

                                    <div className='name'>
                                        <label className='label'>Establishment Date</label>
                                        <input
                                            className='input'
                                            type='date'
                                            name='establishmentDate'
                                            value={establishmentDate}
                                            onChange={(e) => setEstablishmentDate(e.target.value)}
                                        />
                                        {errors.establishmentDate && <p className="error">{errors.establishmentDate}</p>}
                                    </div>
                                    <div className='name'>
                                        <label className='label'>Email</label>
                                        <input
                                            className='input'
                                            type='email'
                                            name='companyEmail'
                                            value={companyEmail}
                                            onChange={(e) => setCompanyEmail(e.target.value)}
                                        />
                                        {errors.companyEmail && <p className="error">{errors.companyEmail}</p>}
                                    </div>
                                    <div className='name'>
                                        <label className='label'>Contact Number</label>
                                        <input
                                            className='input'
                                            type='number'
                                            name='companyContactNo'
                                            value={companyContactNo}
                                            onChange={(e) => setCompanyContactNo(e.target.value)}
                                        />
                                        {errors.companyContactNo && <p className="error">{errors.companyContactNo}</p>}
                                    </div>
                                    <div className='name'>
                                        <label className='label'>Company Image</label>
                                        <input
                                            className='input'
                                            type='text'
                                            name='companyImage'
                                            value={companyImage}
                                            onChange={(e) => setCompanyImage(e.target.value)}
                                        />
                                        {errors.companyImage && <p className="error">{errors.companyImage}</p>}
                                    </div>
                                    <div className='name'>
                                        <label className='label'>Website</label>
                                        <input
                                            className='input'
                                            type='text'
                                            name='companyWebsite'
                                            value={companyWebsite}
                                            onChange={(e) => setCompanyWebsite(e.target.value)}
                                        />

                                        {errors.companyWebsite && <p className="error">{errors.companyWebsite}</p>}
                                    </div>

                                    <input type="hidden" id="empid" name="empid" value={user.id}></input>
                                    <button className="submit" onClick={handleFormSubmit}>
                                        Register
                                   </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}

const styles = {
    container: {
        width: 400,
        height: 800,
        padding: 20,
        position: 'relative',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        margin: 'auto',
        borderColor: '#1E90FF',
        borderRadius: 10,
        broderWidth: 1,
        borderStyle: 'solid',
        boxShadow: '1px 1px 20px 5px #C9C9C9',
    },
    signinButton: {
        position: 'relative',
        width: '100%',
        height: 40,
        backgroundColor: '#1E90FF',
        color: 'white',
        borderRadius: 5,
        border: 'none',
        marginTop: 10,
    },
}


export default CompanyForm