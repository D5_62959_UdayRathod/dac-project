const validation = (values) => {

    let errors = {};

    if (!values.CompanyName) {
        errors.firstName = "CompanyName is Required."
    }

    if (!values.companyType) {
        errors.companyType = "companyType is Required."
    }

    if (!values.companyDescription) {
        errors.companyDescription = "companyDescription is Required."
    }

    if (!values.companyEmail) {
        errors.companyEmail = "companyEmail is Required."
    } else if (!/\S+@\S+\.\S+/.test(values.empEmail)) {
        errors.companyEmail = "companyEmail is invalid."
    }

    if (!values.password) {
        errors.password = "Password is Required."
    } else if (values.password.length < 5) {
        errors.password = "Password must be more then 5 charecters."
    }

    if (!values.establishmentDate) {
        errors.establishmentDate = "establishmentDate is Required."
    }

    if (!values.companyContactNo) {
        errors.companyContactNo = "companyContactNo is Required."
    }

    if (!values.companyImage) {
        errors.companyImage = "companyImage is Required."
    }

    if (!values.companyWebsite) {
        errors.companyWebsite = "companyWebsite is Required."
    }



    return errors;
}
export default validation;