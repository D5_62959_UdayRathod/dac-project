import { useEffect, useState } from 'react'
import employerServices from "../services/employerServices"

const GetAllCompany = () => {
    const [allRegisteredCompany, setAllRegisteredCompany] = useState([])


    useEffect(() => {
        allCompany()
    }, [])

    const allCompany = () => {

        employerServices.allCompany().then((response) => {
            const result = response.data
            setAllRegisteredCompany(result)
            console.log(result)
        })
    }
    return (
        <div className='container'>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>company Name</th>
                        <th>company Type</th>
                        <th>company Description</th>
                        <th>Establishment Date</th>
                        <th>company Email </th>
                        <th>company ContactNo</th>
                        <th>companyImage</th>
                        <th>company Website</th>
                        <th>Employer id</th>
                    </tr>
                </thead>
                <tbody>
                    {allRegisteredCompany.map((regsiteredCompany) => {
                        return (
                            <tr>
                                <td>{regsiteredCompany.id}</td>
                                <td>{regsiteredCompany.companyName}</td>
                                <td>{regsiteredCompany.companyType}</td>

                                <td>{regsiteredCompany.companyDescription}</td>
                                <td>{regsiteredCompany.establishmentDate}</td>
                                <td>{regsiteredCompany.companyEmail}</td>
                                <td>{regsiteredCompany.companyContactNo}</td>
                                <td>{regsiteredCompany.companyImage}</td>
                                <td>{regsiteredCompany.companyWebsite}</td>
                                <td>{regsiteredCompany.employer.id}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )



}

export default GetAllCompany;